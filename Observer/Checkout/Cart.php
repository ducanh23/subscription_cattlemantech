<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/08/2016
 * Time: 08:52
 */
namespace Magenest\Subscription\Observer\Checkout;

use Magenest\Subscription\Model\AttributeFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Subscription\Logger\Logger;

class Cart implements ObserverInterface
{
    /**
     * @var \Magenest\Subscription\Logger\Logger
     */
    protected $_logger;

    /**
     * @var \Magenest\Subscription\Model\AttributeFactory
     */
    protected $_attributeFactory;

    /**
     * @var \Magento\Catalog\Model\Product Factory
     */
    protected $productModelFactory;

    /**
     * @var \Magenest\Subscription\Model\TemplateFactory
     */
    protected $templateFactory;

    public $util;

    /**
     * Cart constructor.
     * @param Logger $logger
     * @param AttributeFactory $attributeFactory
     * @param \Magento\Catalog\Model\ProductFactory $productModelFactory
     * @param \Magenest\Subscription\Model\TemplateFactory $templateFactory
     */
    public function __construct(
        Logger $logger,
        AttributeFactory $attributeFactory,
        \Magento\Catalog\Model\ProductFactory $productModelFactory,
        \Magenest\Subscription\Model\TemplateFactory $templateFactory,
        \Magento\Directory\Model\Currency $currency,
	\Magenest\Subscription\Helper\Util $util
    ) {
        $this->_logger = $logger;
        $this->currency = $currency;	       
	$this->productModelFactory = $productModelFactory;
        $this->_attributeFactory = $attributeFactory;
        $this->templateFactory = $templateFactory;
        $this->util = $util;
    }

    /**
     * binding subscription's option to quote item option
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Quote\Model\Quote\Item $item */
        $item = $observer->getEvent()->getQuoteItem();
        $currencySymbol = $this->currency->getCurrencySymbol();
        /** @var array $subscriptionOption */
        $subscriptionOption = [
            'optionName' => '',
            'selectedUnit' =>'month',
            'frequency'  => '',
            'maxCycles'  => '',
            'trialEnabled' => 'yes',
            'trialAmount'   => '0',
            'trialUnit'     => 'day',
            'trialFrequency' => '',
            'trialMaxCycles'  => '',
            'initEnabled' => 'no',
            'initFee'    => '0',
            'mode'       =>'1'
        ];

        $productId = $item->getProduct()->getId();
        /** @var \Magento\Catalog\Model\Product $product */
        $product  = $this->productModelFactory->create()->load($productId);
        $buyInfo = $item->getBuyRequest();

        $subscriptionOptionParam = $buyInfo->getData('subscription_option');

        $subscriptionOptionName = (isset($subscriptionOptionParam['optionName'])) ?$subscriptionOptionParam['optionName'] : '';
        $modeParam = intval($buyInfo->getData('subscription_mode'));
        $startDateParam = $buyInfo->getData('subscription_start_date');

        if ($subscriptionOptionParam == '' ||  $subscriptionOptionParam == null || $subscriptionOptionParam == "-1") {
            return;
        }

        $subscriptionParams = $this->getSubscriptionParams($productId, $subscriptionOptionParam);
        $subscriptionOption = $subscriptionParams;

        /** init value for mode  */
        //$subscriptionOption['mode'] = $modeParam;
        $subscriptionOption['mode'] = 1;
        $modeParam = 1;
        $subscriptionOption['start_date'] = $this->getFormattedStartDate($startDateParam);

        /** base on the mode of subscription box we define the name and qty and subscription of the subscription box */
        if ($modeParam == 1) {
            $subscriptionOption['name'] = $product->getName() . ' ' . $subscriptionOptionName;
            $subscriptionOption['description'] = $product->getName() .' ' . $subscriptionOptionName;
            ;
            $subscriptionOption['qty'] = $item->getQty();
        }

        if ($subscriptionParams) {
            $additionalOptions = [];

            foreach ($subscriptionParams as $key => $value) {
                if ($value) {
                    //the extension allows displaying information of subscription name, trial amount, and init fee only
                    if ($key =='optionName') {
                        $optionLabel = strval(__('Subscription'));
                        $additionalOptions[] = array(
                            'label' => $optionLabel,
                            'value' => $value
                        );
                    }
                    if ($key =='initEnabled') {
                        $optionLabel = strval(__('Initial Fee Enabled'));
                        $additionalOptions[] = array(
                            'label' => $optionLabel,
                            'value' => $value
                        );
                    }
                    if ($subscriptionParams['initEnabled'] == 'yes') {
                        if ($key =='initFee') {
                            $optionLabel = strval(__('Initial Fee '));
                            $additionalOptions[] = array(
                                'label' => $optionLabel,
                                'value' => $currencySymbol.$value
                            );
                        }
                    }

                }
            }

            $item->addOption([
                'code' => 'additional_options',
                'product_id' => $productId,
                'value' => $this->util->stringifyArray($additionalOptions)
            ]);

            $item->addOption([
                'code' => 'subscription_options',
                'product_id' => $productId,
                'value' => $this->util->stringifyArray($subscriptionOption)
            ]);
        }
    }

    /**
     * get the start date in Y-m-d format
     * @param $startDate
     * @return string
     */
    public function getFormattedStartDate($startDate)
    {
        /** By default the input is australian date time format */

        $startDateObj = \DateTime::createFromFormat('d/m/Y', $startDate);
        return $startDateObj->format('Y-m-d');
    }

    /**
     * get more information about the subscription plan
     * @return array
     */
    public function getSubscriptionParams($productId, $optionKey)
    {
        $optionList = [];
        // get from templates
        $templateCollection = $this->templateFactory->create()->getCollection();
        foreach ($templateCollection as $temp) {
            $tempProductIds = unserialize($temp->getData('product_id'));
            foreach ($tempProductIds as $tempId) {
                if ($productId == $tempId) {
                    $tempOptions = unserialize($temp->getData('value'));
                    array_push($optionList, $tempOptions);
                }
            }
        }

        // get from product attributes
        /** @var \Magenest\Subscription\Model\Attribute $model */
        $model = $this->_attributeFactory->create();
        $attr = $model->getCollection()->addFieldToFilter('entity_id', $productId)->getFirstItem();
        if ($attr->getId()) {
            $options = unserialize($attr->getValue());

            if (is_array($options)) {
                for ($i = 0; $i < sizeof($options); $i++) {
                    array_push($optionList, $options[$i]);
                }
            }
        }

        if (isset($optionList[$optionKey])) {
            return $optionList[$optionKey];
        }
    }
}
