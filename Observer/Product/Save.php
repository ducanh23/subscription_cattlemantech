<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 08/03/2016
 * Time: 09:32
 */
namespace Magenest\Subscription\Observer\Product;

use Magento\Framework\Event\ObserverInterface;
use Magenest\Subscription\Logger\Logger;
use Magento\Framework\App\RequestInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Subscription\Model\AttributeFactory;
use Magenest\Subscription\Model\TemplateFactory;

class Save implements ObserverInterface
{
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var AttributeFactory
     */
    protected $_attributeFactory;

    /**
     * @var TemplateFactory
     */
    protected $_templateFactory;

    /**
     * Save constructor.
     * @param Logger $logger
     * @param RequestInterface $requestInterface
     * @param StoreManagerInterface $storeManagerInterface
     * @param AttributeFactory $attributeFactory
     * @param TemplateFactory $templateFactory
     */
    public function __construct(
        Logger $logger,
        RequestInterface $requestInterface,
        StoreManagerInterface $storeManagerInterface,
        AttributeFactory $attributeFactory,
        TemplateFactory $templateFactory
    ) {
        $this->_logger = $logger;
        $this->_request = $requestInterface;
        $this->_storeManager = $storeManagerInterface;
        $this->_attributeFactory = $attributeFactory;
        $this->_templateFactory = $templateFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $data = $this->_request->getParams();
        $product = $observer->getProduct();
        $productId = $product->getId();
        $billing = [];

        //Load options created by 'Add optioin'
        $attributeModel = $this->_attributeFactory->create();
        $attribute = $attributeModel->getCollection()->addFieldToFilter('entity_id', $productId)->getFirstItem();
        if ($attribute->getId()) {
            $attributeModel->load($attribute->getId());
        }

        // Load options Template
        if (array_key_exists('product', $data)) {
            if (array_key_exists('billing_options', $data['product'])) {
                $data = $data['product']['billing_options'];
                foreach ($data as $item) {
                    if ((!array_key_exists('is_option_template', $item)) || ($item['is_option_template'] == 'no')) {
                        $itemData = [
                            'optionName' => $item['billing_option_name'],
                            'selectedUnit' => $item['billing_unit'],
                            'frequency' => $item['billing_frequency'],
                            'maxCycles' => $item['billing_max_cycles'],
                            'trialEnabled' => $item['billing_is_trial_enabled'],
                            'trialAmount' => $item['billing_trial_amount'],
                            'trialUnit' => $item['billing_trial_unit'],
                            'trialFrequency' => $item['billing_trial_frequency'],
                            'trialMaxCycles' => $item['billing_trial_max_cycles'],
                            'initEnabled' => $item['billing_is_init_enabled'],
                            'initFee' => $item['billing_init_fee']
                        ];

                        array_push($billing, $itemData);

                    }
                }
            }
        }

        $attributeData = [
            'entity_id' => $productId,
            'value' => serialize($billing)
        ];

        $attributeModel->addData($attributeData)->save();

        return $this;
    }
}
