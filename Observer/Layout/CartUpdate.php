<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/08/2016
 * Time: 08:52
 */
namespace Magenest\Subscription\Observer\Layout;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;
use Magenest\Subscription\Model\AttributeFactory;

class CartUpdate implements ObserverInterface
{
    /**
     * @var \Magenest\Subscription\Model\AttributeFactory
     */
    protected $_attributeFactory;

    /**
     * @var \Magento\Catalog\Model\Product Factory
     */
    protected $productModelFactory;

    protected $util;

    /**
     * CartUpdate constructor.
     * @param AttributeFactory $attributeFactory
     * @param \Magento\Catalog\Model\ProductFactory $productModelFactory
     * @param \Magenest\Subscription\Helper\Util $util
     */
    public function __construct(
        AttributeFactory $attributeFactory,
        \Magento\Catalog\Model\ProductFactory $productModelFactory,
        \Magenest\Subscription\Helper\Util $util
    )
    {
        $this->productModelFactory = $productModelFactory;
        $this->_attributeFactory = $attributeFactory;
        $this->util = $util;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $item = $observer->getEvent()->getQuoteItem();
        $buyInfo = $item->getBuyRequest();
        $productId = $buyInfo->getData('product');
        $product = $this->productModelFactory->create()->load($productId);

        $subscriptionOptionParam = $buyInfo->getData('subscription_option');

        $subscriptionOptionName = (isset($subscriptionOptionParam['optionName'])) ? $subscriptionOptionParam['optionName'] : '';
        $modeParam = intval($buyInfo->getData('subscription_mode'));
        $startDateParam = $buyInfo->getData('subscription_start_date');

        if ($subscriptionOptionParam == '' || $subscriptionOptionParam == null || $subscriptionOptionParam == "-1") {
            return;
        }

        $subscriptionParams = $this->getSubscriptionParams($productId, $subscriptionOptionParam);
        $subscriptionOption = $subscriptionParams;

        /** init value for mode  */
        //$subscriptionOption['mode'] = $modeParam;
        $subscriptionOption['mode'] = 1;
        $modeParam = 1;
        $subscriptionOption['start_date'] = $this->getFormattedStartDate($startDateParam);

        /** base on the mode of subscription box we define the name and qty and subscription of the subscription box */
        if ($modeParam == 1) {
            $subscriptionOption['name'] = $product->getName() . ' ' . $subscriptionOptionName;
            $subscriptionOption['description'] = $product->getName() . ' ' . $subscriptionOptionName;;
            $subscriptionOption['qty'] = $item->getQty();
        }

        if ($subscriptionParams) {
            $additionalOptions = [];

            foreach ($subscriptionParams as $key => $value) {
                if ($value) {
                    //the extension allows displaying information of subscription name, trial amount, and init fee only
                    if ($key == 'optionName') {
                        $optionLabel = strval(__('subscription'));
                        $additionalOptions[] = array(
                            'label' => $optionLabel,
                            'value' => $value
                        );
                    }
                }
            }

            $item->addOption([
                'code' => 'additional_options',
                'product_id' => $productId,
                'value' => $this->util->stringifyArray($additionalOptions)
            ]);

            $item->addOption([
                'code' => 'subscription_options',
                'product_id' => $productId,
                'value' => $this->util->stringifyArray($subscriptionOption)
            ]);
        }
    }

    /**
     * get the start date in Y-m-d format
     * @param $startDate
     * @return string
     */
    public function getFormattedStartDate($startDate)
    {
        /** By default the input is australian date time format */

        $startDateObj = \DateTime::createFromFormat('d/m/Y', $startDate);
        return $startDateObj->format('Y-m-d');
    }

    /**
     * get more information about the subscription plan
     * @return array
     */
    public function getSubscriptionParams($productId, $option_key)
    {
        /** @var \Magenest\Subscription\Model\Attribute $model */
        $model = $this->_attributeFactory->create();
        $attr = $model->getCollection()->addFieldToFilter('entity_id', $productId)->getFirstItem();

        if ($attr->getId()) {
            $options = unserialize($attr->getValue());

            if (is_array($options)) {
                if (isset($options[$option_key])) {
                    return $options[$option_key];
                }
            }
        }
    }
}
