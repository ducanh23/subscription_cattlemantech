<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/03/2016
 * Time: 15:04
 */
namespace Magenest\Subscription\Observer\Layout;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magenest\Subscription\Logger\Logger;

/**
 * Class Add
 * @package Magenest\Subscription\Observer\Option
 */
class Add implements ObserverInterface
{
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * Add constructor.
     * @param Logger $logger
     */
    public function __construct(
        Logger $logger
    ) {
        $this->_logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        $product->setHasOptions(true);
    }
}
