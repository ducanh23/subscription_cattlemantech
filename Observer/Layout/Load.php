<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/03/2016
 * Time: 11:21
 */
namespace Magenest\Subscription\Observer\Layout;

use Magento\Framework\Event\ObserverInterface;

class Load implements ObserverInterface
{
    protected $checkoutSession;

    protected $profileFactory;

    protected $helper;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magenest\Subscription\Model\ProfileFactory $profileFactory,
        \Magenest\Subscription\Helper\Data $helper
    )
    {
        $this->checkoutSession = $checkoutSession;
        $this->profileFactory = $profileFactory;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $fullActionName = $observer->getEvent()->getFullActionName();

        /** @var \Magento\Framework\View\Layout $layout */
        $layout = $observer->getEvent()->getLayout();
        $handler = '';
        if ($fullActionName == 'catalog_product_view' || $fullActionName == 'checkout_cart_configure') {
            $handler = 'catalog_product_view_subscription';
        }

        if ($handler) {
            $layout->getUpdate()->addHandle($handler);
        }

        // For checkout onepage success page
        if ($fullActionName == 'checkout_onepage_success') {
            $canCreateOrder = $this->helper->isAllowNominalOrderCreation();
            $profilePlaced = 0;
            $order = $this->checkoutSession->getLastRealOrder();
            if (!$order->getId()) {
                $profilePlaced = 1;
            } else {
                $profileCollection = $this->profileFactory->create()->getCollection()->addFieldToFilter('order_id', $order->getId());
                if ($profileCollection->getSize() || !$canCreateOrder) {
                    $profilePlaced = 1;
                }
            }
            if ($profilePlaced) {
                $layout->getUpdate()->removeHandle('checkout_onepage_success');
                $layout->getUpdate()->addHandle('subscription_subscribe_success');
            }
        }
    }
}
