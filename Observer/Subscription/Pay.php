<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 7/11/17
 * Time: 3:06 PM
 */
namespace Magenest\Subscription\Observer\Subscription;

class Pay extends EmailObserver
{
    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRespository;
    /**
     * Pay constructor.
     * @param \Magenest\Subscription\Logger\Logger $logger
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magenest\Subscription\Helper\Email $emailHelper
     */
    public function __construct(
        \Magenest\Subscription\Logger\Logger $logger,
        \Magento\Framework\UrlInterface $url,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        \Magenest\Subscription\Helper\Email $emailHelper
    ) {
    
        $this->_urlBuiler = $url;
        $this->_templateId = $emailHelper::SUBSCRIPTION_PAY_TEMPLATE_ID;
        $this->_customerRespository = $customerRepositoryInterface;
        parent::__construct($logger, $url, $emailHelper);
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
 /** @var \Magenest\Subscription\Model\Profile $profile */
        $profile = $observer->getEvent()->getProfile();
        if ($profile) {
            $profileUrl = $this->_urlBuilder->getUrl('subscription/order/view', ['id' => $profile->getId()]);
            $customerName = $profile->getSubscriberName();

            $customerId = $profile->getData('customer_id');
            if ($customerId) {
                $customerName = $profile->getData('subscriber_name');
                $customer = $this->_customerRespository->getById($customerId);
                $customerEmail = $customer->getEmail();
                $this->setAddress($customerEmail);
                $this->setToName($customerName);
                $this->setVar([
                    'customer_name' => $customerName,
                    'profile_id' => $profile->getProfileId(),
                    'method_code' => $profile->getMethodCode(),
                    'profile_url' => $profileUrl
                ]);
                $this->send();
            }
        }
    }
}
