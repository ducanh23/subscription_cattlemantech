<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 7/11/17
 * Time: 3:45 PM
 */

namespace Magenest\Subscription\Observer\Subscription;

class Suspend extends EmailObserver
{
    /**
     * @var \Magenest\Subscription\Model\ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $_customerRespository;

    /**
     * Suspend constructor.
     * @param \Magenest\Subscription\Logger\Logger $logger
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magenest\Subscription\Helper\Email $emailHelper
     * @param \Magenest\Subscription\Model\ProfileFactory $profileFactory
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
     */
    public function __construct(
        \Magenest\Subscription\Logger\Logger $logger,
        \Magento\Framework\UrlInterface $url,
        \Magenest\Subscription\Helper\Email $emailHelper,
        \Magenest\Subscription\Model\ProfileFactory $profileFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface
    ) {
    
        $this->_urlBuiler = $url;
        $this->_profileFactory = $profileFactory;
        $this->_customerRespository = $customerRepositoryInterface;
        $this->_templateId = $emailHelper::SUSPEND_SUBSCRIPTION_TEMPLATE_ID;
        parent::__construct($logger, $url, $emailHelper);
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $profileId = $observer->getEvent()->getData('profile_id');

        if ($profileId) {
            /** @var \Magenest\Subscription\Model\Profile $profile */
            $profile = $this->_profileFactory->create();
            $profile = $profile->loadByProfileId($profileId);
            $customerId = $profile->getData('customer_id');

            if ($customerId) {
                $customerName = $profile->getData('subscriber_name');
                $customer = $this->_customerRespository->getById($customerId);
                $customerEmail = $customer->getEmail();
                $profileUrl = $this->_urlBuilder->getUrl('subscription/customer/view', ['id' => $profile->getId()]);

                $this->setAddress($customerEmail);
                $this->setToName($customerName);
                $this->setVar([
                    'customer_name' => $customerName,
                    'profile_id' => $profileId,
                    'profile_url' => $profileUrl
                ]);
                $this->send();
            }
        }
    }
}
