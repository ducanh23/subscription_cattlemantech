<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Logger;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Logger extends \Monolog\Logger
{
    /**
     * @var String
     */
    protected $_isDebugOn;

    /**
     * Logger constructor.
     * @param string $name
     * @param ScopeConfigInterface $scopeConfig
     * @param array $handlers
     * @param array $processors
     */
    public function __construct(
        $name,
        ScopeConfigInterface $scopeConfig,
        $handlers = array(),
        $processors = array()
    ) {
    
        $this->_isDebugOn = $scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_IS_LOGGER_ENABLED);
        parent::__construct($name, $handlers, $processors);
    }

    /**
     * @param string $message
     * @param array $context
     * @return bool
     */
    public function addDebug($message, array $context = array())
    {
        return $this->debug($message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @return bool
     */
    public function debug($message, array $context = array())
    {
        if ($this->isDebugOn()) {
            if (!is_string($message)) {
                if ($message instanceof \Magento\Framework\DataObject) {
                    $message = $message->getData();
                }
                $message = print_r($message, true);
            }
            return parent::debug($message, $context);
        }
        return false;
    }

    public function isDebugOn()
    {
        return !!$this->_isDebugOn;
    }
}
