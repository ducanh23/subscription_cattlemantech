<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 19/09/2017
 * Time: 13:35
 */
namespace Magenest\Subscription\Api;

interface GatewayResponseInterface
{
    /**
     * get payment method code
     * @return string
     */
    public function getPaymentMethod();
    /**
     * get subscription id
     * @return string
     */
    public function getSubscriptionId();

    /**
     * get status of response
     * @return string
     */
    public function getStatus();

    /**
     * @return string
     */
    public function getMessage();

    /**
     * get error messsage if it has one
     * @return string
     */
    public function getErrorMessage();
}
