<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 16/09/2017
 * Time: 10:47
 */

namespace Magenest\Subscription\Api;

interface SubscriptionManagementInterface
{
    /**
     * Get subscription box from quote
     *
     * @param $quote
     * @return mixed
     */
    public function getSubscriptionBoxFromQuote($quote);

    /**
     * Begin processing subscription orders
     *
     * @param $quote
     * @param $payment
     * @return mixed
     */
    public function process($quote, $payment);

    /**
     * Create boxes and subscribe
     *
     * @param $quote
     * @param $payment
     * @return mixed
     */
    public function subscribe($quote, $payment);

    /**
     * Subscription signup process
     *
     * @param $subscriptionBox
     * @param $payment
     * @return mixed
     */
    public function signUpSubscription($subscriptionBox, $payment);

    /**
     * Submit subscription params and get response
     *
     * @param $subscriptionBox
     * @param $payment
     * @return mixed
     */
    public function submitSubscription($subscriptionBox, $payment);

    /**
     * Create profile using gateway response
     *
     * @param $gatewayResponse
     * @param $subscriptionBox
     * @param $payment
     * @return mixed
     */
    public function createProfile($gatewayResponse, $subscriptionBox, $payment);

    /**
     * Get payment method, whether its paypal or authorizenet
     *
     * @param $code
     * @return mixed
     */
    public function getPaymentMethodInstance($code);
}
