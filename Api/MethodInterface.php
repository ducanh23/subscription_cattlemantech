<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 18/09/2017
 * Time: 15:31
 */

namespace Magenest\Subscription\Api;

interface MethodInterface
{
    /**
     * get payment method code
     * @return string
     */
    public function getCode();

    /**
     * get whether it is gateway
     * @return boolean
     */
    public function isGateway();

    /**
     * Retrieve payment method online/offline flag
     *
     * @return bool
     *
     */
    public function isOffline();

    /**
     * get block class to display manage action in customer dashboard
     * @return string
     */
    public function getActionBlockType();

    /**
     * invoked by customer to activate, cancel, suspend, reactive their recurring profile
     * @param $profile_id string
     * @param  $action string
     */
    public function invokeUserAction($profile_id, $action);

    /**
     * invoke the payment API to check weather new payment is made for a recurring profile
     * @param $profile \Magenest\Subscription\Model\Profile
     * @return \Magenest\Subscription\Model\Profile
     */
    public function updateInformationByGatewayInfo($profile);
}
