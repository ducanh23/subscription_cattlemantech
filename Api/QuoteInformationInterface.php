<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 25/08/2017
 * Time: 21:22
 */
namespace Magenest\Subscription\Api;

interface QuoteInformationInterface
{
    /**
     * Whether quote contains subscription product
     *
     * @param $quoteId
     * @return mixed'
     */
    public function isQuoteContainsSubscription($quoteId);
}
