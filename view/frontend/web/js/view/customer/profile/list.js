/**
 * Created by joel on 25/10/2017.
 */
define([
        'jquery',
        'mage/translate',
        'ko',
        'uiComponent',
        'Magento_Ui/js/modal/modal'
    ], function ($, $t, ko, Component, modal) {
        'use strict';

        function orderView(data) {
            this.increment_id = ko.observable(data.increment_id);
            this.order_link = ko.observable(data.order_link);
            this.created_date = ko.observable(data.created_date);
            this.status = ko.observable(data.status);
            this.grand_total = ko.observable(data.grand_total);
        }

        return Component.extend({
            defaults: {
                scope: 'profileList',
                template: 'Magenest_Subscription/catalog/product/subscription/list'

            },
            modalTitle: ko.observable(null),
            orderProfileCollection: [],
            orders: [],
            modalContent: ko.observable(null),
            description: 'description',
            modalWindow: '#modal-wrapper_order',

            initObservable: function () {
                this._super().observe({
                    description: 'desc',
                    orders: []
                });
                this.orderProfileCollection = window.rawprofilesOrder;
                // this.createModal(this.modalWindow);

                return this;
            },

            /**
             * Click and display modal
             * @param profileId
             * @param event
             * @param data
             */
            displayOrderList: function (profileId, event, data) {
                var self = this;
                if (event != undefined) {
                    if (event.target.className == 'action-view') {
                        if (self.orderProfileCollection.hasOwnProperty(profileId)) {
                            var ordersArr = self.orderProfileCollection[profileId];
                            if (ordersArr.length > 0) {
                                self.orders = ko.observableArray([]);
                                ko.utils.arrayForEach(ordersArr, function (item) {
                                    if (!item.hasOwnProperty('increment_id')) {
                                        return;
                                    } else {
                                        var orderObj = new orderView(item);
                                        self.orders.push(orderObj);
                                    }
                                });
                            }

                            var selector = "#" + "modal-wrapper_order-" + profileId;

                            self.createModal(selector);
                            self.showModal(selector);
                        } else {
                            return;
                        }
                    }
                }
            },

            /**
             * Create modal
             * @param element
             */
            createModal: function (element) {
                var options;

                options = {
                    'type': 'popup',
                    'modalClass': 'agreements-modal',
                    'responsive': true,
                    'innerScroll': true,
                    'trigger': '.show-modal',
                    'buttons': [
                        {
                            text: $t('Close'),
                            class: 'action secondary action-hide-popup',

                            /** @inheritdoc */
                            click: function () {
                                this.closeModal();
                            }
                        }
                    ]
                };
                modal(options, $(element));
            },

            /** Show login popup window */
            showModal: function (element) {
                $(element).modal('openModal');
            },

            /**
             * Show agreement content in modal
             *
             * @param {Object} element
             */
            showContent: function (element) {
                this.modalTitle(element.checkboxText);
                this.modalContent(element.content);
                this.showModal();
            },


        });
    }
);