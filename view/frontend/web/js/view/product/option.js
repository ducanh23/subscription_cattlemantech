/**
 * Created by joel on 31/03/2017.
 */
define([
    'jquery',
    'mage/translate',
    'uiComponent',
    'ko',
    './optioninfo'
], function ($, $t, Component, ko, OptionInfoComponent) {
    'use strict';

    ko.bindingHandlers.magenestCalendar = {
        init: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
            $(element).calendar({
                minDate: '0',
                dateFormat: 'yy-mm-dd'
            });
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

        }
    };

    function Subscription(data) {
        this.key = ko.observable(data.key);
        this.optionName = ko.observable(data.optionName);
        this.maxCycles = ko.observable(data.maxCycles);
        this.selectedUnit = ko.observable(data.selectedUnit);
        this.frequency = ko.observable(data.frequency);
        this.trialEnabled = ko.observable(data.trialEnabled);
        this.trialFrequency = ko.observable(data.trialFrequency);
        this.trialUnit = ko.observable(data.trialUnit);
        this.trialMaxCycles = ko.observable(data.trialMaxCycles);
        this.trialAmount = ko.observable(data.trialAmount);
        this.initEnabled = ko.observable(data.initEnabled);
        this.initFee = ko.observable(data.initFee);
        this.mainPeriod = data.maxCycles + ' cycle of ' + data.frequency + ' ' + data.selectedUnit;
    }

    return Component.extend({
        defaults: {
            scope: 'subscription',
            template: 'Magenest_Subscription/catalog/product/subscription/option',
        },
        dataScope: 'subscription',
        isLoading: '',
        oneTimePurchasedLabel: 'One time purchased',
        loadingImgUrl: '',
        loadingTxt: 'Loading',
        optionKey: '',
        options: [],
        mainPeriod: '',
        trialAmount: '',
        trialPeriod: '',
        initFee: '',
        isEnabled: false,
        style: '',
        a: '',
        selectedSubcriptionOption: '',
        total: 0,
        periodUnit: '',
        billingFrequency: '',
        maxBillingCycles: '',
        _trialEnabled: '',
        _trialAmount: '',
        _initFeeEnabled: '',
        _initAmount: '',



        initObservable: function () {
            this._super().observe({
                optionKey: '',
                options: [],
                mainPeriod: '',
                trialAmount: '',
                trialPeriod: '',
                initFee: '',
                isEnabled: false,
                style: '',
                isLoading: true,
                a: '',
                // selectedSubcriptionOption: '',

            });

            var self = this;
            self.isLoading(true);

            self.style(self.displayStyle);


            this.optionKey.subscribe(function (newKey) {
                window.subscribedOption = newKey;
                var event = new CustomEvent("subscribedEvent", {"detail": newKey});
                // Dispatch/Trigger/Fire the event
                document.dispatchEvent(event);
                if (newKey == -1) {
                    self.isEnabled(false);
                } else {
                    ko.utils.arrayForEach(self.options(), function (item) {
                        if (item.key() == newKey) {
                            self.mainPeriod(item.maxCycles() + ' cycle of ' + item.frequency() + ' ' + item.selectedUnit());
                            if (item.trialEnabled() === 'yes') {
                                self.trialPeriod(item.trialMaxCycles() + ' cycle of ' + item.trialFrequency() + ' ' + item.trialUnit());
                                self.trialAmount(item.trialAmount());
                            }

                            if (item.initEnabled() === 'yes') {
                                self.initFee(item.initFee());
                            }
                            self.isEnabled(true);
                        }
                    });
                }
            });

            // this.selectedSubcriptionOption.subscribe(function (v) {
            //     console.log(self.a);
            //     self.addOptionInfo();
            // });

            this.initOptions();

            return this;
        },


        addOptionInfo: function () {
            var optioninfo = OptionInfoComponent();
            optioninfo.index = this.total;
            this.insertChild(optioninfo, optioninfo.index);
            // console.log(formbuilder.index);
        },

        /**
         * stop loader
         */
        stopLoader: function () {
            console.log('stop loader');
            console.log(self.a);

            this.isLoading(false);
        },

        /**
         * init the options
         */
        initOptions: function () {
            var self = this;
            var isSetDefault = true;
            var oneTimePurchase = self.oneTimePurchasedLabel;
            self.options.push({
                key: ko.observable(-1),
                optionName: ko.observable(oneTimePurchase)
            });

            for (var i = self.rawData.length - 1; i >= 0; i--) {
                self.rawData[i].key = i;
                var subsItem = new Subscription(self.rawData[i]);
                if (subsItem.mainPeriod === $('#currentMainPeriod').val()) {
                    self.optionKey(i);
                    isSetDefault = false;
                }
                self.options.push(subsItem);
            }
            if (isSetDefault) {
                self.optionKey(-1);
            }
        },

        /**
         *
         * @param item
         */
        chooseRadio: function (item) {
            var self = this;

            var itemKey = item.key();
            self.optionKey(itemKey);
            return true;
        },

        /**
         * If the option is once time purchase then show the add to cart button,
         * in another cases, show the subscribe button
         * @param item
         */
        chooseOption: function (item) {
            var self = this;
            var i;
            var optioninfo = OptionInfoComponent();
            optioninfo.index = self.total;
            self.destroyChildren(optioninfo);
            // this.insertChild(optioninfo, optioninfo.index);
            for (i = 0; i < item.rawData.length; i++) {
                if (item.rawData[i].key == self.selectedSubcriptionOption[0]) {
                    // console.log(item.rawData[i]);
                    self.periodUnit = item.rawData[i].selectedUnit;
                    self.billingFrequency = item.rawData[i].frequency;
                    self.maxBillingCycles = item.rawData[i].maxCycles;
                    self._trialEnabled = item.rawData[i].trialEnabled;
                    self._trialAmount = item.rawData[i].trialAmount;
                    self._initFeeEnabled = item.rawData[i].initEnabled;
                    self._initAmount = item.rawData[i].initFee;
                    self.addOptionInfo();

                }

            }


        },

    })
});