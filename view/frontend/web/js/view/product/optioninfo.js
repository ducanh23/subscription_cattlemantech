/*jshint browser:true jquery:true*/
/*global alert*/
define(
    [
        'jquery',
        'uiComponent',
        'underscore',
        'ko',
    ],
    function ($, Component, _, ko,) {
        'use strict';

        return Component.extend({
            defaults: {
                template: "Magenest_Subscription/catalog/product/subscription/optioninfoa",
                index: "",
            },

            /**
             * Initialize observable properties
             */
            initObservable: function () {
                var self = this;
                this._super();
                return this;
            },


        });
    }
);