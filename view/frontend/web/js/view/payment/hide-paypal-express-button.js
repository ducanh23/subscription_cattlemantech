/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'underscore',
        'jquery',
        'uiComponent',
        'Magento_Customer/js/customer-data',
        'Magento_Ui/js/model/messageList'
    ],
    function (
        _,
        $,
        Component,
        customerData,
        messageList
    ) {
        'use strict';

        // State of PayPal module initialization
        var clientInit = false;

        return Component.extend({

            defaults: {
                updateUrl : '',
                hasSubscribedItem : false
            },

            /**
             * @returns {Object}
             */
            initialize: function () {
                this._super();
                this.checkQuote();

                return this;
            },

            /**
             *

             * @returns {String}
             */
            checkQuote: function () {
                var self = this;
                $('[data-block="minicart"]').on('contentUpdated', function () {
                    console.log('update the cart content');
                    var cartData = customerData.get('cart');
                    if (cartData().hasOwnProperty('isSubscription'))  {
                        if (cartData().isSubscription) {
                         if ( $('div.paypal.checkout').length > 0) {
                           var paypalExpressButtons =  $('div.paypal.checkout');
                             $.each(paypalExpressButtons,function (index,element) {
                                 $(element).hide();
                                // $(element).find('input').attr('data-checkout-url' , self.updateUrl);
                             }) ;
                         }
                       } else  {
                            if ( $('div.paypal.checkout').length > 0) {
                                var paypalExpressButtons =  $('div.paypal.checkout');
                                $.each(paypalExpressButtons,function (index,element) {
                                    $(element).show();
                                   // $(element).find('input').attr('data-checkout-url' , 'paypal/express/start');
                                }) ;
                            }
                        }
                    }
                });
            }
        });
    }
);
