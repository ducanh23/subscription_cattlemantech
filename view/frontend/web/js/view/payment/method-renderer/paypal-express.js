/**
 * Created by thuy on 27/08/2017.
 */
define(
    [
        'Magento_Paypal/js/view/payment/method-renderer/paypal-express-abstract',
        'ko'
    ],
    function (Component , ko) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magento_Paypal/payment/paypal-express',
                quoteCurrencyCode : ko.observable(window.checkoutConfig.quoteCurrencyCode),
                currencyIsSupportedByPaypal : ko.observable(window.checkoutConfig.currencyIsSupportedByPaypal),

            },
            isDisable: '',

            /** Init observable variables */
            initObservable: function () {
                this._super()
                    .observe(['billingAgreement','isDisable']);
                if (this.currencyIsSupportedByPaypal() == 0 || window.checkoutConfig.mixedCart)  {
                    this.isDisable(true);
                } else {
                    this.isDisable(false);
                }
                return this;
            },

            /**
             * whether the cart has subscribed item
             */
            hasSubscription: ko.observable(window.checkoutConfig.checkoutWithSubscription),

            /**
             * method for sign up recurring profile in Paypal
             */
            signupByPayPal: function () {
                window.location = BASE_URL.concat('subscription/express/start');
            }
        });
    }
);
