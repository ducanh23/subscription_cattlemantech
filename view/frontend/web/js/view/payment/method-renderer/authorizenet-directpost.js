/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
        'jquery',
        'Magento_Payment/js/view/payment/iframe',
        'mage/translate',
        'Magento_Checkout/js/action/redirect-on-success',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/payment/additional-validators',
        'ko'
    ],
    function ($, Component, $t, redirectOnSuccessAction, fullScreenLoader, additionalValidators, ko) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Magento_Authorizenet/payment/authorizenet-directpost',
                timeoutMessage: $t('Sorry, but something went wrong. Please contact the seller.'),
                hasSubscription: ko.observable(window.checkoutConfig.checkoutWithSubscription),
                quoteCurrencyCode: ko.observable(window.checkoutConfig.quoteCurrencyCode),
                currencyIsSupportedByPaypal: ko.observable(window.checkoutConfig.quoteCurrencyCode),
            },
            isDisable: false,
            placeOrderHandler: null,
            validateHandler: null,

            /**
             * Set list of observable attributes
             * @returns {exports.initObservable}
             */
            initObservable: function () {
                this._super()
                    .observe([
                        'creditCardType',
                        'creditCardExpYear',
                        'creditCardExpMonth',
                        'creditCardNumber',
                        'creditCardVerificationNumber',
                        'creditCardSsStartMonth',
                        'creditCardSsStartYear',
                        'creditCardSsIssue',
                        'selectedCardType',
                        'isDisable'
                    ]);

                if (window.checkoutConfig.mixedCart) {
                    this.isDisable(true);
                } else {
                    this.isDisable(false);
                }
                return this;
            },

            /**
             * @param {Object} handler
             */
            setPlaceOrderHandler: function (handler) {
                this.placeOrderHandler = handler;
            },

            /**
             * @param {Object} handler
             */
            setValidateHandler: function (handler) {
                this.validateHandler = handler;
            },

            /**
             * @returns {Object}
             */
            context: function () {
                return this;
            },

            /**
             * @returns {Boolean}
             */
            isShowLegend: function () {
                return true;
            },

            /**
             * @returns {String}
             */
            getCode: function () {
                return 'authorizenet_directpost';
            },

            /**
             * @returns {Boolean}
             */
            isActive: function () {
                return true;
            },

            /**
             * Before signup
             * @returns {exports}
             */
            beforeSignup: function () {
                fullScreenLoader.startLoader(true);
                this.signup();

                return this;
            },

            /**
             * Created by thuy on 29/09/2017.
             */
            signup: function () {
                var self = this;

                if (this.validateHandler() && additionalValidators.validate()) {
                    fullScreenLoader.startLoader(true);

                    this.isPlaceOrderActionAllowed(false);

                    var result = {
                        "ccNumber": $('#authorizenet_directpost_cc_number').val(),
                        "expMonth": $('#authorizenet_directpost_expiration').val(),
                        "expYear": $('#authorizenet_directpost_expiration_yr').val(),
                        "ccId": $('#authorizenet_directpost_cc_cid').val(),
                        'payment_method': 'authorizenet_directpost'
                    };

                    var requestUrl = BASE_URL.concat('subscription/subscribe/signup');
                    $.ajax({
                        type: "POST",
                        url: requestUrl,
                        data: {
                            result: result
                        },
                        success: function (res) {
                            $('.loading-mask').hide();

                            if (res.code == 'success') {
                                redirectOnSuccessAction.execute();
                            } else if (res.code == 'error') {
                                self.messageContainer.addErrorMessage({
                                    message: $t('Sorry, we couldn\'t create your subscription profile. If this problem persists, please contact the store owner.')
                                });
                            } else {
                                self.messageContainer.addErrorMessage({
                                    message: $t('Sorry, something went wrong while submitting your subscription profile. If this problem persists, please contact the store owner.')
                                });
                            }
                        },
                        error: function (res) {
                            $('.loading-mask').hide();
                            self.messageContainer.addErrorMessage({
                                message: $t('Sorry, something went wrong while submitting your subscription profile. If this problem persists, please contact the store owner.')
                            });
                        },
                        async: false
                    });
                } else {
                    fullScreenLoader.stopLoader(true);
                }
            }

        });
    });
