define([
    'jquery',
    'mage/translate',
    'uiComponent',
    'Magento_Ui/js/modal/modal',
    'ko'
], function ($, $t, Component, modal, ko) {
    'use strict';
    return Component.extend({
        defaults: {
            template: 'Magento_Authorizenet/payment/authorizenet-directpost',
            elementId : 'subscription-checkout-popup',
            invalidStartDateElementId : 'invalid-start-date-popup',
            hitMessage: $t('Sorry, but you have to remove other products to subscribe to a billing plan'),
            hintPopup: ''
        },

        showMessage: function () {
            var self = this;

            if (window.checkoutConfig.mixedCart ) {
                var popupContainer = $('#' + self.elementId);
                self.hintPopup = popupContainer.modal({
                    title: $.mage.__('Remove non subscribed-product'),
                    type: 'popup',
                    autoOpen: true,
                    buttons: [{
                        text: $.mage.__('OK'),
                        class: 'invalid-subscription-popup checkout',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                });
            }

            //if there is invalidate start date then
            var subscriptionValidate= JSON.parse(window.checkoutConfig.validateSubscription);
            if (!subscriptionValidate.valid ) {
                var popupContainer = $('#' + self.invalidStartDateElementId);
                self.hintPopup = popupContainer.modal({
                    title: $.mage.__('Start date of subscription is invalid'),
                    type: 'popup',
                    autoOpen: true,
                    buttons: [{
                        text: $.mage.__('OK'),
                        class: 'invalid-subscription-popup checkout',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                });
            }
        }
    })
});