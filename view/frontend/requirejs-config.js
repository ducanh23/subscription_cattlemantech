/**
 * Created by Pham Quang Hau on 11/03/2016.
 */
var config = {
    "map": {
        "*": {
            calendar: "mage/calendar",
            'Magento_Paypal/template/payment/paypal-express.html': 'Magenest_Subscription/template/payment/paypal-express.html',
            'Magento_Paypal/js/view/payment/method-renderer/paypal-express': 'Magenest_Subscription/js/view/payment/method-renderer/paypal-express',
            magenest_subscription: "Magenest_Subscription/js/magenest_subscription",
            "Magento_Authorizenet/js/view/payment/method-renderer/authorizenet-directpost": "Magenest_Subscription/js/view/payment/method-renderer/authorizenet-directpost",
            "Magento_Authorizenet/template/payment/authorizenet-directpost.html": "Magenest_Subscription/template/payment/authorizenet-directpost.html"
        }
    }
};