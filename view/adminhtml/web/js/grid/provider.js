/**
 * Created by thuy on 13/08/2017.
 */
define([
    'jquery',
    'underscore',
    'mageUtils',
    'Magento_Ui/js/grid/provider'
], function ($,_, utils,Provider) {
    'use strict';

    return Provider.extend({
        /**
         * Reload grid
         * @returns {exports}
         */
        assignSelected: function () {
            var self = this;
            console.log(arguments);
            var selections = arguments[1];
            var saveUrl = self.save_url;
            selections.form_key = window.formKey;

            $.ajax({
                url: saveUrl,
                data: selections,
                beforeSend: function () {
                    $('body').trigger('processStart');
                },
                success: function (res) {
                    console.log(res);
                    $('body').trigger('processStop');
                    return this;
                }
            });
        }
    });
});
