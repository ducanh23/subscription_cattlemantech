/**
 * Created by thuy on 14/08/2017.
 */
define([
    'jquery',
    'underscore',
    'ko',
    'Magento_Ui/js/grid/columns/multiselect'
], function ($, _, ko, Column) {
    'use strict';

    return Column.extend({
        initialize: function () {
            this._super()
                .initObservable()
                .initModules()
                .initStatefull()
                .initLinks()
                .initUnique();

            var self = this;

            // todo
            var assignProducts = JSON.parse(window.assignProductsArr);
            console.log(assignProducts);

            if (assignProducts.length > 0) {
                ko.utils.arrayForEach(assignProducts, function (item) {
                    self.select(item.toString(), true);
                });
            }

            return this;
        }
    });
});
