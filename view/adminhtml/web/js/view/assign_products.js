/**
 * Created by joel on 14/10/2017.
 */
define([
    'jquery'
], function ($){
    'use strict';

    return function(config, node) {
        setInterval(function() {
            var buttons = $('.template-product-listing').find('button');
            $.each(buttons, function () {
                $(this).attr('type', 'button');
            });

            $('.template-product-listing').attr('buttonjs-applied', 1);

            $('.action-select').css('width', '70%');
        }, 200);
    }
});