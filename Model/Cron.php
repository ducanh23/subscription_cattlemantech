<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 31/03/2016
 * Time: 15:02
 */
namespace Magenest\Subscription\Model;

use Magenest\Subscription\Logger\Logger;

class Cron
{
    /**
     * @var Cron\Profile
     */
    protected $_profileCron;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * Cron constructor.
     * @param Cron\Profile $profileCron
     * @param Logger $logger
     */
    public function __construct(
        \Magenest\Subscription\Model\Cron\Profile $profileCron,
        Logger $logger
    ) {
        $this->_profileCron = $profileCron;
        $this->_logger = $logger;
    }

    /**
     * collect the abandoned cart
     */
    public function getProfilesInfoCron()
    {
        $this->_logger->debug('Get profiles info cron');
        $this->_profileCron->run();
    }
}
