<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 25/03/2016
 * Time: 20:59
 */
namespace Magenest\Subscription\Model;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Status extends AbstractSource
{
    const STATUS_ACTIVE = 'Active';
    const STATUS_PENDING = 'Pending';
    const STATUS_CANCELLED = 'Cancelled';
    const STATUS_SUSPENDED = 'Suspended';
    const STATUS_EXPIRED = 'Expired';

    /**
     * @return array
     */
    public static function getOptionArray()
    {
        return [
            self::STATUS_ACTIVE => __('Active'),
            self::STATUS_PENDING => __('Pending'),
            self::STATUS_CANCELLED => __('Cancelled'),
            self::STATUS_SUSPENDED => __('Suspended'),
            self::STATUS_EXPIRED => __('Expired')
        ];
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $result = [];
        foreach (self::getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }
        return $result;
    }

    /**
     * @param $optionId
     * @return string
     */
    public function getOptionGrid($optionId)
    {
        $options = self::getOptionArray();
        if ($optionId == self::STATUS_ACTIVE) {
            $html = '<span class="grid-severity-notice"><span>' . $options[$optionId] . '</span>'.'</span>';
        } else {
            $html = '<span class="grid-severity-critical"><span>' . $options[$optionId] . '</span></span>';
        }

        return $html;
    }
}
