<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 31/03/2016
 * Time: 15:13
 */

namespace Magenest\Subscription\Model\Cron;

use Magenest\Subscription\Api\SubscriptionManagementInterface;
use Magenest\Subscription\Model\ProfileFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magenest\Subscription\Logger\Logger;
use Magenest\Subscription\Helper\Data;
use Magento\Store\Model\ScopeInterface;

class Profile
{
    /**
     * @var String
     */
    protected $_api;

    /**
     * @var ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\Event\Manager
     */
    protected $_eventManger;

    /**
     * @var SubscriptionManagementInterface
     */
    protected $_subscriptionManager;

    /**
     * Profile constructor.
     * @param Data $data
     * @param ProfileFactory $profileFactory
     * @param Logger $logger
     * @param ScopeConfigInterface $scopeConfig
     * @param SubscriptionManagementInterface $subscriptionManagement
     * @param \Magento\Framework\Event\Manager $eventManager
     */
    public function __construct(
        Data $data,
        ProfileFactory $profileFactory,
        Logger $logger,
        ScopeConfigInterface $scopeConfig,
        SubscriptionManagementInterface $subscriptionManagement,
        \Magento\Framework\Event\Manager $eventManager
    ) {
    
        $this->_subscriptionManager = $subscriptionManagement;
        $this->_helper = $data;
        $this->_logger = $logger;
        $this->_scopeConfig = $scopeConfig;
        $this->_eventManger = $eventManager;
        $this->_profileFactory = $profileFactory;
    }

    /**
     * The method is invoked by cron job
     * it get information from payment gateway to see if the new payment is made
     * If the payment is made for the recurring profile,then corresponding profile is updated,
     * new order is created
     */
    public function run()
    {
        $profiles = $this->_profileFactory->create()->getCollection();

        if ($profiles->getSize() > 0) {
            /** @var \Magenest\Subscription\Model\Profile $profile */
            foreach ($profiles as $profile) {
                try {
                    $methodCode = $profile->getMethodCode();
                    //update the data if the payment method is paypal express
                    if ($methodCode == 'paypal_express') {
                        $paymentMethodInstance = $this->_subscriptionManager->getPaymentMethodInstance($methodCode);

                        if (is_object($paymentMethodInstance)) {
                            $cycles_completed = $profile->getCyclesCompleted();
                            // Update profile
                            $profile = $paymentMethodInstance->updateInformationByGatewayInfo($profile);

                            //whether new payment is made
                            $newCycleCompleted = $profile->getCyclesCompleted();

                            if ($newCycleCompleted > $cycles_completed) {
                                $profile->generateOrderBaseOnBox();
                                // Save new current billing status
                                $nextBillingDate = $profile->calculateNextBillingDate();
                                $nextBillingDateObj = \DateTime::createFromFormat('Y-m-d H:i:s', $nextBillingDate);
                                $savedNextBilling = $nextBillingDateObj->format('Y-m-d');

                                $cyclesCompleted = intval($profile->getData('cycles_completed'));
                                $cyclesCompleted++;

                                $cyclesRemaining = intval($profile->getData('cycles_remaining'));
                                $cyclesRemaining--;

                                $profile->setData([
                                    'next_billing_date' => date('Y-m-d', $savedNextBilling),
                                    'cycles_completed' => $cyclesCompleted,
                                    'cycles_remaining' => $cyclesRemaining
                                ])->save();
                            }
                        }
                    }
                } catch (\Exception $exception) {
                    $this->_logger->error($exception->getMessage());
                    //generate report for both admin and customer
                }
            }

        }
    }
}
