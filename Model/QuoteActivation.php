<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 29/08/2017
 * Time: 22:44
 */

namespace Magenest\Subscription\Model;

class QuoteActivation extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('quote', 'id');
    }

    /**
     * @param $quote_id
     * @param bool $active
     * @return \Zend_Db_Statement_Interface
     */
    public function saveActiveStatus($quote_id, $active = false)
    {
        $adapter = $this->getConnection('write');
        $quoteTable = $this->getConnection()->getTableName('quote');

        if ($active) {
            $sql ="UPDATE {$quoteTable} SET `is_active`=1 WHERE `entity_id`={$quote_id}";
        } else {
            $sql ="UPDATE {$quoteTable} SET `is_active`=0 WHERE `entity_id`={$quote_id}";
        }

        $rows = $adapter->query($sql);
        return $rows;
    }
}
