<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 13/10/2017
 * Time: 14:10
 */
namespace Magenest\Subscription\Model\Cart;

/**
 * Class RequestInfoFilter used for filtering data from a request
 */
class RequestInfoFilter
{
    /**
     * @var \Magenest\Subscription\Helper\Util
     */
    protected $helper;

    /**
     * @var \Magenest\Subscription\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * RequestInfoFilter constructor.
     * @param \Magenest\Subscription\Helper\Util $util
     * @param \Magenest\Subscription\Helper\Data $dataHelper
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Magenest\Subscription\Helper\Util $util,
        \Magenest\Subscription\Helper\Data $dataHelper,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        $this->helper = $util;
        $this->dataHelper = $dataHelper;
        $this->customerSession = $customerSession;
    }

    /**
     * Filters the data with values from filterList
     * @param \Magento\Framework\DataObject $params
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function filter(\Magento\Framework\DataObject $params)
    {
        //if the quote already had subscription product then you cant add any other item
        $isQuoteContainsSubscription = $this->helper->isQuoteContainsSubscriptionItem();
        if ($isQuoteContainsSubscription) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Item with subscription option can be purchased standalone only.'));
        }

        //if the quote already had item then customer add a subscription item
        $subscription_option = $params->getData('subscription_option');
        if ($subscription_option =='' || $subscription_option == null || $subscription_option === '-1' || $subscription_option === -1) {
            return $this;
        }
        $quote = $this->helper->getQuote();
        if ($quote) {
            $items = $quote->getAllItems();
            if (!empty($items)) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Item with subscription option can be purchased standalone only.'));
            }
        }

        //validate the login requirement
        $isLoginMandatory  = $this->dataHelper->isCustomerLoginMandatory();
        if ($isLoginMandatory) {
            if (!$this->customerSession->isLoggedIn()) {
                throw new \Magento\Framework\Exception\LocalizedException(__('You can\'t checkout with this product as a guest.'));
            }
        }
        return $this;
    }
}
