<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 09/03/2016
 * Time: 01:58
 */
namespace Magenest\Subscription\Model\Entity\Attribute\Source;

use \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class Trial extends AbstractSource
{
    const VALUE_DAY = 0;

    const VALUE_WEEK = 1;

    const VALUE_MONTH = 2;

    const VALUE_SEMI_MONTH = 3;

    const VALUE_YEAR = 4;

    /**
     * @return array|null
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['label' => __('Day'), 'value' => self::VALUE_DAY],
                ['label' => __('Week'), 'value' => self::VALUE_WEEK],
                ['label' => __('Month'), 'value' => self::VALUE_MONTH],
                ['label' => __('SemiMonth'), 'value' => self::VALUE_SEMI_MONTH],
                ['label' => __('Year'), 'value' => self::VALUE_YEAR]
            ];
        }
        return $this->_options;
    }
}
