<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 16/09/2017
 * Time: 10:41
 */

namespace Magenest\Subscription\Model;

use Magenest\Subscription\Api\SubscriptionManagementInterface;
use Magenest\Subscription\Model\SubscriptionBoxFactory;
use Magenest\Subscription\Model\ProfileFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Magenest\Subscription\Model\OrderManagement;

class SubscriptionManagement implements SubscriptionManagementInterface
{
    /** allow only one subscribed product in the cart */
    const MODE_STANDALONE = 1;

    /** allow one subscription box and many non-subscribed products in the cart */
    const MODE_SUBSCRIPTION_BOX = 2;

    /** allow many subscription box and many non-subscribed products in the cart */
    const MODE_SUBSCRIPTION_BOXES = 3;

    protected $mode = self::MODE_STANDALONE;

    /**
     * @var \Magenest\Subscription\Model\SubscriptionBoxFactory
     */
    protected $boxFactory;

    /**
     * @var \Magenest\Subscription\Model\ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var array
     */
    protected $lastAddedProfiles = [];

    /**
     * @var string
     */
    protected $lastOrderId;

    /**
     * @var Method\Factory
     */
    protected $methodFactory;

    /**
     * @var \Magenest\Subscription\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Session\Generic
     */
    protected $subscriptionSession;

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $quoteRepository;

    /**
     * @var \Magenest\Subscription\Model\OrderManagement
     */
    protected $orderManagement;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    protected $util;

    /**
     * SubscriptionManagement constructor.
     * @param Method\Factory $methodFactory
     * @param \Magenest\Subscription\Helper\Data $helper
     * @param \Magenest\Subscription\Model\ProfileFactory $profileFactory
     * @param \Magenest\Subscription\Model\SubscriptionBoxFactory $subscriptionBoxFactory
     * @param \Magento\Framework\Session\Generic $subscriptionSession
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param OrderFactory $orderFactory
     * @param ToOrderItemConverter $toOrderItemConverter
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Quote\Api\CartRepositoryInterface $quoteRepository
     * @param \Magenest\Subscription\Model\OrderManagement $orderManagement
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param array $data
     */
    public function __construct(
        \Magenest\Subscription\Model\Method\Factory $methodFactory,
        \Magenest\Subscription\Helper\Data $helper,
        ProfileFactory $profileFactory,
        SubscriptionBoxFactory $subscriptionBoxFactory,
        \Magento\Framework\Session\Generic $subscriptionSession,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        OrderFactory $orderFactory,
        ToOrderItemConverter $toOrderItemConverter,
        StoreManagerInterface $storeManager,
        \Magento\Quote\Api\CartRepositoryInterface $quoteRepository,
        OrderManagement $orderManagement,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magenest\Subscription\Helper\Util $util,
        $data = []
    ) {
        $this->methodFactory = $methodFactory;
        $this->boxFactory = $subscriptionBoxFactory;
        $this->_profileFactory = $profileFactory;
        $this->helper = $helper;
        $this->subscriptionSession = $subscriptionSession;
        $this->objectManager = $objectManager;
        $this->orderFactory = $orderFactory;
        $this->quoteItemToOrderItem = $toOrderItemConverter;
        $this->storeManager = $storeManager;
        $this->quoteRepository = $quoteRepository;
        $this->orderManagement = $orderManagement;
        $this->checkoutSession = $checkoutSession;
        $this->util = $util;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    public function getSubscriptionBoxFromQuote($quote)
    {
        $boxes = [];
        $shippingAmt = 0;
        $baseShippingAmt = 0;
        $taxAmt = 0;
        $baseTaxtAmt = 0;
        $firstTimeDiscount = 0;
        $subTotal = 0;
        $grandTotal = 0;
        /** @var array $boxItemIds */
        $boxItemIds = [];

        $allItems = $quote->getAllVisibleItems();
        $isVirtual = $quote->getIsVirtual();

        if ($isVirtual) {
            $quoteAddress = $quote->getBillingAddress();
        } else {
            $quoteAddress = $quote->getShippingAddress();
            $shippingAmt = $quoteAddress->getShippingAmount();
            $baseShippingAmt = $quoteAddress->getBaseShippingAmount();
        }
        $baseCurrencyCode = $quote->getBaseCurrencyCode();
        $quoteCurrencyCode = $quote->getQuoteCurrencyCode();
        $storeId = $quote->getStoreId();
        $taxAmt = $quoteAddress->getTaxAmount();
        $baseTaxtAmt = $quoteAddress->getBaseTaxAmount();

        $subTotal = $quoteAddress->getSubtotal();
        $baseSubTotal = $quoteAddress->getBaseSubtotal();
        $grandTotal = $quoteAddress->getGrandTotal();
        $baseGrandTotal = $quoteAddress->getBaseGrandTotal();

        if ($allItems) {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($allItems as $item) {
                $quoteItemId = $item->getId();
                $boxItemIds[] = $quoteItemId;

                $itemOptions = $item->getOptions();
                $subscriptionOption = null;

                foreach ($itemOptions as $option) {
                    if ($option->getData('code') == 'subscription_options') {
                        $subscriptionOption = $option;
                    }
                }

                if (is_object($subscriptionOption)) {
                    $storedValue = $subscriptionOption->getValue();
                    $subscriptionValue = $this->util->destringifyArray($storedValue);
                    $boxData = [];

                    //init the value for the box
                    $boxData['option_name'] = (isset($subscriptionValue['optionName'])) ? $subscriptionValue['optionName'] : '';
                    $boxData['selected_unit'] = (isset($subscriptionValue['selectedUnit'])) ? $subscriptionValue['selectedUnit'] : '';
                    $boxData['frequency'] = (isset($subscriptionValue['frequency'])) ? $subscriptionValue['frequency'] : '';
                    $boxData['max_cycles'] = (isset($subscriptionValue['maxCycles'])) ? $subscriptionValue['maxCycles'] : '';
                    $boxData['trial_enabled'] = (isset($subscriptionValue['trialEnabled'])) ? $subscriptionValue['trialEnabled'] : '';
                    $boxData['trial_amount'] = (isset($subscriptionValue['trialAmount'])) ? $subscriptionValue['trialAmount'] : '';
                    $boxData['trial_unit'] = (isset($subscriptionValue['trialUnit'])) ? $subscriptionValue['trialUnit'] : '';
                    $boxData['trial_frequency'] = (isset($subscriptionValue['trialFrequency'])) ? $subscriptionValue['trialFrequency'] : '';
                    $boxData['trial_max_cycles'] = (isset($subscriptionValue['trialMaxCycles'])) ? $subscriptionValue['trialMaxCycles'] : '';
                    $boxData['init_enabled'] = (isset($subscriptionValue['initEnabled'])) ? $subscriptionValue['initEnabled'] : '';
                    $boxData['init_fee'] = (isset($subscriptionValue['initFee'])) ? $subscriptionValue['initFee'] : '';
                    $boxData['start_date'] = (isset($subscriptionValue['start_date'])) ? $subscriptionValue['start_date'] : '';
                    $boxData['mode'] = (isset($subscriptionValue['mode'])) ? $subscriptionValue['mode'] : '';
                    $boxData['currency_code'] = $quoteCurrencyCode;
                    $boxData['store_id'] = $storeId;
                    $boxData['subscriber_name'] = $quote->getBillingAddress()->getName();

                    if ($this->mode == self::MODE_STANDALONE) {
                        $boxData['description'] = $item->getProduct()->getName() . ' ' . $boxData['option_name'];
                    }

                    if (is_array($subscriptionValue)) {
                        /** @var \Magenest\Subscription\Model\SubscriptionBox $subscriptionBox */
                        $subscriptionBox = $this->boxFactory->create()->loadByItemIds($boxItemIds);
                        $subscriptionBox->setSubscriptionInformation($boxData);
                        $subscriptionBox->addData($boxData);
                        $subscriptionBox->setItemIds($boxItemIds);

                        $subscriptionBox->setQuoteId($quote->getId());
                        //in case the quote contains only 1 subscribed item, we go with simplest shipping and tax calculation

                        if ($this->mode == self::MODE_STANDALONE) {
                            $subscriptionBox->setShippingAmt($shippingAmt);
                            $subscriptionBox->setBaseShippingAmt($baseShippingAmt);

                            $subscriptionBox->setTaxAmt($taxAmt);
                            $subscriptionBox->setBaseTaxAmt($baseTaxtAmt);

                            $subscriptionBox->setSubTotal($subTotal);
                            $subscriptionBox->setGrandTotal($grandTotal);
                            $subscriptionBox->save();
                        }

                        $boxes[] = $subscriptionBox;
                    }
                }
            }
        }

        return $boxes;
    }

    /**
     * get currency symbol
     * @return mixed
     */
    protected function getCurrencySymbol()
    {
        return $this->storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();
    }

    /**
     * Subscribe to a payment gateway and form boxes
     *
     * @param $quote
     * @param $payment
     */
    public function subscribe($quote, $payment)
    {
        /** clear the last profile id in subscription session */
        $this->subscriptionSession->clearStorage();
        $lastAddedProfileIds = [];
        $subscribedBoxes = $this->getSubscriptionBoxFromQuote($quote);

        if (count($subscribedBoxes) > 0) {
            //subscribe the box
            foreach ($subscribedBoxes as $box) {
                $profile = $this->signUpSubscription($box, $payment);
                $lastAddedProfileIds[] = $profile->getId();
            }

            $this->subscriptionSession->setLastAddedProfileIds($lastAddedProfileIds);
//            $this->checkoutSession->clearQuote()->clearStorage();
        }
    }


    /**
     * create recurring profile in gateway and use the response of gateway to create profile in Magento side
     *
     */
    public function signUpSubscription($subscriptionBox, $payment)
    {
        $gatewayResponse = $this->submitSubscription($subscriptionBox, $payment);
        $profile = $this->createProfile($gatewayResponse, $subscriptionBox, $payment);
        $enableCreateOrder = $this->helper->isAllowNominalOrderCreation();

        if ($enableCreateOrder) {
            $nominalOrder = $this->createNominalOrder($subscriptionBox, $payment);
            $profile->addData(['order_id' => $nominalOrder->getId()]);
            $profile->addSequenceOrder($nominalOrder->getId());

            // create invoice
            $this->createBaseInvoice($nominalOrder, $profile);

        }

        $quote = $this->quoteRepository->get($subscriptionBox->getQuoteId())->setIsActive(false);
        $this->quoteRepository->save($quote);
        return $profile;
    }

    /**
     * process subscription information in the payment gateway
     * @param $subscriptionBox
     * @param $payment
     */
    public function submitSubscription($subscriptionBox, $payment)
    {
        $paymentMethod = $payment->getPaymentMethod();

        $paymentMethodInstance = $this->getPaymentMethodInstance($paymentMethod);
        if ($paymentMethodInstance) {
            $response = $paymentMethodInstance->submitPlan($subscriptionBox, $payment);
            return $response;
        }
    }

    /**
     * create profile in Magento side
     * @param $gatewayResponse
     * @param \Magenest\Subscription\Model\SubscriptionBox $subscriptionBox
     * @param $payment
     * @return \Magenest\Subscription\Model\Profile
     */
    public function createProfile($gatewayResponse, $subscriptionBox, $payment)
    {
        // subscription variable init
        $quote = $this->quoteRepository->get($subscriptionBox->getQuoteId())->setIsActive(false);
        $profileModel = $this->_profileFactory->create();
        $subscriptionId = $gatewayResponse->getSubscriptionId();
        $status = $gatewayResponse->getStatus();
        $subscriber_name = $subscriptionBox->getSubscriberName();
        if (!$subscriber_name) {
            $subscriber_name = $quote->getBillingAddress()->getFirstname() . ' ' . $quote->getBillingAddress()->getMiddlename() . ' ' . $quote->getBillingAddress()->getLastname();
        }
        $startDate = $subscriptionBox->getStartDate();
        $nextBillingDate = $subscriptionBox->getNextBillingDate();
        $cycleRemaining = $subscriptionBox->getCycles();
        $period_unit = $subscriptionBox->getPeriodUnit();
        $frequency = $subscriptionBox->getFrequency();
        $cycles = $subscriptionBox->getCycles();
        $amount = $subscriptionBox->getAmount();
        $trial_info = $subscriptionBox->getTrialInfo();
        $customerId = $subscriptionBox->getCustomerId();
        $currency_code = $subscriptionBox->getCurrencyCode();
        $store_id = $subscriptionBox->getStoreId();
        $shipping_info = '';

        $profileData = [
            // Response general fields
            'profile_id' => $subscriptionId,
            'state' => $status,
            'suspension_threshold' => -1,
            'aggregate_amount' => -1,
            'aggregate_optional_amount' => -1,
            'final_payment_duedate' => -1,

            // Profile Detail fields
            'subscriber_name' => $subscriber_name,
            'start_datetime' => $startDate,

            // Summary detail fields
            'next_billing_date' => $nextBillingDate,
            'cycles_completed' => 0,
            'cycles_remaining' => $cycleRemaining,
            'outstanding_balance' => -1,
            'failed_payments' => -1,
            'trial_amt_paid' => -1,
            'regular_amt_paid' => -1,

            // Billing period fields
            'period_unit' => $period_unit,
            'period_frequency' => $frequency,
            'period_max_cycles' => $cycles,
            'billing_amount' => $amount,
            'tax_amount' => $subscriptionBox->getTaxFee(),
            'shipping_amount' => $subscriptionBox->getShippingFee(),

            // Trial and init info
            'trial_info' => serialize($trial_info),
            //Trial Information
            'trial_billing_period' => $subscriptionBox->getTrialUnit(),
            'trial_billing_frequency' => $subscriptionBox->getTrialFrequency(),
            'trial_billing_cycles' => $subscriptionBox->getTrialCycles(),
            'trial_amt' => $subscriptionBox->getTrialAmount(),

            //Init Amount
            'init_amt' => $subscriptionBox->getInitFee(),
            'currency_code' => $currency_code,
            'store_id' => $store_id,
            //Shipping Information
            'additional_info' => serialize($shipping_info)
        ];

        if ($this->mode == self::MODE_STANDALONE) {
            $profileData['shipping_amount'] = $subscriptionBox->getShippingAmt();
            $profileData['tax_amount'] = $subscriptionBox->getTaxAmt();
        }

        //If the subscription box is not
        $isVirtual = $subscriptionBox->isVirtual();
        if (!$isVirtual) {
            $shippingAddress = $subscriptionBox->getShippingAddress();
            $profileData['ship_to_name'] = $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname();
            $profileData['ship_to_street'] = $shippingAddress->getStreetFull();
            $profileData['ship_to_city'] = $shippingAddress->getCity();
            $profileData['ship_to_state'] = $shippingAddress->getRegion();
            $profileData['ship_to_zip'] = $shippingAddress->getPostcode();
            $profileData['ship_to_country'] = $shippingAddress->getCountry();
        }

        $profileData['box_id'] = $subscriptionBox->getId();
        $profileData['customer_id'] = $customerId;
        $profileData['method_code'] = $gatewayResponse->getPaymentMethod();
        $profileData['quote_item_id'] = $subscriptionBox->getQuoteItemId();
        $itemIds = unserialize($subscriptionBox->getQuoteItemId());
        $itemInfoArr = [];
        $quoteId = $subscriptionBox->getQuoteId();
        $quote = $this->objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);

        if ($itemIds) {
            foreach ($itemIds as $itemId) {
                /** @var \Magento\Quote\Model\Quote\Item $quoteItem */
                $quoteItem = $this->objectManager->create('Magento\Quote\Model\Quote\Item')->load($itemId);
                $quoteItem->setQuote($quote);
                $product = $quoteItem->getProduct();
                array_push($itemInfoArr, ['product_id' => $product->getId(), 'product_name' => $product->getName(), 'product_sku' => $product->getSku()]);
                if ($this->mode == self::MODE_STANDALONE) {
                    $profileData['product_id'] = $product->getId();
                    $profileData['product_name'] = $product->getName();
                }
            }
            $profileData['item_info'] = serialize($itemInfoArr);
        }

        $profileModel->addData($profileData)->save();

        return $profileModel;
    }

    public function createNominalOrder($subscriptionBox, $payment)
    {
        return $this->orderManagement->createBaseOrder($subscriptionBox, $payment);
    }

    public function createBaseInvoice($order, $profile)
    {
        return $this->orderManagement->createBaseInvoice($order, $profile);
    }

    /**
     * create a nominal order for the subscription m
     *
     * This method could be invoked by admin's configuration
     * @param \Magenest\Subscription\Model\SubscriptionBox $subscriptionBox
     * @param \Magento\Framework\DataObject $payment
     *
     */
    public function createNominalOrderObsolete($subscriptionBox, $payment)
    {
        $objectManager = ObjectManager::getInstance();
        $boxId = $subscriptionBox->getId();
        $subscriptionBox = $objectManager->create('Magenest\Subscription\Model\SubscriptionBox')->load($boxId);
        $quoteId = $subscriptionBox->getQuoteId();

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);
        $quoteBillingData = $quote->getBillingAddress()->getData();
        $newOrder = $this->orderFactory->create();
        $billingAdd = $objectManager->create('Magento\Sales\Model\Order\Address');
        $billingAddrData = $quoteBillingData;
        $billingAdd->setData($billingAddrData)->setId(null);
        $shippingAdd = $objectManager->create('Magento\Sales\Model\Order\Address');
        if (!$quote->isVirtual()) {
            $shippingAddrData = $quote->getShippingAddress()->getData();
            $shippingAdd->setData($shippingAddrData);
        }

        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $orderPayment = $objectManager->create('Magento\Sales\Model\Order\Payment');
        $paymentMethodCode = $payment->getData('payment_method');

        $orderPayment->setMethod($paymentMethodCode);

        /** @var array $transferDataKeys */
        $transferDataKeys = [
            'store_id' => $quote->getStoreId(),
            'store_name' => $quote->getStore()->getName(),
            'customer_id' => $quote->getCustomerId(),
            'customer_email' => $quote->getCustomerEmail(),
            'customer_firstname' => $quote->getCustomerFirstname(),
            'customer_lastname' => $quote->getCustomerLastname(),
            'customer_middlename' => $quote->getCustomerPrefix(),
            'customer_prefix' => $quote->getCustomerPrefix(),
            'customer_suffix' => $quote->getCustomerSuffix(),
            'customer_taxvat' => $quote->getCustomerTaxvat(),
            'customer_gender' => $quote->getCustomerGender(),
            'customer_is_guest' => $quote->getCustomerIsGuest(),
            'customer_note_notify' => $quote->getCustomerNoteNotify(),
            'customer_group_id' => $quote->getCustomerGroupId(),
            'customer_note' => $quote->getCustomerNote(),
            'shipping_method' => $quote->getExtShippingInfo(),
            'shipping_description' => '',
            'base_currency_code' => $quote->getBaseCurrencyCode(),
            'global_currency_code' => $quote->getGlobalCurrencyCode(),
            'order_currency_code' => $quote->getQuoteCurrencyCode(),
            'store_currency_code' => $quote->getStoreCurrencyCode(),
            'base_to_global_rate' => $quote->getBaseToGlobalRate(),
            'base_to_order_rate' => $quote->getBaseToGlobalRate(),
            'store_to_base_rate' => $quote->getStoreToBaseRate(),
            'store_to_order_rate' => $quote->getStoreToQuoteRate()
        ];
        $newOrder->addData($transferDataKeys);

        $newOrder->setStoreId($quote->getStoreId())
            ->setState(Order::STATE_NEW)
            ->setStatus('pending')
            ->setBaseToOrderRate($quote->getBaseToQuoteRate())
            ->setStoreToOrderRate($quote->getStoreToQuoteRate())
            ->setOrderCurrencyCode($quote->getQuoteCurrencyCode())
            ->setBaseSubtotal($quote->getBaseSubtotal())
            ->setSubtotal($quote->getSubtotal())
            ->setBaseShippingAmount($subscriptionBox->getShippingFee())
            ->setShippingAmount($subscriptionBox->getShippingFee())
            ->setBaseTaxAmount($subscriptionBox->getTaxFee())
            ->setTaxAmount($subscriptionBox->getTaxFee())
            ->setBaseGrandTotal($subscriptionBox->getAmount())
            ->setGrandTotal($subscriptionBox->getAmount())
            ->setIsVirtual($subscriptionBox->isVirtual())
            ->setWeight($subscriptionBox->getWeight())
            ->setTotalQtyOrdered($subscriptionBox->getTotalQtyOrdered())
            ->setBillingAddress($billingAdd)
            ->setPayment($orderPayment);

        $isVirtual = $subscriptionBox->isVirtual();
        if (!$isVirtual) {
            $newOrder->setShippingAddress($shippingAdd);
        }
        /** @var \Magento\Sales\Model\Order\Item[] $items */
        if ($this->mode == self::MODE_STANDALONE) {
            $items = $quote->getAllVisibleItems();
        }
        foreach ($items as $item) {
            $newOrderItem = $this->quoteItemToOrderItem->convert($item);
            //$newOrderItem->setId(null);
            $newOrder->addItem($newOrderItem);
        }
        $newOrder->save();
        return $newOrder;
    }

    /**
     * @return mixed
     */
    public function getLastAddedProfileIds()
    {
        return $this->subscriptionSession->getLastAddedProfileIds();
    }

    /**
     * @return string
     */
    public function getLastOrderId()
    {
        return $this->lastOrderId;
    }

    /**
     * @param $orderId
     */
    public function setLastOrderId($orderId)
    {
        $this->lastOrderId = $orderId;
    }

    /**
     * Start processing payment info and turn into boxes and profiles
     * @param $quote
     * @param $payment
     */
    public function process($quote, $payment)
    {
        $this->subscribe($quote, $payment);
        $this->removeSubscribedItems();
        $orderId = $this->placeOneTimePurchasedOrder($quote, $payment);
        $this->setLastOrderId($orderId);
    }

    /**
     * Retrieve method model object
     *
     * @param string $code
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPaymentMethodInstance($code)
    {
        $class = $this->helper->getPaymentClass(
            $this->getMethodModelConfigName($code)
        );

        if (!$class) {
            throw new \UnexpectedValueException('Payment model name is not provided in config!');
        }

        return $this->methodFactory->create($class);
    }

    /**
     * @param string $code
     * @return string
     */
    protected function getMethodModelConfigName($code)
    {
        return sprintf('%s/%s/model', \Magenest\Subscription\Helper\Data::XML_GATEWAY_SUPPORT_SUBSCRIPTION_PATH, $code);
    }

    /**
     * For future use
     */
    public function removeSubscribedItems()
    {
    }

    /**
     * For future use
     * @param $quote
     * @param $payment
     */
    public function placeOneTimePurchasedOrder($quote, $payment)
    {

    }
}
