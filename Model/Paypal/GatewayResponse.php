<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 22/09/2017
 * Time: 15:35
 */

namespace Magenest\Subscription\Model\Paypal;

class GatewayResponse implements \Magenest\Subscription\Api\GatewayResponseInterface
{
    /**
     * @var string
     */
    protected $subscriptionId;

    /**
     * @var string
     */
    protected $customerProfileId;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $errorMessage;

    /**
     * @var string
     */
    protected $originalResponse;

    /**
     * GatewayResponse constructor.
     * @param $originalResponse
     */
    public function __construct($originalResponse)
    {
        $this->originalResponse = $originalResponse;
        $this->subscriptionId = $originalResponse['PROFILEID'];
        //$this->customerProfileId = $originalResponse->getProfile()->getCustomerProfileId();

        if ($originalResponse['PROFILESTATUS'] == 'ActiveProfile') {
            $this->status = "active";
        } elseif ($originalResponse['PROFILESTATUS'] == 'PendingProfile') {
            $this->status = "active";
        } else {
            $this->status = "error";
        }

        if (isset($originalResponse['ACK']) && $originalResponse['ACK'] =='Success') {
            $this->message  = 'Success';
        } else {
            $this->message = "Error";
        }
        //$this->message = $originalResponse->getMessages()->getMessage();
    }

    /**
     * get subscription id
     * @return string
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * get status of response
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * get error messsage if it has one
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->message;
    }

    /**
     * get payment method code
     * @return string
     */
    public function getPaymentMethod()
    {
        return 'paypal_express';
    }
}
