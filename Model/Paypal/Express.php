<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 18/09/2017
 * Time: 16:01
 */
namespace Magenest\Subscription\Model\Paypal;

use Magenest\Subscription\Model\Paypal\Api\Nvp\Request\CreateRecurringPaymentsProfile;
use Magenest\Subscription\Model\Paypal\Api\Nvp\Request\ManageRecurringPaymentsProfileStatus;
use Magenest\Subscription\Model\Paypal\Api\Nvp\Request\GetRecurringPaymentsProfileDetails;

class Express implements \Magenest\Subscription\Api\MethodInterface
{
    /**
     * @var CreateRecurringPaymentsProfile
     */
    protected $api;

    /**
     * @var ManageRecurringPaymentsProfileStatus
     */
    protected $manageApi;

    /**
     * @var GetRecurringPaymentsProfileDetails
     */
    protected $getRecurringPaymentsProfileDetailsApi;

    /**
     * @var string
     */
    protected $actionBlockType = \Magenest\Subscription\Block\Customer\Action\Paypal::class;

    /**
     * Express constructor.
     * @param CreateRecurringPaymentsProfile $api
     * @param ManageRecurringPaymentsProfileStatus $manageRecurringPaymentsProfileStatus
     * @param GetRecurringPaymentsProfileDetails $getRecurringPaymentsProfileDetails
     */
    public function __construct(
        CreateRecurringPaymentsProfile $api,
        ManageRecurringPaymentsProfileStatus $manageRecurringPaymentsProfileStatus,
        GetRecurringPaymentsProfileDetails $getRecurringPaymentsProfileDetails
    ) {
    
        $this->api = $api;
        $this->manageApi = $manageRecurringPaymentsProfileStatus;
        $this->getRecurringPaymentsProfileDetailsApi = $getRecurringPaymentsProfileDetails;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return "paypal_express";
    }

    /**
     * @return string
     */
    public function getActionBlockType()
    {
        return $this->actionBlockType;
    }

    /**
     * @return bool
     */
    public function isGateway()
    {
        return true;
    }

    /**
     * Retrieve payment method online/offline flag
     *
     * @return bool
     *
     */
    public function isOffline()
    {
        return false;
    }

    /**
     * communicate to the gateway
     * @param $subscriptionBox
     * @param $paymentInfo
     * @return mixed
     */
    public function submitPlan($subscriptionBox, $paymentInfo)
    {
        $this->api->setSubscriptionBox($subscriptionBox);
        $this->api->sendRequest();
        $response = $this->api->getFormattedResponse();
        return $response;
    }

    /**
     * invoked by customer to activate, cancel, suspend, reactive their recurring profile
     * @param $profile_id string
     * @param $action string
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function invokeUserAction($profile_id, $action)
    {
        $this->manageApi->setAction($action);
        $this->manageApi->setProfileId($profile_id);
        $this->manageApi->process();
        $response = $this->manageApi->getResponse();

        if (isset($response['ACK']) && $response['ACK'] === 'Success') {
            return true;
        } else {
            $this->_logger->critical($response['message']);
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Something went wrong while trying to change profile\'s status')
            );
        }
    }

    /**
     * invoke the payment API to check weather new payment is made for a recurring profile
     * @param $profile \Magenest\Subscription\Model\Profile
     * @return \Magenest\Subscription\Model\Profile
     */
    public function updateInformationByGatewayInfo($profile)
    {
        $profileId = $profile->getProfileId();

        //set the profile id
        $this->getRecurringPaymentsProfileDetailsApi->setProfileId($profileId);
        $this->getRecurringPaymentsProfileDetailsApi->process();

        $response = $this->getRecurringPaymentsProfileDetailsApi->getResponse();

        if (isset($response['ACK']) && $response['ACK'] === 'Success') {
            $profileData = [
                // Response general fields
                'profile_id' => $response['PROFILEID'],
                'state' => $response['STATUS'],
                'suspension_threshold' => $response['MAXFAILEDPAYMENTS'],
                'aggregate_amount' => $response['AGGREGATEAMT'],
                'aggregate_optional_amount' => $response['AGGREGATEOPTIONALAMT'],
                'final_payment_duedate' => $response['FINALPAYMENTDUEDATE'],

                // Profile Detail fields
                'subscriber_name' => $response['SUBSCRIBERNAME'],
                'start_datetime' => $response['PROFILESTARTDATE'],

                // Summary detail fields
//            'next_billing_date' => $response['NEXTBILLINGDATE'],
                'cycles_completed' => $response['NUMCYCLESCOMPLETED'],
                'cycles_remaining' => $response['NUMCYCLESREMAINING'],
                'outstanding_balance' => $response['OUTSTANDINGBALANCE'],
                'failed_payments' => $response['FAILEDPAYMENTCOUNT'],
                'trial_amt_paid' => $response['TRIALAMTPAID'],
                'regular_amt_paid' => $response['REGULARAMTPAID'],

                // Billing period fields
                'period_unit' => $response['REGULARBILLINGPERIOD'],
                'period_frequency' => $response['REGULARBILLINGFREQUENCY'],
                'period_max_cycles' => $response['REGULARTOTALBILLINGCYCLES'],
                'billing_amount' => $response['REGULARAMT'],
                'tax_amount' => $response['REGULARTAXAMT'],
                'shipping_amount' => $response['REGULARSHIPPINGAMT'],
            ];

            //update the trial information
            if (isset($response['TRIALBILLINGPERIOD'])) {
                $profileData['trial_billing_period'] = $response['TRIALBILLINGPERIOD'];
                $profileData['trial_billing_frequency'] = $response['TRIALBILLINGFREQUENCY'];
                $profileData['trial_billing_cycles'] = $response['TRIALTOTALBILLINGCYCLES'];

                $profileData['trial_amt'] = $response['TRIALAMT'];
                $profileData['trial_tax_amt'] = $response['TRIALTAXAMT'];
                $profileData['trial_shipping_amt'] = $response['TRIALSHIPPINGAMT'];
            }

            //update the init fee
            if (isset($response['INITAMT'])) {
                $profileData['initFee'] = $response['INITAMT'];
            }

            //update the shipping information
            if (isset($response['SHIPTONAME'])) {

                $shipping_info = [
                    'ship_to_name' => $response['SHIPTONAME'],
                    'ship_to_street' => $response['SHIPTOSTREET'],
                    'ship_to_city' => $response['SHIPTOCITY'],
                    'ship_to_zip' => $response['SHIPTOZIP'],
                    'ship_to_country' => $response['SHIPTOCOUNTRY']
                ];
            }

            if (isset($shipping_info)) {
                $data = array_merge($profileData, $shipping_info);
                $profile->addData($data)->save();
            } else {
                $profile->addData($profileData)->save();
            }
        }

        return $profile;
    }
}
