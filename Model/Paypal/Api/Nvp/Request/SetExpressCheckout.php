<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 20/09/2017
 * Time: 20:11
 */
namespace Magenest\Subscription\Model\Paypal\Api\Nvp\Request;

class SetExpressCheckout extends \Magenest\Subscription\Model\Paypal\Api\Nvp\AbstractNvp
{
    /**
     * Form the request to call method CallSetExpressCheckout on Paypal side
     */
    public function formRequest()
    {
        if (count($this->_boxes) > 0) {
            $this->calculateInitAmount();
            $amt = $this->getTotalAmountOfAllBoxes();

            // Payment Details Type Fields
            $this->_request['PAYMENTREQUEST_0_AMT'] = number_format($amt, 2);
            $this->_request['PAYMENTREQUEST_0_CURRENCYCODE'] = $this->currencyCode;
            $counter = 0;
            /** @var \Magenest\Subscription\Model\SubscriptionBox $box */
            foreach ($this->_boxes as $box) {
                $this->_request['L_PAYMENTREQUEST_0_NAME' . $counter] = $box->getName();
                $this->_request['L_PAYMENTREQUEST_0_AMT' . $counter] = number_format($box->getAmount(), 2);
                $this->_request['L_PAYMENTREQUEST_0_NUMBER' . $counter] = $box->getUniqueName();
                $this->_request['L_PAYMENTREQUEST_0_QTY' . $counter] = $box->getQty();
                $this->_request['L_BILLINGTYPE' . $counter] = 'RecurringPayments';
                $this->_request['PAYMENTREQUEST_0_CURRENCYCODE'] = $box->getCurrencyCode();
                $this->_request['L_BILLINGAGREEMENTDESCRIPTION' . $counter] = $box->getDescription();
                $counter++;
            }
        }
    }

    /**
     * process the business login
     */
    public function process()
    {
        $this->formRequest();
        $this->sendRequest();
        $token = $this->response['TOKEN'];

        $this->setToken($token);
    }
}
