<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 21/09/2017
 * Time: 09:06
 */

namespace Magenest\Subscription\Model\Paypal\Api\Nvp\Request;

class ManageRecurringPaymentsProfileStatus extends \Magenest\Subscription\Model\Paypal\Api\Nvp\AbstractNvp
{
    /**
     * @var String
     */
    protected $action;

    /**
     * @var String
     */
    protected $profileId;

    /**
     * set profile id
     * @param $profileId
     */
    public function setProfileId($profileId)
    {
        $this->profileId = $profileId;
    }

    /**
     * get the profile Id
     * @return string|null
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * set action for class
     * @param $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return String
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * form the request which is name-pair value in order to send it to paypal gateway
     * @return mixed
     */
    public function formRequest()
    {
        $this->_request['METHOD'] = 'ManageRecurringPaymentsProfileStatus';
        $this->_request['PROFILEID'] = $this->getProfileId();
        $this->_request['ACTION'] = $this->getAction();
    }

    /**
     * process the business login
     */
    public function process()
    {
        $this->formRequest();
        $this->sendRequest();
    }
}
