<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 21/09/2017
 * Time: 09:06
 */

namespace Magenest\Subscription\Model\Paypal\Api\Nvp\Request;

class GetRecurringPaymentsProfileDetails extends \Magenest\Subscription\Model\Paypal\Api\Nvp\AbstractNvp
{
    /**
     * @var String
     */
    protected $profile_id;

    /**
     * @param $profileId
     */
    public function setProfileId($profileId)
    {
        $this->profile_id = $profileId;
    }

    /**
     * @return int
     */
    public function getProfileId()
    {
        return $this->profile_id ;
    }

    /**
     * form the request which is name-pair value in order to send it to paypal gateway
     * @return mixed
     */
    public function formRequest()
    {
        $this->_request['METHOD'] = 'GetRecurringPaymentsProfileDetails';
        $this->_request['PROFILEID'] = strval($this->getProfileId());

    }

    /**
     * process the business login
     */
    public function process()
    {
        $this->formRequest();
        $this->sendRequest();
    }
}
