<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 21/09/2017
 * Time: 09:05
 */

namespace Magenest\Subscription\Model\Paypal\Api\Nvp\Request;

use Magenest\Subscription\Model\ProfileFactory;
use Magenest\Subscription\Model\SubscriptionBoxFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use Magento\Payment\Model\Method\Logger;
use Magento\Quote\Model\QuoteFactory;

class CreateRecurringPaymentsProfile extends \Magenest\Subscription\Model\Paypal\Api\Nvp\AbstractNvp
{
    protected $recurringRequest = [];

    /**
     * @var \Magenest\Subscription\Model\SubscriptionBox
     */
    protected $box;

    /**
     * @param $box
     */
    public function setSubscriptionBox($box)
    {
        $this->box = $box;
    }

    /**
     * @return \Magenest\Subscription\Model\SubscriptionBox
     */
    public function getSubscriptionBox()
    {
        return $this->box ;
    }

    /**
     * form the request to send to paypal gateway
     */
    public function formRequest()
    {
        $this->_request['METHOD'] = 'CreateRecurringPaymentsProfile';
        $this->_request['TOKEN'] = $this->getToken();
        $this->_request['PAYERID'] = $this->getPayerId();

        $isAutoBill = $this->box->getAutoBill();
        if ($isAutoBill) {
            $isAutoBill = 'AddToNextBilling';
        } else {
            $isAutoBill = 'NoAutoBill';
        }
        $recurringRequest['PROFILESTARTDATE'] = $this->box->getFormattedDateStart();
        //adjust if the billing period is month
        $billingPeriod = strtr($this->box->getPeriodUnit(), ['day' => 'Day' , 'week' => 'Week' , 'semimonth' => 'SemiMonth'  ,'month' => 'Month' , 'year' => 'Year']);
        $billingFrequency =  intval($this->box->getFrequency());

        if ($billingPeriod == 'month') {
            $billingFrequency =   $billingFrequency*4;
            $billingPeriod= 'Week';
        }

        $recurringRequest['PROFILESTARTDATE'] = $this->box->getFormattedDateStart();

        $recurringRequest['DESC'] = $this->box->getDescription();
        $recurringRequest['MAXFAILEDPAYMENTS'] = $this->box->getMaximumPaymentFailures();
        $recurringRequest['BILLINGPERIOD'] = $billingPeriod;
        $recurringRequest['BILLINGFREQUENCY'] =$billingFrequency;
        $recurringRequest['TOTALBILLINGCYCLES'] = $this->box->getCycles();
        $recurringRequest['AMT'] = number_format(($this->box->getAmount()-$this->box->getData('init_fee')), 2) ;
	$recurringRequest['INITAMT'] = number_format($this->box->getAmount(),2);
        $recurringRequest['CURRENCYCODE'] = $this->box->getCurrencyCode();
        $recurringRequest['AUTOBILLOUTAMT'] = $isAutoBill;
        $recurringRequest['ADDROVERRIDE'] = '0';

        /** @var boolean $hasTrial */
        $hasTrial = $this->box->hasTrialInformation();
        if ($hasTrial) {
            $trialPeriod = strtr($this->box->getTrialUnit(), ['day' => 'Day' , 'week' => 'Week' , 'semimonth' => 'SemiMonth'  ,'month' => 'Month' , 'year' => 'Year']);
            $recurringRequest['TRIALBILLINGPERIOD'] = $trialPeriod;
            $recurringRequest['TRIALBILLINGFREQUENCY'] = $this->box->getTrialFrequency();
            $recurringRequest['TRIALTOTALBILLINGCYCLES'] = $this->box->getTrialCycles();
            $recurringRequest['TRIALAMT'] = number_format(floatval($this->box->getTrialAmount()), 2);

            $initAmount = floatval($this->box->getInitFee());

            if ($initAmount > 0) {
                $recurringRequest['INITAMT'] = number_format($initAmount, 2);
                $recurringRequest['FAILEDINITAMTACTION'] = $this->subscriptionHelper->getConfigValue(\Magenest\Subscription\Helper\Data::XML_PAYPAL_INIT_FAIL_ACTION);
            }
        }

        /** form shipping params */
        $isVirtual = $this->box->isVirtual();

        if (!$isVirtual) {
            /** @var  $shippingAddr */
            $shippingAddr = $this->box->getShippingAddress();
            $recurringRequest['SHIPTONAME'] = $shippingAddr->getFirstname() . " " .$shippingAddr->getLastname();
            $recurringRequest['SHIPTOSTREET'] = $shippingAddr->getStreetFull();
            $recurringRequest['SHIPTOCITY'] = $shippingAddr->getCity();
            $recurringRequest['SHIPTOZIP'] = $shippingAddr->getPostcode();
            $recurringRequest['SHIPTOCOUNTRY'] = $shippingAddr->getCountryId();
        }

        $request = array_merge($this->_request, $recurringRequest);
        $this->_request = $request;
    }

    /**
     * form the request which is name-pair value in order to send it to paypal gateway
     * @return mixed
     */
    public function formRequestInBatch()
    {
        $this->_request['METHOD'] = 'CreateRecurringPaymentsProfile';
        $this->_request['TOKEN'] = $this->getToken();
        $this->_request['PAYERID'] = $this->getPayerId();

       //loop through all the subscript boxes
        if ($this->_boxes) {

            /** @var \Magenest\Subscription\Model\SubscriptionBox $box */
            foreach ($this->_boxes as $box) {
                $isAutoBill = $box->getAutoBill();
                if ($isAutoBill) {
                    $isAutoBill = 'AddToNextBilling';
                } else {
                    $isAutoBill = 'NoAutoBill';
                }
                $recurringRequest['PROFILESTARTDATE'] = $box->getFormattedDateStart();

                $recurringRequest['DESC'] = $box->getDescription();
                $recurringRequest['MAXFAILEDPAYMENTS'] = $box->getMaximumPaymentFailures();
                $recurringRequest['BILLINGPERIOD'] = $box->getPeriodUnit();
                $recurringRequest['BILLINGFREQUENCY'] = $box->getFrequency();
                $recurringRequest['TOTALBILLINGCYCLES'] = $box->getCycles();
                $recurringRequest['AMT'] = $box->getAmount();
                $recurringRequest['CURRENCYCODE'] = $box->getBaseCurrencyCode();
                $recurringRequest['AUTOBILLOUTAMT'] = $isAutoBill;
                $recurringRequest['ADDROVERRIDE'] = '0';

               /** @var boolean $hasTrial */
                $hasTrial = $box->hasTrialInformation();
                if ($hasTrial) {
                    $recurringRequest['TRIALBILLINGPERIOD'] = $box->getTrialUnit();
                    $recurringRequest['TRIALBILLINGFREQUENCY'] = $box->getTrialFrequency();
                    $recurringRequest['TRIALTOTALBILLINGCYCLES'] = $box->getTrialCycles();
                    $recurringRequest['TRIALAMT'] = number_format(number_format($box->getTrialAmount(), 2));

                    $initAmount = floatval($box->getInitFee());

                    if ($initAmount > 0) {
                        $recurringRequest['INITAMT'] = number_format($initAmount, 2);
                        $recurringRequest['FAILEDINITAMTACTION'] = $this->subscriptionHelper->getConfigValue(\Magenest\Subscription\Helper\Data::XML_PAYPAL_INIT_FAIL_ACTION);
                    }
                }

                $finalRecurringRequest = array_merge($this->_request, $recurringRequest);
                array_push($this->recurringRequest, $finalRecurringRequest);
            }
        }
    }
}
