<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 21/09/2017
 * Time: 09:07
 */

namespace Magenest\Subscription\Model\Paypal\Api\Nvp\Request;

class GetPalDetails extends \Magenest\Subscription\Model\Paypal\Api\Nvp\AbstractNvp
{
    /**
     * @var string
     */
    protected $profileId;

    /**
     * @var
     */
    protected $response;

    /**
     * set profile id
     * @param $profileId
     */
    public function setProfileId($profileId)
    {
        $this->profileId = $profileId;
    }

    /**
     * get the profile Id
     * @return string|null
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * form the request which is name-pair value in order to send it to paypal gateway
     * @return mixed
     */
    public function formRequest()
    {
        $this->_request['METHOD'] = 'GetPalDetails';

    }


    /**
     * process the business login
     */
    public function process()
    {
        $this->formRequest();
        $this->sendRequest();
    }
}
