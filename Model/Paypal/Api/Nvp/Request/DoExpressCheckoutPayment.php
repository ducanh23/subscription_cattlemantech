<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 21/09/2017
 * Time: 09:05
 */

namespace Magenest\Subscription\Model\Paypal\Api\Nvp\Request;

class DoExpressCheckoutPayment extends \Magenest\Subscription\Model\Paypal\Api\Nvp\AbstractNvp
{
    /**
     * form the request which is name-pair value in order to send it to paypal gateway
     * @return mixed
     */
    public function formRequest()
    {
        $this->_request['METHOD'] = 'DoExpressCheckoutPayment';
        $this->_request['TOKEN'] = $this->getToken();
        $this->_request['PAYERID'] = $this->getPayerId();
        $this->_request['PAYMENTREQUEST_0_PAYMENTACTION'] = 'Sale';
        $this->_request['SKIPBACREATION'] = 'false';
    }
}
