<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 21/09/2017
 * Time: 09:06
 */

namespace Magenest\Subscription\Model\Paypal\Api\Nvp\Request;

class GetExpressCheckoutDetails extends \Magenest\Subscription\Model\Paypal\Api\Nvp\AbstractNvp
{
    /**
     * Form request to paypal
     */
    public function formRequest()
    {
        $this->_request['TOKEN'] = $this->getToken();
        $this->_request['METHOD'] = 'GetExpressCheckoutDetails';
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function process()
    {
        $this->formRequest();
        $this->sendRequest();

        if ($this->getQuote()) {
            if ($this->response) {
                //store information into the payment of quote
                $quotePayment = $this->_quote->getPayment();
                $quotePayment->setMethod('paypal_express');

                $additionalData = [];
                if (isset($this->response['TOKEN'])) {
                    $additionalData['paypal_express_checkout_token'] = $this->response['TOKEN'];
                }
                if (isset($this->response['PAYERID'])) {
                    $additionalData['paypal_express_checkout_payer_id'] = $this->response['PAYERID'];
                }
                if (isset($this->response['PAYERID'])) {
                    $additionalData['paypal_payer_id'] = $this->response['PAYERID'];
                }
                if (isset($this->response['EMAIL'])) {
                    $additionalData['paypal_payer_email'] = $this->response['EMAIL'];
                }
                if (isset($this->response['PAYERSTATUS'])) {
                    $additionalData['paypal_payer_status'] = $this->response['PAYERSTATUS'];
                }
                if (isset($this->response['ADDRESSSTATUS'])) {
                    $additionalData['paypal_address_status'] = $this->response['ADDRESSSTATUS'];
                }
                if (isset($this->response['CORRELATIONID'])) {
                    $additionalData['paypal_correlation_id'] = $this->response['CORRELATIONID'];
                }
                if (isset($this->response['CURRENCYCODE'])) {
                    $additionalData['currency_code'] = $this->response['CURRENCYCODE'];
                }

                $quotePayment->setAdditionalData($this->serialize($additionalData))->save();

                //set payer id for the model
                if (isset($this->response['PAYERID'])) {
                    $this->setPayerId($this->response['PAYERID']);

                } ;
                return $this->response;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while returning from Paypal.')
                );
            }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Quote  does not exist.')
            );
        }
    }
}
