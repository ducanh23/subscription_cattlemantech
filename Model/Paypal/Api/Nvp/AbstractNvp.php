<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 20/09/2017
 * Time: 15:56
 */
namespace Magenest\Subscription\Model\Paypal\Api\Nvp;

use Magento\Payment\Model\Method\Logger;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\QuoteFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\ObjectManagerInterface;
use Magenest\Subscription\Model\ProfileFactory;
use Magenest\Subscription\Model\SubscriptionBoxFactory;
use Magento\Framework\Message\ManagerInterface;

abstract class AbstractNvp extends \Magento\Paypal\Model\Api\AbstractApi
{
    protected $_request;
    protected $response;
    protected $token;
    protected $payer_id;
    protected $quote_id;
    protected $boxes_id;
    protected $_headers = [];
    protected $_boxes = [];
    protected $initAmount = 0;
    protected $currencyCode = "USD";
    protected $timeout = 60;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    /**
     * @var \Magento\Framework\HTTP\Adapter\CurlFactory
     */
    protected $_curlFactory;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $_quote;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Tax\Helper\Data
     */
    protected $_taxData;

    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Quote\Model\QuoteManagement
     */
    protected $_quoteManagement;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Checkout\Helper\Data
     */
    protected $_helperData;

    /**
     * @var ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var \Magenest\Subscription\Model\RelationshipFactory
     */
    protected $_relationshipFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magenest\Subscription\Helper\Util
     */
    protected $_util;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var SubscriptionBoxFactory
     */
    protected $boxFactory;

    /**
     * @var \Magento\Framework\Session\Generic
     */
    protected $paypalSession;


    protected $subscriptionHelper;

    /**
     * AbstractNvp constructor.
     * @param \Magenest\Subscription\Model\RelationshipFactory $relationshipFactory
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Customer\Helper\Address $customerAddress
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param ObjectManagerInterface $objectManagerInterface
     * @param \Magenest\Subscription\Logger\Logger $logger
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param \Magento\Checkout\Helper\Data $helperData
     * @param \Magenest\Subscription\Helper\Util $util
     * @param \Magento\Paypal\Model\Config $config
     * @param ManagerInterface $managerInterface
     * @param \Magento\Tax\Helper\Data $taxData
     * @param ProfileFactory $profileFactory
     * @param Logger $customLogger
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Framework\Session\Generic $paypalSession
     * @param QuoteFactory $quoteFactory
     * @param SubscriptionBoxFactory $subscriptionBoxFactory
     * @param array $data
     */
    public function __construct(
        \Magenest\Subscription\Model\RelationshipFactory $relationshipFactory,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\Framework\HTTP\Adapter\CurlFactory $curlFactory,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Customer\Helper\Address $customerAddress,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        ObjectManagerInterface $objectManagerInterface,
        \Magenest\Subscription\Logger\Logger $logger,
        ScopeConfigInterface $scopeConfigInterface,
        \Magento\Checkout\Helper\Data $helperData,
        \Magenest\Subscription\Helper\Util $util,
        \Magento\Paypal\Model\Config $config,
        ManagerInterface $managerInterface,
        \Magento\Tax\Helper\Data $taxData,
        ProfileFactory $profileFactory,
        Logger $customLogger,
        \Magento\Framework\UrlInterface $url,
        \Magento\Framework\Session\Generic $paypalSession,
        QuoteFactory $quoteFactory,
        SubscriptionBoxFactory $subscriptionBoxFactory,
        \Magenest\Subscription\Helper\Data $subscriptionHelper,
        $data = []
    ) {
    
        $this->_util = $util;
        $this->_config = $config;
        $this->_taxData = $taxData;
        $this->_helperData = $helperData;
        $this->_curlFactory = $curlFactory;
        $this->_profileFactory = $profileFactory;
        $this->messageManager = $managerInterface;
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_quoteManagement = $quoteManagement;
        $this->_scopeConfig = $scopeConfigInterface;
        $this->_objectManager = $objectManagerInterface;
        $this->_relationshipFactory = $relationshipFactory;
        $this->_url = $url;
        $this->paypalSession = $paypalSession;
        $this->quoteFactory = $quoteFactory;
        $this->boxFactory = $subscriptionBoxFactory;
        $this->subscriptionHelper = $subscriptionHelper;

        $returnUrl = $this->_url->getUrl('subscription/express/return');
        $cancelUrl = $this->_url->getUrl('subscription/express/cancel');
        $this->_request = [
            'PAYMENTACTION' => 'Authorization',
            'RETURNURL' => $returnUrl,
            'CANCELURL' => $cancelUrl,
            'SOLUTIONTYPE' => 'Mark',
            'VERSION' => '72.0',
            'METHOD' => 'SetExpressCheckout',
            'USER' => $this->_scopeConfig->getValue('paypal/wpp/api_username', ScopeInterface::SCOPE_STORE),
            'PWD' => $this->_scopeConfig->getValue('paypal/wpp/api_password', ScopeInterface::SCOPE_STORE),
            'SIGNATURE' => $this->_scopeConfig->getValue('paypal/wpp/api_signature', ScopeInterface::SCOPE_STORE)
        ];

        parent::__construct($customerAddress, $logger, $customLogger, $localeResolver, $regionFactory, $data);
    }

    /**
     * form the request which is name-pair value in order to send it to paypal gateway
     * @return mixed
     */
    abstract public function formRequest();

    /**
     * @return array
     */
    public function getSubscriptionBoxes()
    {
        if ($this->_boxes) {
            return $this->_boxes;
        }

        $boxedIds = $this->getBoxesId();
        if ($boxedIds) {
            foreach ($boxedIds as $boxid) {
                array_push($this->_boxes, $this->boxFactory->create()->load($boxid));
            }
        }
        return $this->_boxes;
    }

    /**
     * @param $boxes
     */
    public function setSubscriptionBoxes($boxes)
    {
        $this->_boxes = $boxes;

        if ($boxes) {
            $boxesId = [];
            foreach ($boxes as $box) {
                $boxesId[] = $box->getId();
            }
        }
        $this->setBoxesId($boxesId);
    }

    /**
     * @return mixed
     */
    public function getQuote()
    {
        if ($this->_quote) {
            return $this->_quote;
        }

        $quoteId = $this->getQuoteId();
        if ($quoteId) {
            $this->_quote = $this->quoteFactory->create()->load($quoteId);
            return $this->_quote;
        }
    }

    /**
     * @param $quote
     */
    public function setQuote($quote)
    {
        $this->_quote = $quote;

        $this->setQuoteId($quote->getId());
    }

    /**
     * @return float
     */
    public function getInitAmount()
    {
        return $this->initAmount;
    }

    /**
     * get amount of all boxes
     * @return int|string
     */
    public function getTotalAmountOfAllBoxes()
    {
        $amt = 0;
        if ($this->_boxes) {
            /** @var \Magenest\Subscription\Model\SubscriptionBox $box */
            foreach ($this->_boxes as $box) {
                $amt += number_format($box->getAmount(), 2);
            }
        }
        return $amt;
    }

    /**
     * Calculate init amount
     */
    public function calculateInitAmount()
    {
        if ($this->_boxes) {
            //reset initial amount
            $this->initAmount = 0;

            /** @var \Magenest\Subscription\Model\SubscriptionBox $box */
            foreach ($this->_boxes as $box) {
                $this->initAmount += $box->getInitFee();
            }
        }
    }

    /**
     * @return string
     */
    public function getApiEndpoint()
    {
        $url = $this->getUseCertAuthentication() ? 'https://api%s.paypal.com/nvp' : 'https://api-3t%s.paypal.com/nvp';
        return sprintf(
            $url,
            $this->_scopeConfig->getValue('paypal/wpp/sandbox_flag', ScopeInterface::SCOPE_STORE) ? '.sandbox' : ''
        );
    }

    /**
     * format the response
     * @param $nvpstr
     * @return array
     */
    protected function _deformatNVP($nvpstr)
    {
        $intial = 0;
        $nvpArray = [];

        $nvpstr = strpos($nvpstr, "\r\n\r\n") !== false ? substr($nvpstr, strpos($nvpstr, "\r\n\r\n") + 4) : $nvpstr;

        while (strlen($nvpstr)) {
            //postion of Key
            $keypos = strpos($nvpstr, '=');
            //position of value
            $valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen($nvpstr);

            /*getting the Key and Value values and storing in a Associative Array*/
            $keyval = substr($nvpstr, $intial, $keypos);
            $valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);
            //decoding the respose
            $nvpArray[urldecode($keyval)] = urldecode($valval);
            $nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
        }
        return $nvpArray;
    }

    /**
     * Send request
     */
    public function sendRequest()
    {
        $this->formRequest();
        $response = $this->_sendRequest();
        $this->response = $response;
    }

    /**
     * Get response
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Get formatted response
     * @return mixed
     */
    public function getFormattedResponse()
    {
        if ($this->response) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $response = $objectManager->create('Magenest\Subscription\Model\Paypal\GatewayResponse', ['originalResponse' => $this->response]);
            return $response;
        }
    }

    /**
     * Send request to paypal
     * @param $request
     * @param null $timeout
     * @return array|false|string|string[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _sendRequest()
    {
        try {
            /** @var \Magento\Framework\HTTP\Adapter\Curl $http */
            $http = $this->_curlFactory->create();
            $config = [
                'timeout' => $this->timeout,
                'verifypeer' => $this->_scopeConfig->getValue(
                    'payment/paypal_express/verify_peer',
                    ScopeInterface::SCOPE_STORE
                )
            ];

            if ($this->_scopeConfig->getValue('paypal/wpp/use_proxy', ScopeInterface::SCOPE_STORE)) {
                $config['proxy'] = $this->_scopeConfig->getValue('paypal/wpp/proxy_host', ScopeInterface::SCOPE_STORE)
                    . ':' . $this->_scopeConfig->getValue('paypal/wpp/proxy_port', ScopeInterface::SCOPE_STORE);
            }

            if ($this->_scopeConfig->getValue('paypal/wpp/api_authentication', ScopeInterface::SCOPE_STORE)) {
                $config['ssl_cert'] = $this->getApiCertificate();
            }

            $http->setConfig($config);
            $http->write(
                \Zend_Http_Client::POST,
                $this->getApiEndpoint(),
                '1.1',
                $this->_headers,
                $this->_buildQuery($this->_request)
            );

            $response = $http->read();

            // handle transport error
            if ($http->getErrno()) {
                $this->_logger->critical(
                    new \Exception(
                        sprintf('PayPal NVP CURL connection error #%s: %s', $http->getErrno(), $http->getError())
                    )
                );
                $http->close();

                throw new \Magento\Framework\Exception\LocalizedException(__('We can\'t contact the PayPal gateway.'));
            }

            // cUrl resource must be closed after checking it for errors
            $http->close();
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Can not send request to ' . $this->getApiEndpoint() . '.')
            );
        }

        $response = preg_split('/^\r?$/m', $response, 2);
        $response = trim($response[1]);
        $response = $this->_deformatNVP($response);

        $this->_logger->debug("Paypal request:\n" . print_r($this->_request, true) . "\n");
        $this->_logger->debug("Paypal response:\n" . print_r($response, true) . "\n");

        if ($response['ACK'] == 'Failure') {
            if (isset($response['L_ERRORCODE0']) && $response['L_ERRORCODE0'] == '10002') {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Please contact store owner to re-check API keys')
                );
            }
            throw new \Magento\Framework\Exception\LocalizedException(
                __($response['L_LONGMESSAGE0'])
            );
        } else {
            return $response;
        }
    }

    /**
     * get the token
     * @return string
     */
    public function getToken()
    {
        return $this->paypalSession->getToken();
    }

    /**
     * set token
     * @param $token
     */
    public function setToken($token)
    {
        $this->paypalSession->setToken($token);
        $this->token = $token;
    }

    /**
     * @param $quoteId
     */
    public function setQuoteId($quoteId)
    {
        $this->paypalSession->setQuoteId($quoteId);
        $this->quote_id = $quoteId;
    }

    /**
     * @return mixed
     */
    public function getQuoteId()
    {
        return $this->paypalSession->getQuoteId();
    }

    /**
     * @param $boxesId
     */
    public function setBoxesId($boxesId)
    {
        $this->paypalSession->setBoxesId($boxesId);
    }

    /**
     *
     */
    public function getBoxesId()
    {
        return $this->paypalSession->getBoxesId();
    }

    /**
     * @param $payerId
     */
    public function setPayerId($payerId)
    {
        $this->paypalSession->setPayerId($payerId);
    }

    /**
     * @return mixed
     */
    public function getPayerId()
    {
        return $this->paypalSession->getPayerId();
    }

    /**
     * get the redirect url
     * @return string
     */
    public function getRedirectUrl()
    {
        if (!$this->_taxData->getConfig()->priceIncludesTax()) {
            return $this->getExpressCheckoutStartUrl();
        } else {
            return $this->getPayPalBasicStartUrl();
        }
    }

    /**
     * get express checkout start url
     * @return string
     */
    public function getExpressCheckoutStartUrl()
    {
        return $this->getPaypalUrl(['cmd' => '_express-checkout', 'token' => $this->getToken()]);
    }

    /**
     * @param array $params
     * @return string
     */
    public function getPaypalUrl(array $params = [])
    {
        return sprintf(
            'https://www.%spaypal.com/cgi-bin/webscr%s',
            $this->_scopeConfig->getValue('paypal/wpp/sandbox_flag', ScopeInterface::SCOPE_STORE) ? 'sandbox.' : '',
            $params ? '?' . http_build_query($params) : ''
        );
    }

    /**
     * get paypal basic start url
     * @param $token
     * @return string
     */
    public function getPayPalBasicStartUrl()
    {
        $params = [
            'cmd' => '_express-checkout',
            'token' => $this->getToken(),
        ];

        if ($this->isOrderReviewStepDisabled()) {
            $params['useraction'] = 'commit';
        }

        return $this->getPaypalUrl($params);
    }

    /**
     * whether the order review step is disabled
     * @return mixed
     */
    public function isOrderReviewStepDisabled()
    {
        return $this->_scopeConfig->getValue(
            'payment/paypal_express/skip_order_review_step',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
