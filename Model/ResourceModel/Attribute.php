<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/03/2016
 * Time: 16:24
 */
namespace Magenest\Subscription\Model\ResourceModel;

class Attribute extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize banner catalog rule resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_subscription_product_attribute', 'id');
    }
}
