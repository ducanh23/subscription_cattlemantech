<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Model\ResourceModel\Relationship;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define collection item type and corresponding table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Subscription\Model\Relationship', 'Magenest\Subscription\Model\ResourceModel\Relationship');
        $this->_idFieldName = 'id';
    }
}
