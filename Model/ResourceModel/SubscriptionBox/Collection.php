<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 18/09/2017
 * Time: 13:42
 */
namespace Magenest\Subscription\Model\ResourceModel\SubscriptionBox;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Define collection item type and corresponding table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magenest\Subscription\Model\SubscriptionBox', 'Magenest\Subscription\Model\ResourceModel\SubscriptionBox');
        $this->_idFieldName = 'id';
    }
}
