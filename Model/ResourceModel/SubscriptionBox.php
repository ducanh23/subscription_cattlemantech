<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 18/09/2017
 * Time: 13:41
 */

namespace Magenest\Subscription\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class SubscriptionBox extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize banner catalog rule resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_subscription_box', 'id');

    }

    /**
     * load Box by item ids
     * @param array $itemIds
     * @return  int $boxId
     */
    public function loadByItemIds($itemIds)
    {
            $select = $this->getConnection()->select()->from(
                $this->getMainTable(),
                ['id']
            )->where(
                "quote_item_id = :qid"
            );

            $binds = ['qid' => serialize($itemIds)];

            $boxId = $this->getConnection()->fetchOne($select, $binds);

            return $boxId;
    }
}
