<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 23/03/2016
 * Time: 19:33
 */
namespace Magenest\Subscription\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Profile extends AbstractDb
{
    /**
     * Initialize banner catalog rule resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magenest_subscription_recurring_profile', 'id');
    }
}
