<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 23/03/2016
 * Time: 19:35
 */

namespace Magenest\Subscription\Model\ResourceModel\Profile;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->_toOptionArray('state', 'state');
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionHash()
    {
        return $this->_toOptionHash('state', 'state');
    }

    /**
     * @return array
     */
    public function getMethodListArray()
    {
        return $this->toArray(['method_code'])['items'];
    }

    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init('Magenest\Subscription\Model\Profile', 'Magenest\Subscription\Model\ResourceModel\Profile');
    }
}
