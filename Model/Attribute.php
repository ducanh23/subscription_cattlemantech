<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/03/2016
 * Time: 16:19
 */
namespace Magenest\Subscription\Model;

use Magenest\Subscription\Model\ResourceModel\Attribute as Resource;
use Magenest\Subscription\Model\ResourceModel\Attribute\Collection as Collection;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

class Attribute extends AbstractModel
{
    protected $_eventPrefix = 'magenest_subscription_attr';

    /**
     * Attribute constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Resource $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Resource $resource,
        Collection $resourceCollection,
        $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
}
