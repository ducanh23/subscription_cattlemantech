<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 16/09/2017
 * Time: 14:44
 */
namespace Magenest\Subscription\Model\AuthorizeDirectPost;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Authorizenet helper calls
 * @package Magenest\Subscription\Model\AuthorizeDirectPost
 */
class Authorize implements \Magenest\Subscription\Api\MethodInterface
{
    /**
     * @var String
     */
    protected $_merchantId;

    /**
     * @var mixed
     */
    protected $_merchantTransactionKey;

    /**
     * @var \Magenest\Subscription\Logger\Logger
     */
    protected $_logger;

    /**
     * @var mixed
     */
    protected $_useRef;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magenest\Subscription\Model\ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var String
     */
    protected $_testMode;

    /**
     * @var String
     */
    protected $actionBlockType = \Magenest\Subscription\Block\Customer\Action\Authorize::class;

    /**
     * Authorize constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param \Magenest\Subscription\Logger\Logger $logger
     * @param \Magenest\Subscription\Model\ProfileFactory $profileFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Magenest\Subscription\Logger\Logger $logger,
        \Magenest\Subscription\Model\ProfileFactory $profileFactory
    ) {
    
        $this->_scopeConfig = $scopeConfig;
        $this->_profileFactory = $profileFactory;
        $this->_logger = $logger;
        $this->_merchantId = $this->_scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_MERCHANT_ID);
        $this->_merchantTransactionKey = $this->_scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_TRANSACTION_KEY);
        $this->_testMode = $this->_scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_IS_TEST);
        $this->_paymentAction = $this->_scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_PAYMENT_ACTION);
        $this->_useRef = $this->_scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_USE_REF);
    }

    /**
     * @return String
     */
    public function getActionBlockType()
    {
        return $this->actionBlockType;
    }

    /**
     * @param \Magenest\Subscription\Model\SubscriptionBox $subscriptionBox
     * @param $paymentInfo
     * @return array
     */
    public function submitPlan($subscriptionBox, $paymentInfo)
    {
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($this->_merchantId);
        $merchantAuthentication->setTransactionKey($this->_merchantTransactionKey);

        //get subscription information
        $subscriptionUnit = $subscriptionBox->getTimeUnit();
        $subscriptionFrequency = intval($subscriptionBox->getFrequency());
        $startDate = $subscriptionBox->getStartDate();
        $cycles = $subscriptionBox->getCycles();
        $trialAmount = $subscriptionBox->getTrialAmount();

        //Subscription Type Info
        $subscription = new AnetAPI\ARBSubscriptionType();
        $subscription->setName($subscriptionBox->getName());

        // set main subscription interval
        $interval = new AnetAPI\PaymentScheduleType\IntervalAType();

        switch ($subscriptionUnit) {
            case 'week':
                $interval->setLength($subscriptionFrequency * 7);
                $interval->setUnit('days');
                break;
            case 'semimonth':
                $interval->setLength($subscriptionFrequency * 15);
                $interval->setUnit('days');
                break;
            case 'year':
                $interval->setLength($subscriptionFrequency * 12);
                $interval->setUnit('months');
                break;
            default:
                $interval->setLength($subscriptionFrequency);
                $interval->setUnit(strtolower($subscriptionUnit) . 's');
                break;
        }

        //Set up payment Schedule
        $paymentSchedule = new AnetAPI\PaymentScheduleType();
        $paymentSchedule->setInterval($interval);
        $paymentSchedule->setStartDate(new \DateTime($startDate));

        if (!$cycles) {
            $paymentSchedule->setTotalOccurrences(9999);
        } else {
            if ($trialAmount) {
                $totalOccurrences = intval($this->getTotalOccurrences($subscriptionBox));
                $trialCycles = $subscriptionBox->getTrialCycles();
                $paymentSchedule->setTotalOccurrences($totalOccurrences);
                $paymentSchedule->setTrialOccurrences($trialCycles);
                $subscription->setTrialAmount(intval($subscriptionBox->getTrialAmount()));
            } else {
                $paymentSchedule->setTotalOccurrences($cycles);
            }
        }

        $subscription->setPaymentSchedule($paymentSchedule);
        $amount = $subscriptionBox->getAmount();
        $amount = number_format((float)$amount, 2, '.', '');
        $subscription->setAmount($amount);

        /** set the payment information */
        $cc_number = $paymentInfo->getData('ccNumber');
        $cc_exp_month = $paymentInfo->getData('expMonth');
        $cc_exp_year = $paymentInfo->getData('expYear');
        $cc_id = $paymentInfo->getData('ccId');

        $formatted_cc_exp_month = "";
        if (intval($cc_exp_month) < 10) {
            $formatted_cc_exp_month = '0' . $cc_exp_month;
        } else {
            $formatted_cc_exp_month = $cc_exp_month;
        }

        //Set up Credit Card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($cc_number);
        $creditCard->setExpirationDate($cc_exp_year . '-' . $formatted_cc_exp_month);
        if ($cc_id) {
            $creditCard->setCardCode($cc_id);
        }

        //Payment Type
        $payment = new AnetAPI\PaymentType();
        $payment->setCreditCard($creditCard);
        $subscription->setPayment($payment);

        //Order Type
        $orderAnet = new AnetAPI\OrderType();
        $orderAnet->setInvoiceNumber($subscriptionBox->getId());
        $orderAnet->setDescription('Subscription order from ' . $this->getStoreName());
        $subscription->setOrder($orderAnet);

        //Customer Type may be wrong here
        $customer = new AnetAPI\CustomerType();

        if ($subscriptionBox->getCustomerEmail()) {
            $customer->setEmail($subscriptionBox->getCustomerEmail());
        }
        if ($subscriptionBox->getCustomerId()) {
            //fix me
            $customer->setEmail($subscriptionBox->getCustomerId());
        }

        // set billing info, billTo
        $billingAdr = $subscriptionBox->getBillingAddress();

        if (!empty($billingAdr)) {
            $billTo = new AnetAPI\NameAndAddressType();
            $billingStr = $billingAdr->getStreet();
            $billTo->setFirstName($billingAdr->getFirstname());
            $billTo->setLastName($billingAdr->getLastname());
            $billTo->setAddress($billingStr[0]);

            $billTo->setCity($billingAdr->getCity());
            $billTo->setCompany($billingAdr->getCompany());
            $billTo->setCountry($billingAdr->getCountryId());
            $billTo->setState($billingAdr->getRegion());
            $billTo->setZip($billingAdr->getPostcode());

            $subscription->setBillTo($billTo);
        }

        // set shipping info
        $shippingAdr = $subscriptionBox->getShippingAddress();
        if (!empty($shippingAdr)) {
            $shipTo = new AnetAPI\NameAndAddressType();
            $shippingStr = $shippingAdr->getStreet();
            $shipTo->setAddress($shippingStr[0]);
            $shipTo->setFirstName($shippingAdr->getFirstname());
            $shipTo->setLastName($shippingAdr->getLastname());

            $shipTo->setCity($shippingAdr->getCity());
            $shipTo->setCompany($shippingAdr->getCompany());
            $shipTo->setCountry($shippingAdr->getCountryId());
            $shipTo->setState($shippingAdr->getRegion());
            $shipTo->setZip($shippingAdr->getPostcode());

            $subscription->setShipTo($shipTo);
        }

        $request = new AnetAPI\ARBCreateSubscriptionRequest();
        $request->setmerchantAuthentication($merchantAuthentication);
        if ($this->_useRef) {
            $request->setRefId('ref' . time());
        }
        $request->setSubscription($subscription);
        $controller = new AnetController\ARBCreateSubscriptionController($request);

        $this->_testMode = $this->_scopeConfig->getValue('payment/authorizenet_directpost/test');
        if ($this->_testMode) {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        } else {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
        }

        // If the code says Ok
        if ((!empty($response)) && ($response->getMessages()->getResultCode() == "Ok")) {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $authorizeResponse = $objectManager->create('Magenest\Subscription\Model\AuthorizeDirectPost\GatewayResponse', ['originalResponse' => $response]);
            return $authorizeResponse;
        } else {
            $this->_logger->debug(print_r($response, true));
            $errorMessages = $response->getMessages()->getMessage();
            throw new \Magento\Framework\Exception\LocalizedException(
                __($errorMessages[0]->getText())
            );
        }
    }

    /**
     * @param \Magenest\Subscription\Model\SubscriptionBox $subscriptionBox
     * @return int
     */
    public function getTotalOccurrences($subscriptionBox)
    {
        $cycles = $subscriptionBox->getCycles();
        $trialCycles = $subscriptionBox->getTrialCycles();
        $totalOccurrence = $cycles + $trialCycles;
        return min($totalOccurrence, 9999);
    }

    /**
     * @param $subscriptionId
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getProfileDetail($subscriptionId)
    {
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($this->_merchantId);
        $merchantAuthentication->setTransactionKey($this->_merchantTransactionKey);

        $request = new AnetAPI\ARBGetSubscriptionStatusRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setSubscriptionId($subscriptionId);

        if ($this->_useRef) {
            $request->setRefId('ref' . time());
        }

        $controller = new AnetController\ARBGetSubscriptionStatusController($request);

        if ($this->_testMode) {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        } else {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
        }

        if ((!empty($response)) && ($response->getMessages()->getResultCode() == 'Ok')) {
            $currentStatus = $response->getStatus();
            return $currentStatus;
        } else {
            $this->_logger->critical($response->getMessages()->getMessage());
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Something went wrong while trying to get subscription status.')
            );
        }
    }

    public function cancelProfile($subscriptionId)
    {
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($this->_merchantId);
        $merchantAuthentication->setTransactionKey($this->_merchantTransactionKey);

        $request = new AnetAPI\ARBCancelSubscriptionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        if ($this->_useRef) {
            $request->setRefId('ref' . time());
        }
        $request->setSubscriptionId($subscriptionId);
        $controller = new AnetController\ARBCancelSubscriptionController($request);

        if ($this->_testMode) {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        } else {
            $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);
        }

        if ((!empty($response)) && ($response->getMessages()->getResultCode() == "Ok")) {
            return true;

            return $response;
        } else {
            $this->_logger->critical($response->getMessages()->getMessage());
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Something went wrong while trying to cancel profile.')
            );
        }
    }

    /**
     * Get gateway code (authorizenet_directpost)
     * @return string
     */
    public function getCode()
    {
        return 'authorizenet_directpost';
    }

    /**
     * This is a gateway
     * @return bool
     */
    public function isGateway()
    {
        return true;
    }

    /**
     * Retrieve payment method online/offline flag
     *
     * @return bool
     */
    public function isOffline()
    {
        return false;
    }

    /**
     * invoked by customer to activate, cancel, suspend, reactive their recurring profile
     * @param string
     * @return mixed
     */
    public function invokeUserAction($profile_id, $action)
    {
        if ($action == 'cancel') {
            return $this->cancelProfile($profile_id);
        }
    }

    /**
     * invoke the payment API to check weather new payment is made for a recurring profile
     * @param $profile \Magenest\Subscription\Model\Profile
     * @return bool
     */
    public function updateInformationByGatewayInfo($profile)
    {
        //get the profile id which is assigned by authorize
        $profileId = $profile->getProfileId();
        return $profile;
    }

    public function getStoreName()
    {
        return $this->_scopeConfig->getValue(
            'general/store_information/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
