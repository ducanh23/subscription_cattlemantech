<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 19/09/2017
 * Time: 13:35
 */

namespace Magenest\Subscription\Model\AuthorizeDirectPost;

/**
 * Gateway response class
 * @package Magenest\Subscription\Model\AuthorizeDirectPost
 */
class GatewayResponse implements \Magenest\Subscription\Api\GatewayResponseInterface
{
    /**
     * @var string
     */
    protected $subscriptionId;

    /**
     * @var string
     */
    protected $customerProfileId;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var \net\authorize\api\contract\v1\MessagesType\MessageAType[]
     */
    protected $message;

    /**
     * @var String
     */
    protected $errorMessage;

    /**
     * @var \net\authorize\api\contract\v1\ARBCreateSubscriptionResponse
     */
    protected $originalResponse;

    /**
     * GatewayResponse constructor.
     * @param \net\authorize\api\contract\v1\ARBCreateSubscriptionResponse $originalResponse
     */
    public function __construct($originalResponse)
    {
        $this->originalResponse = $originalResponse;
        $this->subscriptionId = $originalResponse->getSubscriptionId();
        $this->customerProfileId = $originalResponse->getProfile()->getCustomerProfileId();

        if ($originalResponse->getMessages()->getResultCode() == 'Ok') {
            $this->status = "active";
        } else {
            $this->status = "error";
        }
        $this->message = $originalResponse->getMessages()->getMessage();
    }

    /**
     * get subscription id
     * @return string
     */
    public function getSubscriptionId()
    {
        return $this->subscriptionId;
    }

    /**
     * @return string
     */
    public function getProfileId()
    {
        return $this->customerProfileId;
    }

    /**
     * get status of response
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * get error messsage if it has one
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * get payment method code
     * @return string
     */
    public function getPaymentMethod()
    {
        return 'authorizenet_directpost';
    }
}
