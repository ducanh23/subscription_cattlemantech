<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 8/9/17
 * Time: 1:17 PM
 */

namespace Magenest\Subscription\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Model\AbstractModel;
use Magenest\Subscription\Model\ResourceModel\Relationship as Resource;
use Magenest\Subscription\Model\ResourceModel\Relationship\Collection as Collection;

class Relationship extends AbstractModel
{
    const PARENT_FIELD = 'parent_order';
    const CHILD_FIELD = 'child_order';
    const PROFILE_FIELD = 'profile_id';

    /**
     * Relationship constructor.
     * @param Context $context
     * @param \Magento\Framework\Registry $registry
     * @param Resource $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Registry $registry,
        Resource $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
    
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @param $parent
     * @param $child
     * @param $profileId
     * @return $this
     */
    public function addRelationship($parent, $child, $profileId)
    {
        if ($parent instanceof \Magento\Sales\Model\Order) {
            $parent = $parent->getIncrementId();
        } elseif ($parent instanceof \Magento\Quote\Model\Quote) {
            $parent = $parent->getReservedOrderId();
        }
        if ($child instanceof \Magento\Sales\Model\Order) {
            $child = $child->getIncrementId();
        }
        $this->setData([
            self::PARENT_FIELD => $parent,
            self::CHILD_FIELD => $child,
            self::PROFILE_FIELD => $profileId
        ]);
        return $this->save();
    }
}
