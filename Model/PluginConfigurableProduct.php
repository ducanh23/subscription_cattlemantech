<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 27/10/2016
 * Time: 13:53
 */
namespace Magenest\Subscription\Model;

class PluginConfigurableProduct
{
    /**
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $subject
     * @param callable $proceed
     * @param $optionProduct
     * @param $option
     * @param $product
     * @return $this
     */
    public function aroundAssignProductToOption(
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $subject,
        callable $proceed,
        $optionProduct,
        $option,
        $product
    ) {
        if ($optionProduct) {
            $option->setProduct($optionProduct);
        }
        return $this;
    }
}
