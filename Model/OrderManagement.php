<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 24/10/2017
 * Time: 13:34
 */

namespace Magenest\Subscription\Model;

use Magento\Framework\App\ObjectManager;

class OrderManagement extends \Magento\Quote\Model\QuoteManagement
{
    /**
     * @param $subscriptionBox
     * @param $payment
     * @return mixed
     * @throws \Exception
     */
    public function createBaseOrder($subscriptionBox, $payment, $isNominal = false)
    {
        $this->orderRepository = ObjectManager::getInstance()
            ->get(\Magento\Sales\Api\OrderRepositoryInterface::class);
        $orderData=[];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->orderFactory->create();

        $boxId = $subscriptionBox->getId();

        $subscriptionBox = $objectManager->create('Magenest\Subscription\Model\SubscriptionBox')->load($boxId);

        $quoteId = $subscriptionBox->getQuoteId();

        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);
        if (!$quote->getCustomerIsGuest()) {
            if ($quote->getCustomerId()) {
                $this->_prepareCustomerQuote($quote);
                //$this->customerManagement->validateAddresses($quote);
            }
            $this->customerManagement->populateCustomerInfo($quote);
        }
        $addresses = [];
        $quote->reserveOrderId();

        if ($quote->isVirtual()) {
            $this->dataObjectHelper->mergeDataObjects(
                \Magento\Sales\Api\Data\OrderInterface::class,
                $order,
                $this->quoteAddressToOrder->convert($quote->getBillingAddress(), $orderData)
            );
        } else {
            $this->dataObjectHelper->mergeDataObjects(
                \Magento\Sales\Api\Data\OrderInterface::class,
                $order,
                $this->quoteAddressToOrder->convert($quote->getShippingAddress(), $orderData)
            );
            $shippingAddress = $this->quoteAddressToOrderAddress->convert(
                $quote->getShippingAddress(),
                [
                    'address_type' => 'shipping',
                    'email' => $quote->getCustomerEmail()
                ]
            );
            $addresses[] = $shippingAddress;
            $order->setShippingAddress($shippingAddress);
            $order->setShippingMethod($quote->getShippingAddress()->getShippingMethod());
        }
        $billingAddress = $this->quoteAddressToOrderAddress->convert(
            $quote->getBillingAddress(),
            [
                'address_type' => 'billing',
                'email' => $quote->getCustomerEmail()
            ]
        );
        $addresses[] = $billingAddress;
        $order->setBillingAddress($billingAddress);
        $order->setAddresses($addresses);


        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $orderPayment = $objectManager->create('Magento\Sales\Model\Order\Payment');
        $paymentMethodCode = $payment->getData('payment_method');

        $orderPayment->setMethod($paymentMethodCode);

        $order->setPayment($orderPayment);

        $items = $this->resolveItems($quote);
        $subscriptionOptions = $this->getSubscriptionOption($quote);

        //inject data
        if ($items) {
            /**
             * @var  $quoteId
             * @var  \Magento\Sales\Model\Order\Item $orderItem
             */
            foreach ($items as $key => $orderItem) {
                $quoteId = $orderItem->getQuoteItemId();
                $productOptions = $orderItem ->getProductOptions();
                if (isset($subscriptionOptions[$quoteId]['optionName'])) {
                    $productOptions['additional_options'] = [
                        ['label' => "Subscription", 'value' => $subscriptionOptions[$quoteId]['optionName'],
                            'initial_fee'=>$subscriptionOptions[$quoteId]['initFee']]
                    ];
                    $orderItem->setProductOptions($productOptions);
                }

            }

        }

        $order->setItems($items);

        if ($quote->getCustomer()) {
            $order->setCustomerId($quote->getCustomer()->getId());
        }
        $order->setQuoteId($quote->getId());
        $order->setCustomerEmail($quote->getCustomerEmail());
        $order->setCustomerFirstname($quote->getCustomerFirstname());
        $order->setCustomerMiddlename($quote->getCustomerMiddlename());
        $order->setCustomerLastname($quote->getCustomerLastname());

        if ($isNominal) {
            $order->setData('state', \Magento\Sales\Model\Order::STATE_CLOSED)->setData('status', 'closed');
        }

        $this->eventManager->dispatch(
            'sales_model_service_quote_submit_before',
            [
                'order' => $order,
                'quote' => $quote
            ]
        );
        try {
            $this->orderRepository->save($order);
           // $quote->setIsActive(false);
            $this->eventManager->dispatch(
                'sales_model_service_quote_submit_success',
                [
                    'order' => $order,
                    'quote' => $quote
                ]
            );
            $quote->save();
           // $this->quoteRepository->save($quote);
        } catch (\Exception $e) {
            $this->eventManager->dispatch(
                'sales_model_service_quote_submit_failure',
                [
                    'order'     => $order,
                    'quote'     => $quote,
                    'exception' => $e
                ]
            );
            throw $e;
        }
        return $order;
    }

    /**
     * @param $order \Magento\Sales\Model\Order
     * @param $profile \Magenest\Subscription\Model\Profile
     */
    public function createBaseInvoice($order, $profile)
    {
        $payment = $order->getPayment();
        $payment->setTransactionId($profile->getData('profile_id'))
            ->setIsTransactionClosed(0);

        $order->save();

        $grossAmount = $order->getGrandTotal();

        $payment->registerCaptureNotification($grossAmount);
        $order->save();

        $invoice = $payment->getCreatedInvoice();
        if ($invoice) {
            // notify customer
            $message = __('Notified customer about invoice #%s.', $invoice->getIncrementId());
            /*  $order->queueNewOrderEmail()->addStatusHistoryComment($message)
                  ->setIsCustomerNotified(true)
                  ->save(); */
            $order->addStatusHistoryComment($message)
                ->setIsCustomerNotified(true)
                ->save();
        }
    }

    /**
     * @param $quote
     * @return array
     */
    public function getSubscriptionOption($quote)
    {
        $result = [];
        $visibleItems = $quote->getAllVisibleItems();
        if ($visibleItems) {
            foreach ($visibleItems as $item) {
                $itemOptions = $item->getOptions();
                $subscriptionOption = null;

                foreach ($itemOptions as $option) {
                    if ($option->getData('code') == 'subscription_options') {
                        $subscriptionOption = $option;
                        if (is_object($subscriptionOption)) {
                            $result[$item->getId()] = json_decode($subscriptionOption->getValue(), true);
                        }
                    }

                }
            }

        }
        return $result;
    }
}
