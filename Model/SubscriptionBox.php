<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 16/09/2017
 * Time: 11:03
 */

namespace Magenest\Subscription\Model;

use Magento\Framework\DataObject;
use Magento\Framework\Model\AbstractModel;
use Magenest\Subscription\Model\ResourceModel\SubscriptionBox as Resource;
use Magenest\Subscription\Model\ResourceModel\SubscriptionBox\Collection as Collection;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\App\ObjectManager;
use Magenest\Subscription\Api\SubscriptionManagementInterface;
use Magento\Quote\Model\QuoteFactory;

/**
 * SubscriptionBox model
 *
 * Supported events:
 *  magenest_subscription_subscription_box_load_after
 *  magenest_subscription_subscription_box_save_before
 *  magenest_subscription_subscription_box_save_after
 *  magenest_subscription_subscription_box_delete_before
 *  magenest_subscription_subscription_box_delete_after
 *
 * @api
 * @method float getInitFee()
 * @method string getOptionName()
 * @method int getFrequency()
 * @method int getMaxCycles()
 * @method string getTrialEnabled()
 * @method int getTrialAmount()
 * @method int getTrialUnit()
 * @method string getSelectedUnit()
 * @method int getTrialFrequency()
 * @method int getTrialMaxCycles()
 * @method string getInitEnabled()
 * @method string getStartDate()
 * @method int getQty()
 * @method string getDescription()
 * @method int getMaximumPaymentFailures()
 * @method int getAutoBill
 * @method int getQuoteId();
 *
 * @method float getShippingAmt()
 * @method float getTaxAmt()
 *
 * @method string getQuoteItemId()
 *
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 2.0.0
 */
class SubscriptionBox extends AbstractModel
{
    protected $_eventPrefix = 'magenest_subscription_subscription_box';

    /**
     * @var SubscriptionManagementInterface
     */
    protected $subscriptionInformation;

    /**
     * @var float
     */
    protected $shippingAmt;

    /**
     * @var float
     */
    protected $taxAmt;

    /**
     * @var float
     */
    protected $firstTimeDiscountAmt;

    /**
     * @var array
     */
    protected $productIds = [];

    /**
     * @var array
     */
    protected $itemIds = [];

    /**
     * @var string
     */
    protected $name;

    /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var array
     */
    protected $items = [];

    /**
     * SubscriptionBox constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Resource $resource
     * @param Collection $resourceCollection
     * @param SubscriptionManagementInterface $subscriptionManagement
     * @param QuoteFactory $quoteFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Resource $resource,
        Collection $resourceCollection,
        SubscriptionManagementInterface $subscriptionManagement,
        QuoteFactory $quoteFactory,
        $data = []
    ) {
    
        $this->subscriptionInformation = $subscriptionManagement;
        $this->quoteFactory = $quoteFactory;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * get all items in the subscription box
     * @return mixed|string
     */
    public function getName()
    {
        $name = $this->name;
        if (!$name) {
            $name = $this->getData('option_name');
        }
        return $name;
    }

    /**
     * get the unique name of the subscription box if necessary
     * @return string
     */
    public function getUniqueName()
    {
        return $this->getName();
    }

    /**
     * get start date of recurring profile in well-formatted string
     * @return string
     */
    public function getFormattedDateStart()
    {
        $startDate = $this->getData('start_date');
        if (!$startDate) {
            $today = new \DateTime();
            return $today->format('Y-m-d H:i:s');
        } else {
            $date = \DateTime::createFromFormat('Y-m-d', $startDate);
            return $date->format('Y-m-d H:i:s');
        }
    }

    /**
     * load box by item ids
     * @param array $itemIds
     * @return \Magenest\Subscription\Model\SubscriptionBox
     */
    public function loadByItemIds($itemIds)
    {
        $resource = $this->_resource;
        $boxId = $resource->loadByItemIds($itemIds);
        if ($boxId) {
            return $this->load($boxId);
        } else {
            return $this;
        }
    }

    public function extractShippingAddress()
    {

    }

    /**
     * get recurring paid amount each billing period
     * @return float
     */
    public function getAmount()
    {
        return floatval($this->getData('grand_total'));
    }

    /**
     * whether this subscription box has trial information
     * @return bool
     */
    public function hasTrialInformation()
    {
        $trialFrequency = $this->getTrialFrequency();
        if ($trialFrequency) {
            return true;
        }
        return false;
    }

    /**
     * get the quote that is associated with the subscription box
     */
    public function getAssociatedQuote()
    {

    }

    /**
     * Get billing address
     * @return \Magento\Quote\Model\Quote\Address
     */
    public function getBillingAddress()
    {
        $quoteId = $this->getData('quote_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);

        $billingAddress = $quote->getBillingAddress();
        return $billingAddress;
    }

    public function getOriginalQuoteId()
    {
    }

    /**
     * @return string
     */
    public function getCustomerEmail()
    {
        $quoteId = $this->getData('quote_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);
        $customerEmail = $quote->getCustomerEmail();
        return $customerEmail;
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        $quoteId = $this->getData('quote_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);
        $customerId = $quote->getCustomerId();
        return $customerId;
    }

    /**
     * @return string
     */
    public function getSubscriberName()
    {
        $quoteId = $this->getData('quote_id');
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote  =  $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId) ;
        $customerName = $quote->getCustomerFirstname() . " " .$quote->getCustomerLastname();
        return $customerName;
    }

    /**
     * @return \Magento\Quote\Model\Quote\Address
     */
    public function getShippingAddress()
    {
        $quoteId = $this->getData('quote_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);

        $shippingAddress = $quote->getShippingAddress();
        return $shippingAddress;
    }

    /**
     *get period unit such as month week or day
     * @return int
     */
    public function getPeriodUnit()
    {
        return $this->getData('selected_unit');
    }

    /**
     * get base currency code
     * @return string
     */
    public function getBaseCurrencyCode()
    {
        $this->getQuote()->getBaseCurrencyCode();
    }

    /**
     * get the quote associated with the subscription box
     * @return \Magento\Quote\Model\Quote|null
     */
    public function getQuote()
    {
        if ($this->quote) {
            return $this->quote;
        }

        $quoteId = $this->getQuoteId();
        if (!$quoteId) {
            return null;
        }

        $quote = $this->quoteFactory->create()->load($quoteId);
        $this->quote = $quote;
        return $this->quote;
    }

    /**
     * get next billing date in welled-format
     */
    public function getNextBillingDate()
    {
        /** @var \DateTime $startDate */
        $startDateStr = $this->getStartDate();
        if ($startDateStr) {
            $startDate = \DateTime::createFromFormat('Y-m-d', $startDateStr);
        } else {
            $startDate = new \DateTime();
        }

        $modifyDelta = 0;
        $unit = $this->getSelectedUnit();

        if ($unit == 'semimonth') {
            $modifyDelta = strval($this->getFrequency()*2);
        } else {
            $modifyDelta = strval($this->getFrequency());
        }

        if ($modifyDelta == '1' && $unit != 'semimonth') {
            $modify = '+' . $modifyDelta . ' ' . $unit;
        } else {
            $modify = '+' . $modifyDelta . ' weeks';
        }

        $startDate->modify($modify);
        $formattedDate = $startDate->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT);
        return $formattedDate;
    }

    /**
     * get the maximum cycle of recurring billing
     * @return int
     */
    public function getCycles()
    {
        return $this->getData('max_cycles');
    }

    /**
     * get trial information of recurring billing
     * @return string|null
     */
    public function getTrialInfo()
    {

    }

    /**
     * @return int
     */
    public function getTrialCycles()
    {
        return $this->getData('trial_max_cycles');
    }

    /**
     * @return int
     */
    public function getTimeUnit()
    {
        return $this->getData('selected_unit');
    }

    /**
     * get all the quote item
     * @return array
     */
    public function getAllItems()
    {
        $quoteItemId = unserialize($this->getQuoteItemId());
        $objectManager = ObjectManager::getInstance();
        $quoteId = $this->getQuoteId();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId);

        if (!$this->itemIds || !is_array($this->itemIds) || empty($this->itemIds)) {
            $this->itemIds = $quoteItemId;
        }
        $itemIds = $this->itemIds;

        if (is_array($itemIds) > 0 && !empty($itemIds)) {
            foreach ($itemIds as $itemId) {

                /** @var \Magento\Quote\Model\Quote\Item $item */
               /* $item = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($itemId);
                $item->setQuote($quote);
                $this->items[] = $item;*/
                $allItems = $quote->getAllItems();
                foreach ($allItems as $quoteItem) {
                    if ($quoteItem->getId() == $itemId && !in_array($itemId, $this->items)) {
                        $this->items[] = $quoteItem;
                    }
                }
            }
        }
        return $this->items;
    }

    /**
     * get subtotal
     * @return float
     */
    public function getSubtotal()
    {
        $total  = 0;
        $allItems = $this->getAllItems();
        if (!empty($allItems)) {

            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($allItems as $item) {
                $item->getPrice();
                $total += $item->getBasePrice();
            }
        }
        return $total;
    }

    /**
     * set the item id of quote item
     * @param $item_ids
     */
    public function setItemIds($item_ids)
    {
        $this->itemIds = $item_ids;
        $this->setQuoteItemId(serialize($item_ids));
    }

    public function getCustomerInformation()
    {

    }

    public function getAllProductIds()
    {
        $itemIds = $this->itemIds;

        if (count($itemIds) > 0) {
            foreach ($itemIds as $itemId) {
                $objectManager = ObjectManager::getInstance();

                /** @var \Magento\Quote\Model\Quote\Item $item */
                $item = $objectManager->create('Magento\Quote\Model\Quote\Item')->load($itemId);
                $this->productIds[] = $item->getProduct()->getId();
            }
        }
        return $this->productIds;
    }

    /**
     * get shipping fee
     */
    public function getShippingFee()
    {
        $shippingAmt = 0;
        if (!$this->items) {
            return;
        }
        foreach ($this->items as $item) {
            $shippingAmt += floatval($item->getShippingAmt());
        }
        return $shippingAmt;
    }

    public function getFirstTimeDiscountFee()
    {

    }

    /**
     * @return float|int|void
     */
    public function getTaxFee()
    {
        $taxAmt = 0;
        if ($this->items) {
            foreach ($this->items as $item) {
                $taxAmt += floatval($item->getTaxAmt());
            }
        }
        return $taxAmt;
    }

    /**
     * @param $subscriptionInformation
     */
    public function setSubscriptionInformation($subscriptionInformation)
    {
        if (is_array($subscriptionInformation)) {
            $subscriptionInfo = new DataObject();
            $subscriptionInfo->addData($subscriptionInformation);
            $this->subscriptionInformation = $subscriptionInfo;
        } elseif ($subscriptionInformation instanceof DataObject) {
            $this->subscriptionInformation = $subscriptionInformation;
        }
    }

    /**
     * get items collection
     */
    public function getItemsCollection()
    {

    }

    /**
     * Check quote for virtual product only
     *
     * @return bool
     */
    public function isVirtual()
    {
        $isVirtual = true;
        $countItems = 0;
        foreach ($this->getAllItems() as $_item) {
            /* @var $_item \Magento\Quote\Model\Quote\Item */
            if ($_item->isDeleted() || $_item->getParentItemId()) {
                continue;
            }
            $countItems++;

            if (!$_item->getQuote()) {
                $quote = $this->getQuote();
                $_item->setQuote($quote);
            }
            if (!$_item ->getProductId()) {
                continue;
            }

            if (!$_item->getProduct()->getIsVirtual()) {
                $isVirtual = false;
                break;
            }
        }
        return $countItems == 0 ? false : $isVirtual;
    }

    public function getWeight()
    {
        $weight = 0;
        $items = $this->getAllItems();
        if (!empty($items)) {
            /** @var \Magento\Quote\Model\Quote\Item $item */
            foreach ($items as $item) {
                $weight +=$item->getWeight();
            }

        }
        return $weight;
    }

    /**
     * get the total qty of the box
     * @return int
     */
    public function getTotalQtyOrdered()
    {
        $totalQty = 0;
        $allItems = $this->getAllItems();
        if (is_array($allItems)) {
            if (count($allItems > 0)) {
                /** @var \Magento\Quote\Model\Quote|Item $item */
                foreach ($allItems as $item) {
                    $totalQty += intval($item->getQty());
                }
            }
        }
        return $totalQty;
    }

    /**
     * @return SubscriptionManagementInterface
     */
    public function getSubscriptionInformation()
    {
        return $this->subscriptionInformation;
    }
}
