<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Model\Source\Email;

use Magento\Config\Model\Config\Source\Email\Template as ParentTemplate;

class Template extends ParentTemplate
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $options = parent::toOptionArray();
        array_unshift($options, ['value' => -1, 'label' => "Don't send email"]);
        return $options;
    }
}
