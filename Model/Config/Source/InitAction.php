<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 23/07/2016
 * Time: 10:56
 */
namespace Magenest\Subscription\Model\Config\Source;

class InitAction implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'ContinueOnFailure', 'label' => __('Continue on failure')],
            ['value' => 'CancelOnFailure', 'label' => __('Cancel on failure')]
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'ContinueOnFailure' => __('Continue on failure'),
            'CancelOnFailure' => __('Cancel on failure')
        ];
    }
}
