<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 31/03/2017
 * Time: 16:41
 */
namespace Magenest\Subscription\Model\Config\Source;

class DisplayStyle implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'dropdown', 'label' => __('Dropdown')],
            ['value' => 'radio', 'label' => __('Radio List')]
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'dropdown' => __('Dropdown'),
            'radio' => __('Radio List')
        ];
    }
}
