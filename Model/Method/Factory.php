<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 18/09/2017
 * Time: 15:33
 */
namespace Magenest\Subscription\Model\Method;

class Factory
{
    /**
     * Object manager
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Construct
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Creates new instances of payment method models
     *
     * @param string $className
     * @param array $data
     * @return \Magento\Payment\Model\MethodInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function create($className, $data = [])
    {
        $method = $this->objectManager->create($className, $data);
        if (!$method instanceof \Magenest\Subscription\Api\MethodInterface) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('%1 class doesn\'t implement \Magenest\Subscription\Api\MethodInterface', $className)
            );
        }
        return $method;
    }
}
