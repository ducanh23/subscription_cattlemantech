<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 23/03/2016
 * Time: 19:30
 */

namespace Magenest\Subscription\Model;

use Magenest\Subscription\Model\ResourceModel\Profile as Resource;
use Magenest\Subscription\Model\ResourceModel\Profile\Collection as Collection;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Quote\Model\Quote\Item\ToOrderItem as ToOrderItemConverter;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magenest\Subscription\Model\OrderManagement;
use Magento\Framework\DataObject;
use Magento\Sales\Model\Service\InvoiceService;

/**
 * Profile model
 *
 * Supported events:
 *  magenest_subscription_profile_load_after
 *  magenest_subscription_profile_save_before
 *  magenest_subscription_profile_save_after
 *  magenest_subscription_profile_delete_before
 *  magenest_subscription_profile_delete_after
 *
 * @api
 * @method string getProfileId()
 * @method string getState()
 * @method string getOrderId()
 * @method int getCustomerId()
 * @method string getMethodCode()
 * @method string getCreatedAt()
 * @method string getUpdatedAt()
 * @method string getSubscriberName()
 * @method string getStartDatetime()
 * @method string getNextBillingDate()
 * @method int getCyclesCompleted()
 * @method int getCyclesRemaining()
 * @method float getOutstandingBalance()
 * @method int getFailedPayments()
 * @method float getTrialAmtPaid()
 * @method float getRegularAmtPaid()
 * @method int getProductId()
 * @method string getProductName()
 * @method int|null getBoxId()
 *
 * @method int getSuspensionThreshold()
 * @method string getPeriodUnit()
 * @method int getPeriodFrequency()
 * @method int getPeriodMaxCycles()
 *
 * @method float getBillingAmount()
 * @method float getTaxAmount()
 * @method float getShippingAmount()
 *
 * @method string getTrialInfo()
 * @method string getInitInfo()
 * @method string getAdditionalInfo()
 * @method string getSequenceOrderIds()
 *
 * @method float getAggregateAmount()
 * @method float getAggregateOptionalAmount()
 * @method string getFinalPaymentDuedate()
 *
 * @method string getShipToName()
 * @method string getShipToStreet()
 * @method string getShipToCity()
 * @method string getShipToZip()
 * @method string getShipToCountry()
 * @method string getShipToState()
 *
 * @method string getTrialBillingPeriod()
 * @method int getTrialBillingFrequency()
 * @method int getTrialBillingCycles()
 * @method float getTrialAmt()
 * @method float getTrialShippingAmt()
 * @method float getTrialTaxAmt()
 *
 * @method float getInitAmt()


 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @since 2.0.0
 */

class Profile extends AbstractModel
{
    const STATE_ACTIVE = 'active';

    const STATE_SUSPENDED = 'suspended';

    const STATE_PENDING = 'pending';

    const STATE_CANCELLED = 'cancelled';

    const STATE_EXPIRED = 'expired';

    const STATE_TERMINATED = 'terminated';

    protected $_eventPrefix = 'magenest_subscription_profile';

    /**
     * @var OrderFactory
     */
    protected $orderFactory;

    /**
     * @var \Magento\Framework\Event\Manager
     */
    protected $_eventManger;

    /**
     * @var ToOrderItemConverter
     */
    protected $quoteItemToOrderItem;

    /**
     * @var \Magenest\Subscription\Model\OrderManagement
     */
    protected $orderManagement;

    /**
     * @var InvoiceService
     */
    protected $invoiceService;

    /**
     * Profile constructor.
     * @param Context $context
     * @param Registry $registry
     * @param Resource $resource
     * @param Collection $resourceCollection
     * @param OrderFactory $orderFactory
     * @param ToOrderItemConverter $toOrderItemConverter
     * @param \Magenest\Subscription\Model\OrderManagement $orderManagement
     * @param InvoiceService $invoiceService
     * @param \Magento\Framework\Event\Manager $eventManager
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Resource $resource,
        Collection $resourceCollection,
        OrderFactory $orderFactory,
        ToOrderItemConverter $toOrderItemConverter,
        OrderManagement $orderManagement,
        InvoiceService $invoiceService,
        \Magento\Framework\Event\Manager $eventManager,
        $data = []
    ) {
    
        $this->orderFactory = $orderFactory;
        $this->orderManagement = $orderManagement;
        $this->quoteItemToOrderItem = $toOrderItemConverter;
        $this->invoiceService = $invoiceService;
        $this->_eventManger = $eventManager;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    public function placeOrder()
    {
        /** @var \Magento\Sales\Model\Order $newOrder */
        $newOrder = $this->generateOrder();
        $this->addSequenceOrder($newOrder->getId());

        return $newOrder;
    }

    /**
     * calculate the next billing date
     * return the datetime in format Y-m-d H:i:s
     * @return string|null
     */
    public function calculateNextBillingDate()
    {
        if (!$this->getId()) {
            return;
        }
        //next billing date has format Y-m-d H:i:s for example 2017-10-29 11:13:08
        $nextBillingDate = $this->getNextBillingDate();
        $nextBillingDateObj = \DateTime::createFromFormat('Y-m-d H:i:s', $nextBillingDate);
        //unit's value could be week,semimonth,month,year
        $unit = $this->getPeriodUnit();

        if ($unit == 'semimonth') {
            $modifyDelta = strval($this->getPeriodFrequency() * 2);
        } else {
            $modifyDelta = strval($this->getPeriodFrequency());
        }

        if ($modifyDelta == '1' && $unit != 'semimonth') {
            $modify = '+' . $modifyDelta . ' ' . $unit;
        } else {
            $modify = '+' . $modifyDelta . ' ' . $unit . 's';
        }

        $nextBillingDateObj->modify($modify);
        $formattedDate = $nextBillingDateObj->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT);
        return $formattedDate;
    }

    public function generateOrderBaseOnBox()
    {
        //the order id of first related order of the subscription
        $orderId = $this->getFirstOrderId();

        if ($orderId) {
            /** @var  \Magento\Sales\Model\Order $order */
            $order = $this->orderFactory->create()->load($orderId);
            $newOrder = $this->orderFactory->create();
            $orderInfo = $order->getData();
            try {
                $objectManager = ObjectManager::getInstance();

                $billingAdd = $objectManager->create('Magento\Sales\Model\Order\Address');
                $oriBA = $order->getBillingAddress()->getData();
                $billingAdd->setData($oriBA)->setId(null);

                $shippingAdd = $objectManager->create('Magento\Sales\Model\Order\Address');
                $shippingInfo = $order->getBillingAddress()->getData();
                $shippingAdd->setData($shippingInfo)->setId(null);

                /** @var \Magento\Sales\Model\Order\Payment $payment */
                $payment = $objectManager->create('Magento\Sales\Model\Order\Payment');
                $paymentMethodCode = $order->getPayment()->getMethod();

                $payment->setMethod($paymentMethodCode);


                $transferDataKeys = array(
                    'store_id', 'store_name', 'customer_id', 'customer_email',
                    'customer_firstname', 'customer_lastname', 'customer_middlename', 'customer_prefix',
                    'customer_suffix', 'customer_taxvat', 'customer_gender', 'customer_is_guest',
                    'customer_note_notify', 'customer_group_id', 'customer_note', 'shipping_method',
                    'shipping_description', 'base_currency_code', 'global_currency_code', 'order_currency_code',
                    'store_currency_code', 'base_to_global_rate', 'base_to_order_rate', 'store_to_base_rate',
                    'store_to_order_rate'
                );


                foreach ($transferDataKeys as $key) {
                    if (isset($orderInfo[$key])) {
                        $newOrder->setData($key, $orderInfo[$key]);
                    } elseif (isset($shippingInfo[$key])) {
                        $newOrder->setData($key, $shippingInfo[$key]);
                    }
                }

                $storeId = $order->getStoreId();
                $newOrder->setStoreId($storeId)
                    ->setState(Order::STATE_NEW)
                    ->setStatus('pending')
                    ->setBaseToOrderRate($order->getBaseToOrderRate())
                    ->setStoreToOrderRate($order->getStoreToOrderRate())
                    ->setOrderCurrencyCode($order->getOrderCurrencyCode())
                    ->setBaseSubtotal($order->getBaseSubtotal())
                    ->setSubtotal($order->getSubtotal())
                    ->setBaseShippingAmount($order->getBaseShippingAmount())
                    ->setShippingAmount($order->getShippingAmount())
                    ->setBaseTaxAmount($order->getBaseTaxAmount())
                    ->setTaxAmount($order->getTaxAmount())
                    ->setBaseGrandTotal($order->getGrandTotal())
                    ->setGrandTotal($order->getGrandTotal())
                    ->setIsVirtual($order->getIsVirtual())
                    ->setWeight($order->getWeight())
                    ->setTotalQtyOrdered($this->getInfoValue('order_info', 'items_qty'))
                    ->setTotalQtyOrdered($order->getTotalQtyOrdered())
                    ->setBillingAddress($billingAdd)
                    ->setShippingAddress($shippingAdd)
                    ->setPayment($payment);

                /** @var \Magento\Sales\Model\Order\Item[] $items */
                $items = $order->getAllVisibleItems();
                foreach ($items as $item) {
                    $newOrderItem = clone $item;
                    $newOrderItem->setId(null);
                    $newOrder->addItem($newOrderItem);
                }

                $newOrder->save();
                $this->createInvoice($newOrder);
            } catch (\Exception $e) {
                throw $e;
            }
            return $newOrder;
        } else {
            //generate the first time order
            $boxId = $this->getBoxId();
            $objectManager = ObjectManager::getInstance();

            $subscriptionBox = $objectManager->create('Magenest\Subscription\Model\SubscriptionBox')->load($boxId);
            /** @var \Magenest\Subscription\Model\SubscriptionManagement $manager */
            $payment = new DataObject();

            $payment->addData([
                    'payment_method' => $this->getMethodCode()
                ]);

            //activate the quote
            $quoteId = $subscriptionBox->getQuoteId();
            $quote = $objectManager->create('Magento\Quote\Model\Quote')->load($quoteId)->setActive(true);
            $quote->save();
            $newOrder = $this->orderManagement->createBaseOrder($subscriptionBox, $payment);
            $subscriptionBox->setOrderId($newOrder->getId()) ->save();
            $this->createInvoice($newOrder);

        }
        return null;
    }

    /**
     * @return string
     */
    public function getFirstOrderId()
    {
        $orderId = $this->getData('order_id');
        return $orderId;

    }

    public function addSequenceOrder($orderId)
    {
        $sequenceOrderIds = $this->getData('sequence_order_ids');

        if (!$sequenceOrderIds) {
            $this->addData([
                "sequence_order_ids" => serialize([$orderId])
            ])->save();
        } else {
            $sequenceOrderIds = unserialize($sequenceOrderIds);
            array_push($sequenceOrderIds, $orderId);

            $this->addData([
                "sequence_order_ids" => serialize($sequenceOrderIds)
            ])->save();
        }
        return $this;
    }

    public function createInvoice($order)
    {
        $this->_eventManger->dispatch(
            'magenest_subscription_pay_subscription',
            ['profile' => $this]
        );

        $payment = $order->getPayment();
        $payment->setTransactionId($this->getData('profile_id'))
            ->setIsTransactionClosed(0);

        $order->save();

        //the gross amount
        $grossAmount = $order->getGrandTotal();

        $payment->registerCaptureNotification($grossAmount);
        $order->save();

        $invoice = $payment->getCreatedInvoice();
        if ($invoice) {
            // notify customer
            $message = __('Notified customer about invoice #%s.', $invoice->getIncrementId());
            /*  $order->queueNewOrderEmail()->addStatusHistoryComment($message)
                  ->setIsCustomerNotified(true)
                  ->save(); */
            $order->addStatusHistoryComment($message)
                ->setIsCustomerNotified(true)
                ->save();
        }

    }

    /**
     * @param \Magento\Sales\Model\Order $order
     */
    public function sendInvoice($order)
    {
        /** @var \Magento\Sales\Model\Order\Payment $payment */
        $payment = $order->getPayment();
        $payment->setTransactionId($this->getData('profile_id'))->setIsTransactionClosed(0);
        $payment->registerCaptureNotification($order->getGrandTotal());
        $invoice = $payment->getCreatedInvoice();
       // $order->setData('increment_id' , null);

       // $order->save();
        if ($invoice) {
            // notify customer
            $message = __('Notified customer about invoice #%s.', $invoice->getIncrementId());
            $order->addStatusHistoryComment($message)
                ->setIsCustomerNotified(true)
                ->save();
        }
    }

    /**
     * @param $profileId
     * @return \Magenest\Subscription\Model\Profile
     */
    public function loadByProfileId($profileId)
    {
        return $this->getCollection()->addFieldToFilter('profile_id', $profileId)->getFirstItem();
    }

    /**
     * Save recurring payment profile
     * @param $data
     * @return $this
     */
    public function saveProfile($data)
    {
        $this->setData($data);
        return $this->save();
    }

    /**
     * update profile data base on the information from paypal gateway
     * @param $response
     * @return $this;
     */

    public function updateProfileBaseOnInfoFromPaypal($response)
    {
        if (!$this->getId()) {
            return;
        }
        $trial_info = '';
        if (array_key_exists('TRIALAMT', $response)) {
            $trial_info = [
                'trial_billing_period' => $response['TRIALBILLINGPERIOD'],
                'trial_billing_frequency' => $response['TRIALBILLINGFREQUENCY'],
                'trial_billing_cycles' => $response['TRIALTOTALBILLINGCYCLES'],
                'trial_amt' => $response['TRIALAMT'],
                'trial_tax_amt' => $response['TRIALTAXAMT'],
                'trial_shipping_amt' => $response['TRIALSHIPPINGAMT']
            ];
        }

        $init_info = '';
        if (array_key_exists('INITAMT', $response)) {
            $init_info = [
                'init_amt' => $response['INITAMT']
            ];
        }

        $shipping_info = [
            'ship_to_name' => $response['SHIPTONAME'],
            'ship_to_street' => $response['SHIPTOSTREET'],
            'ship_to_city' => $response['SHIPTOCITY'],
            'ship_to_zip' => $response['SHIPTOZIP'],
            'ship_to_country' => $response['SHIPTOCOUNTRY']
        ];

        if (array_key_exists('SHIPTOSTATE', $response)) {
            $shipping_info['ship_to_state'] = $response['SHIPTOSTATE'];
        }

        $profileData = [
            // Response general fields
            'profile_id' => $response['PROFILEID'],
            'state' => $response['STATUS'],
            'suspension_threshold' => $response['MAXFAILEDPAYMENTS'],
            'aggregate_amount' => $response['AGGREGATEAMT'],
            'aggregate_optional_amount' => $response['AGGREGATEOPTIONALAMT'],
            'final_payment_duedate' => $response['FINALPAYMENTDUEDATE'],

            // Profile Detail fields
            'subscriber_name' => $response['SUBSCRIBERNAME'],
            'start_datetime' => $response['PROFILESTARTDATE'],

            // Summary detail fields
//            'next_billing_date' => $response['NEXTBILLINGDATE'],
            'cycles_completed' => $response['NUMCYCLESCOMPLETED'],
            'cycles_remaining' => $response['NUMCYCLESREMAINING'],
            'outstanding_balance' => $response['OUTSTANDINGBALANCE'],
            'failed_payments' => $response['FAILEDPAYMENTCOUNT'],
            'trial_amt_paid' => $response['TRIALAMTPAID'],
            'regular_amt_paid' => $response['REGULARAMTPAID'],

            // Billing period fields
            'period_unit' => $response['REGULARBILLINGPERIOD'],
            'period_frequency' => $response['REGULARBILLINGFREQUENCY'],
            'period_max_cycles' => $response['REGULARTOTALBILLINGCYCLES'],
            'billing_amount' => $response['REGULARAMT'],
            'tax_amount' => $response['REGULARTAXAMT'],
            'shipping_amount' => $response['REGULARSHIPPINGAMT'],

            // Trial and init info
            'trial_info' => serialize($trial_info),
            'init_info' => serialize($init_info),
            'additional_info' => serialize($shipping_info)
        ];

        //todo: use box instead of order

//        if ($order) {
//            /** @var \Magento\Catalog\Model\Product $product */
//            $product = $order->getItemsCollection()->getFirstItem()->getProduct();
//
//            // Magento fields
//            $profileData['order_id'] = $order->getId();
//            $profileData['customer_id'] = $order->getCustomerId();
//            $profileData['method_code'] = 'paypal_express_checkout';
//            $profileData['product_id'] = $product->getId();
//            $profileData['product_name'] = $product->getName();
//        }

        $this->addData($profileData)->save();

        return $this;
    }
}
