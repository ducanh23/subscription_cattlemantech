<?php
/**
 * Created by PhpStorm.
 * User: ducanh
 * Date: 01/06/2018
 * Time: 14:01
 */

namespace Magenest\Subscription\Model\Total;

class Fee extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    protected $quoteValidator = null;

    public function __construct(\Magento\Quote\Model\QuoteValidator $quoteValidator)
    {
        $this->quoteValidator = $quoteValidator;
    }

    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    )
    {
        parent::collect($quote, $shippingAssignment, $total);
        $feeWithoutVAT = 0;

        foreach ($quote->getAllItems() as $item) {
            if ($item->getData('tax_percent') != null) {
            $taxPercent = $item->getData('tax_percent');
            }else {
                $taxPercent = 0;}
            $feeWithoutVAT = substr($item->getBuyRequest()->getData("additional_options")["Initial Fee"], 2) / 2;
        }
//        $fee = $feeWithoutVAT + ($feeWithoutVAT * $taxPercent / 100);
        $taxAmount = $quote->getShippingAddress()->getData('tax_amount');
        if (isset($taxPercent)) {
            $fee = $feeWithoutVAT + ($feeWithoutVAT * $taxPercent / 100);
        } else {
            $fee = $feeWithoutVAT;
        }
        $fee = $feeWithoutVAT;
        $exist_amount = 0;
        $balance = $fee - $exist_amount;

        $total->setTotalAmount('fee', $balance);
        $total->setBaseTotalAmount('fee', $balance);

        $total->setFee($balance);
        $total->setBaseFee($balance);

        $total->setGrandTotal($total->getGrandTotal() + $balance);
        $total->setBaseGrandTotal($total->getBaseGrandTotal() + $balance);


        return $this;
    }

    protected function clearValues(\Magento\Quote\Model\Quote\Address\Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
    }
    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return array|null
     */
    /**
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        foreach ($quote->getAllItems() as $item) {
            $feeWithoutVAT = (int)substr($item->getBuyRequest()->getData("additional_options")["Initial Fee"], 2);
            $fee = $feeWithoutVAT + ($feeWithoutVAT * $item->getData('tax_percent') / 100);
//            $fee = $feeWithoutVAT;
        }
        return [
            'code' => 'fee',
            'title' => 'Initial Fee + VAT',
            'value' => $fee
        ];
    }

    /**
     *
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('Initial Fee and VAT');
    }

}
