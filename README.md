#Magenest Subscription and Recurring Payment
Thank you for buying our product.
This extension address to [Magenest](https://store.magenest.com/).

- If you have trouble installing this extension, please visit [Our installation guide](http://confluence.izysync.com/display/DOC/1.+Subscription+and+Recurring+Payment+Installation+Guides).

- For detailed user guide of this extension, please visit [Our user guide](http://confluence.izysync.com/display/DOC/2.+Subscriptions+and+Recurring+Payments+User+Guides).

- Support portal: http://servicedesk.izysync.com/servicedesk/customer/portal/23

## [Requirement](http://devdocs.magento.com/guides/v2.1/install-gde/system-requirements.html)
1. [Magento 2.1](https://github.com/magento/magento2) or [Magento 2.2](https://github.com/magento/magento2)
1. PHP >= 5.6.x
2. Apache2
3. MySQL 5.6
4. Composer

## User guide
### Setup
- run composer require Magenest/module-subscription
- php bin/magento setup:upgrade
- php bin/magento setup:static-content:deploy
- php bin/magento cache:flush
 Be sure to config the correct permission in your Magento system
