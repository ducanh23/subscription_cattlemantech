<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 20/07/2016
 * Time: 13:25
 */
namespace Magenest\Subscription\Setup;

use Magento\Catalog\Model\Product;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    protected $_eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory
    ) {
        $this->_eavSetupFactory = $eavSetupFactory;
    }

    /**
     * Upgrade method
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->updateAttribute(\Magento\Catalog\Model\Product::ENTITY, 'enable_subscription', 'frontend_input', 'boolean');
            $eavSetup->updateAttribute(\Magento\Catalog\Model\Product::ENTITY, 'customer_define_startdate', 'frontend_input', 'boolean');
            $eavSetup->updateAttribute(\Magento\Catalog\Model\Product::ENTITY, 'auto_bill', 'frontend_input', 'boolean');
        }

        if (version_compare($context->getVersion(), '1.0.2', '<')) {
            $eavSetup = $this->_eavSetupFactory->create(['setup' => $setup]);

            $eavSetup->updateAttribute(\Magento\Catalog\Model\Product::ENTITY, 'enable_subscription', 'apply_to', 'simple,virtual,downloadable,configurable');
            $eavSetup->updateAttribute(\Magento\Catalog\Model\Product::ENTITY, 'customer_define_startdate', 'apply_to', 'simple,virtual,downloadable,configurable');
            $eavSetup->updateAttribute(\Magento\Catalog\Model\Product::ENTITY, 'auto_bill', 'apply_to', 'simple,virtual,downloadable,configurable');
            $eavSetup->updateAttribute(\Magento\Catalog\Model\Product::ENTITY, 'maximum_payment_failures', 'apply_to', 'simple,virtual,downloadable,configurable');
        }

        $setup->endSetup();
    }
}
