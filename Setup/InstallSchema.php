<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/03/2016
 * Time: 16:10
 */
namespace Magenest\Subscription\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table as Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Setup method
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()->newTable($installer->getTable('magenest_subscription_product_attribute'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'attribute_id',
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'Attribute ID'
            )
            ->addColumn(
                'store_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Store ID'
            )
            ->addColumn(
                'entity_id',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'Product ID'
            )
            ->addColumn(
                'value',
                Table::TYPE_TEXT,
                null,
                [],
                'Attribute Value'
            )
            ->setComment('Product Attribute Value Table');

        $installer->getConnection()->createTable($table);

        $table = $installer->getConnection()->newTable($installer->getTable('magenest_subscription_recurring_profile'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'profile_id',
                Table::TYPE_TEXT,
                null,
                [],
                'profile ID'
            )
            ->addColumn(
                'state',
                Table::TYPE_TEXT,
                null,
                [],
                'Profile State'
            )
            ->addColumn(
                'order_id',
                Table::TYPE_INTEGER,
                null,
                [],
                'Order ID'
            )
            ->addColumn(
                'customer_id',
                Table::TYPE_INTEGER,
                null,
                [],
                'Customer ID'
            )
            ->addColumn(
                'method_code',
                Table::TYPE_TEXT,
                null,
                [],
                'Method Code'
            )
            ->addColumn(
                'created_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['default' => Table::TIMESTAMP_INIT],
                'Date Created'
            )
            ->addColumn(
                'updated_at',
                Table::TYPE_TIMESTAMP,
                null,
                ['default' => Table::TIMESTAMP_INIT_UPDATE],
                'Date Updated'
            )
            ->addColumn(
                'subscriber_name',
                Table::TYPE_TEXT,
                null,
                [],
                'Subscriber Name'
            )
            ->addColumn(
                'start_datetime',
                Table::TYPE_TEXT,
                null,
                [],
                'Start Datetime'
            )
            ->addColumn(
                'next_billing_date',
                Table::TYPE_TEXT,
                null,
                [],
                'Next Billing Date'
            )
            ->addColumn(
                'cycles_completed',
                Table::TYPE_INTEGER,
                null,
                [],
                'Number of Cycles Completed'
            )
            ->addColumn(
                'cycles_remaining',
                Table::TYPE_INTEGER,
                null,
                [],
                'Number of Cycles Remaining'
            )
            ->addColumn(
                'outstanding_balance',
                Table::TYPE_DECIMAL,
                null,
                [],
                'Outstanding Balance'
            )
            ->addColumn(
                'failed_payments',
                Table::TYPE_INTEGER,
                null,
                [],
                'Failed Payments Count'
            )
            ->addColumn(
                'trial_amt_paid',
                Table::TYPE_DECIMAL,
                null,
                [],
                'Trial Amount Paid'
            )
            ->addColumn(
                'regular_amt_paid',
                Table::TYPE_DECIMAL,
                null,
                [],
                'Trial Amount Paid'
            )
            ->addColumn(
                'product_id',
                Table::TYPE_INTEGER,
                null,
                [],
                'Product ID'
            )
            ->addColumn(
                'product_name',
                Table::TYPE_TEXT,
                null,
                [],
                'Product ID'
            )
            ->addColumn(
                'suspension_threshold',
                Table::TYPE_INTEGER,
                null,
                [],
                'Suspension Threshold'
            )
            ->addColumn(
                'period_unit',
                Table::TYPE_TEXT,
                null,
                [],
                'Period Unit'
            )
            ->addColumn(
                'period_frequency',
                Table::TYPE_INTEGER,
                null,
                [],
                'Period Frequency'
            )
            ->addColumn(
                'period_max_cycles',
                Table::TYPE_INTEGER,
                null,
                [],
                'Period Max Cycles'
            )
            ->addColumn(
                'billing_amount',
                Table::TYPE_DECIMAL,
                null,
                [],
                'Billing Amount'
            )
            ->addColumn(
                'tax_amount',
                Table::TYPE_DECIMAL,
                null,
                [],
                'Tax Amount'
            )
            ->addColumn(
                'shipping_amount',
                Table::TYPE_DECIMAL,
                null,
                [],
                'Shipping Amount'
            )
            ->addColumn(
                'current_info',
                Table::TYPE_TEXT,
                null,
                [],
                'Profile\'s Current Information'
            )
            ->addColumn(
                'trial_info',
                Table::TYPE_TEXT,
                null,
                [],
                'Trial Information'
            )
            ->addColumn(
                'init_info',
                Table::TYPE_TEXT,
                null,
                [],
                'Initial Information'
            )
            ->addColumn(
                'additional_info',
                Table::TYPE_TEXT,
                null,
                [],
                'Additional Information'
            )
            ->setComment('Product Attribute Value Table');

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
