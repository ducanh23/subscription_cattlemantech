<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 09/07/2016
 * Time: 01:47
 */

namespace Magenest\Subscription\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.0') <= 0) {
            $setup->getConnection()->dropColumn(
                $setup->getTable('magenest_subscription_product_attribute'),
                'store_id'
            );
            $setup->getConnection()->dropColumn(
                $setup->getTable('magenest_subscription_product_attribute'),
                'attribute_id'
            );
        }

        if (version_compare($context->getVersion(), '1.0.3') <= 0) {
            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_subscription_recurring_profile'),
                'sequence_order_ids',
                [
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Sequential Order ID'
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.4') <= 0) {
            $setup->getConnection()->dropColumn(
                $setup->getTable('magenest_subscription_recurring_profile'),
                'current_info'
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_subscription_recurring_profile'),
                'aggregate_amount',
                [
                    'type' => Table::TYPE_DECIMAL,
                    'comment' => 'Aggregate Amount',
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_subscription_recurring_profile'),
                'aggregate_optional_amount',
                [
                    'type' => Table::TYPE_DECIMAL,
                    'comment' => 'Aggregate Optional Amount',
                ]
            );

            $setup->getConnection()->addColumn(
                $setup->getTable('magenest_subscription_recurring_profile'),
                'final_payment_duedate',
                [
                    'type' => Table::TYPE_TEXT,
                    'comment' => 'Final Payment Due Date',
                ]
            );
        }

        if (version_compare($context->getVersion(), '1.0.6') <= 0) {
            $setup->getConnection()
                ->modifyColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'order_id',
                    [
                        'type' => Table::TYPE_TEXT,
                        'comment' => 'Original Order Id'
                    ]
                )
                ->modifyColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'outstanding_balance',
                    [
                        'type' => Table::TYPE_DECIMAL,
                        'precision' => 10,
                        'scale' => 2,
                        'comment' => 'Outstanding Balance'
                    ]
                )
                ->modifyColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'trial_amt_paid',
                    [
                        'type' => Table::TYPE_DECIMAL,
                        'precision' => 10,
                        'scale' => 2,
                        'comment' => 'Trial Amount Paid'
                    ]
                )
                ->modifyColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'regular_amt_paid',
                    [
                        'type' => Table::TYPE_DECIMAL,
                        'precision' => 10,
                        'scale' => 2,
                        'comment' => 'Regular Amount Paid'
                    ]
                )
                ->modifyColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'billing_amount',
                    [
                        'type' => Table::TYPE_DECIMAL,
                        'precision' => 10,
                        'scale' => 2,
                        'comment' => 'Billing Amount'
                    ]
                )
                ->modifyColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'tax_amount',
                    [
                        'type' => Table::TYPE_DECIMAL,
                        'precision' => 10,
                        'scale' => 2,
                        'comment' => 'Tax Amount'
                    ]
                )
                ->modifyColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'shipping_amount',
                    [
                        'type' => Table::TYPE_DECIMAL,
                        'precision' => 10,
                        'scale' => 2,
                        'comment' => 'Shipping Amount'
                    ]
                )
                ->modifyColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'aggregate_amount',
                    [
                        'type' => Table::TYPE_DECIMAL,
                        'precision' => 10,
                        'scale' => 2,
                        'comment' => 'Aggregate Amount'
                    ]
                )
                ->modifyColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'aggregate_optional_amount',
                    [
                        'type' => Table::TYPE_DECIMAL,
                        'precision' => 10,
                        'scale' => 2,
                        'comment' => 'Aggredate Optional Amount'
                    ]
                );
        }

        if (version_compare($context->getVersion(), '1.0.8') <= 0) {
            $table = $setup->getConnection()
                ->newTable($setup->getTable('magenest_subscription_option_attribute'))
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'product_id',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true],
                    'Product ID'
                )
                ->addColumn(
                    'option_name',
                    Table::TYPE_TEXT,
                    null,
                    [],
                    'Option Name'
                )
                ->addColumn(
                    'value',
                    Table::TYPE_TEXT,
                    null,
                    [],
                    'Attribute Value'
                )
                ->setComment('Option Attribute Value Table');

            $setup->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '1.0.9') <= 0) {
            $table = $setup->getConnection()->newTable(
                $setup->getTable('magenest_subscription_order_relationship')
            )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )->addColumn(
                    'parent_order',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Parent order increment ID'
                )->addColumn(
                    'child_order',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Child order increment ID'
                )->addColumn(
                    'profile_id',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Profile ID'
                );
            $setup->getConnection()->createTable($table);
        }

        if (version_compare($context->getVersion(), '2.0.0') <= 0) {
            $table = $setup->getConnection()->newTable(
                $setup->getTable('magenest_subscription_box')
            )
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )->addColumn(
                    'option_name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Subscription Option name for example 2 months'
                )->addColumn(
                    'selected_unit',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Unit of recurring payment for example day or month'
                )->addColumn(
                    'frequency',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Frequency'
                )->addColumn(
                    'maximum_payment_failures',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Maximum payment failures'
                )->addColumn(
                    'start_date',
                    Table::TYPE_DATETIME,
                    null,
                    ['nullable' => true, 'default' => null],
                    'Start Date of the recurring profile'
                )->addColumn(
                    'subscriber_name',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true],
                    'Subscriber name'
                )
                ->addColumn(
                    'qty',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true],
                    'Quantity of subscription box'
                )->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    '64k',
                    ['nullable' => true],
                    'Description'
                )
                ->addColumn(
                    'max_cycles',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Max cycles'
                )->addColumn(
                    'trial_enabled',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Trial Enable whether yes or no'
                )->addColumn(
                    'trial_amount',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Trial Amount'
                )->addColumn(
                    'trial_unit',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Trial Amount example day or month'
                )->addColumn(
                    'trial_frequency',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Trial frequency'
                )->addColumn(
                    'trial_max_cycles',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Trial max cycles'
                )->addColumn(
                    'init_enabled',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Whether initial fee is enable'
                )->addColumn(
                    'init_fee',
                    Table::TYPE_DECIMAL,
                    null,
                    ['precision' => 10,
                            'scale' => 2
                        ],
                    "Init fee"
                )->addColumn(
                    'quote_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true
                        ],
                    "Quote Id"
                )
                ->addColumn(
                    'shipping_amt',
                    Table::TYPE_DECIMAL,
                    null,
                    ['precision' => 10,
                        'scale' => 2
                    ],
                    "Shipping Amount"
                )->addColumn(
                    'tax_amt',
                    Table::TYPE_DECIMAL,
                    null,
                    ['precision' => 10,
                        'scale' => 2
                    ],
                    "Tax Amount"
                )->addColumn(
                    'first_time_discount',
                    Table::TYPE_DECIMAL,
                    null,
                    ['precision' => 10,
                        'scale' => 2
                    ],
                    "First time discount Amount"
                )->addColumn(
                    'sub_total',
                    Table::TYPE_DECIMAL,
                    null,
                    ['precision' => 10,
                        'scale' => 2
                    ],
                    "Sub total"
                )->addColumn(
                    'grand_total',
                    Table::TYPE_DECIMAL,
                    null,
                    ['precision' => 10,
                        'scale' => 2
                    ],
                    "Grand total"
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'name'
                ) ->addColumn(
                    'product_id',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'product ids'
                ) ->addColumn(
                    'quote_item_id',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Associated quote item id'
                ) ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                )->addColumn(
                    'updated_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                )->addColumn(
                    'is_active',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '1'],
                    'Defines Is Active'
                )->addColumn(
                    'mode',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '1'],
                    'in case value is 1 then the subscription box contains 1 product only,2 is used for a build a box subscription'
                )->addColumn(
                    'auto_bill',
                    \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '1'],
                    '1 is auto bill,  0 used when system does not allow auto bill'
                )->addColumn(
                    'currency_code',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    null,
                    [ 'nullable' => false],
                    '1 is auto bill,  0 used when system does not allow auto bill'
                )
                ->addColumn(
                    'store_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false, 'default' => '1'],
                    'store id'
                )
            ;
            $setup->getConnection()->createTable($table);

            /** Add more columns to profile table */
            if (version_compare($context->getVersion(), '2.0.0') <= 0) {

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'ship_to_name',
                    [
                        'type' => Table::TYPE_TEXT,
                        'comment' => 'Shipping to name',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'ship_to_street',
                    [
                        'type' => Table::TYPE_TEXT,
                        'comment' => 'street shipping address',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'ship_to_city',
                    [
                        'type' => Table::TYPE_TEXT,
                        'comment' => 'Ship To City',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'ship_to_zip',
                    [
                        'type' => Table::TYPE_TEXT,
                        'comment' => 'Ship To Zip',
                    ]
                );
                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'ship_to_state',
                    [
                        'type' => Table::TYPE_TEXT,
                        'comment' => 'Ship To State',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'ship_to_country',
                    [
                        'type' => Table::TYPE_TEXT,
                        'comment' => 'Ship To Zip',
                    ]
                );

                /** Add trial information */
                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'trial_billing_period',
                    [
                        'type' => Table::TYPE_TEXT,
                        'comment' => 'Trial Billing Period',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'trial_billing_frequency',
                    [
                        'type' => Table::TYPE_INTEGER,
                        'comment' => 'Trial Billing frequency',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'trial_billing_cycles',
                    [
                        'type' => Table::TYPE_INTEGER,
                        'comment' => 'Trial Billing cycles',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'trial_amt',
                    [
                        'type' => Table::TYPE_DECIMAL,

                        null,
                        ['precision' => 10,
                            'scale' => 2
                        ],
                        'comment' => 'Trial Amt',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'trial_tax_amt',
                    [
                        'type' => Table::TYPE_DECIMAL,

                        null,
                        ['precision' => 10,
                            'scale' => 2
                        ],
                        'comment' => 'Trial Tax Amt',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'init_amt',
                    [
                        'type' => Table::TYPE_DECIMAL,

                        null,
                        ['precision' => 10,
                            'scale' => 2
                        ],
                        'comment' => 'Trial Tax Amt',
                    ]
                );

                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'trial_shipping_amt',
                    [
                        'type' => Table::TYPE_DECIMAL,

                        null,
                        ['precision' => 10,
                            'scale' => 2
                        ],
                        'comment' => 'Trial Shipping Amt',
                    ]
                );
                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'quote_item_id',
                    [
                        'type' => Table::TYPE_TEXT,

                        [ 'size' => '64k',
                          'null' => false
                        ],
                        'comment' => 'quote item id array which is serialized',
                    ]
                );

                //quote item id
                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'item_info',
                    [
                        'type' => Table::TYPE_TEXT,
                        null,
                        ['size' => Table::MAX_TEXT_SIZE,
                          'null' => false
                        ],
                        'comment' => 'Information about products in profiles Amt',
                    ]
                );

                // box id
                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'box_id',
                    [
                        'type' => Table::TYPE_INTEGER,
                        null,
                        [
                            'null' => true
                        ],
                        'comment' => 'Box ID',
                    ]
                );

                //add currency_code column
                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'currency_code',
                    [
                        'type' => Table::TYPE_TEXT,
                        null,
                        ['size' => Table::DEFAULT_TEXT_SIZE,
                            'null' => false
                        ],
                        'comment' => 'Currency Code',
                    ]
                );
                //add store id
                $setup->getConnection()->addColumn(
                    $setup->getTable('magenest_subscription_recurring_profile'),
                    'store_id',
                    [
                        'type' => Table::TYPE_INTEGER,
                        null,
                        [
                            'null' => true
                        ],
                        'comment' => 'Store ID',
                    ]
                );

            }
        }

        $setup->endSetup();
    }
}
