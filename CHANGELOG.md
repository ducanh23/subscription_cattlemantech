# Change Log
All notable changes to this extension will be documented in this file.
This extension adheres to [Magenest](http://magenest.com/).

## [100.2.0] - 2017-10-26
### Added
- Implement logger
- Stability fixes
- Major code structure refactor
- Compatible with Magento 2.2 CE, EE and B2B

## [100.0.7] - 2017-10-27
### Added
- Authorizenet webhooks
- Allow cart item's subscription options edit

## [100.0.6] - 2017-03-15
### Added
- Stability fixes
- Implement email sending

## [100.0.5] - 2016-10-27
### Added
- New order will be created as new billing cycle starts.
- Refactor databases.
- Minor logic fixes.

## [100.0.4] - 2016-10-27
### Added
- Ability to enable recurring feature with configurable products.
- Minor fixes

## [100.0.3] - 2016-07-28
### Added
- Added Authorize.net subscription feature.

## [100.0.2] - 2016-07-13
### Added
- Magento 2.1 compatibility.
- Integrated trial period and initial fee into each billing options.

## [100.0.1] - 2016-04-03
### Added
- Admins can manage recurring profiles with grid view and detail view.
- Users can also view their recurring profiles. They can suspend, cancel and reactivate their profiles.
- A cron job is set to be activated every minute. This cron job will get the latest profile status and information.

## [100.0.0] - 2016-03-31
### Added
- Enable a product of type simple, virtual or downloadable to become a subscription product
- Admin can have full control of a product's recurring options, including:
+ Enable recurring feature
+ Customer can define start date or not
+ Number of maximum payment failures
+ Auto bill on next cycle
+ Multiple billing options, each has period unit (Day, Week, Month, SemiMonth or Year), billing frequency and max billing cycles
+ Enable trial and its options (amount, period unit, billing frequency and billing cycles)
+ Enable initial fee and its amount
- When a customer purchase a product that has recurring feature enabled, regular check/order checkout will also create a recurring profile.
- Customers can checkout with Paypal easily, they can also manage their recurring profiles in My Recurring Profiles in My Account
- Admin can manage all recurring profiles in Magenest Subscription -> Recurring Profiles