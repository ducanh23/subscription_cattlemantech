<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 16/09/2017
 * Time: 13:55
 */

namespace Magenest\Subscription\Controller\Subscribe;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\DataObject;
use Magenest\Subscription\Api\SubscriptionManagementInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

class SignUp extends Action
{
    /**
     * @var SubscriptionManagementInterface
     */
    protected $subscriptionManagement;

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magenest\Subscription\Logger\Logger
     */
    protected $logger;

    /**
     * SignUp constructor.
     * @param Context $context
     * @param CheckoutSession $checkoutSession
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magenest\Subscription\Logger\Logger $logger
     * @param SubscriptionManagementInterface $subscriptionManagement
     */
    public function __construct(
        Context $context,
        CheckoutSession $checkoutSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magenest\Subscription\Logger\Logger $logger,
        SubscriptionManagementInterface $subscriptionManagement
    ) {
    
        $this->checkoutSession = $checkoutSession;
        $this->subscriptionManagement = $subscriptionManagement;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();
        $result = [];
        $paymentParams = $this->getRequest()->getParam('result');

        $createdProfiles = [];
        $order = [];
        try {
            $ccNumber =(isset($paymentParams['ccNumber'])) ?$paymentParams['ccNumber'] :'';
            $expMonth =  (isset($paymentParams['expMonth'])) ?$paymentParams['expMonth'] :'';
            $expYear = (isset($paymentParams['expYear'])) ? $paymentParams['expYear'] :'';
            $ccId = (isset($paymentParams['ccId'])) ? $paymentParams['ccId'] :'';
            $paymentMethod = (isset($paymentParams['payment_method'])) ?$paymentParams['payment_method'] :'';

            $payment = new DataObject();

            $payment->addData(
                [
                    'ccNumber' => $ccNumber,
                    'expMonth' => $expMonth,
                    'expYear'  => $expYear,
                    'ccId'     => $ccId,
                    'payment_method' => $paymentMethod
                ]
            );

            //sign up the payment
            $quote = $this->checkoutSession->getQuote();
            $this->subscriptionManagement->process($quote, $payment);
            $result['code'] = 'success';

        } catch (\Exception $e) {
            $this->logger->addError($e->getMessage());
            $result['code'] = 'error';
        }

        $resultJson->setData($result);
        return $resultJson;
    }
}
