<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 28/03/2016
 * Time: 14:14
 */
namespace Magenest\Subscription\Controller\Customer;

use Magenest\Subscription\Logger\Logger;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Registry;

class View extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * View constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param LoggerInterface $loggerInterface
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Logger $logger,
        Registry $registry
    ) {
        $this->_resultPageFactory = $pageFactory;
        $this->_logger = $logger;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * process the request
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        $this->_coreRegistry->register('customer_view_profile_id', $id);
        $profile_id = $this->_objectManager->get('\Magenest\Subscription\Model\Profile')->load($id)->getProfileId();

        $this->_view->loadLayout();
        if ($block = $this->_view->getLayout()->getBlock('customer_subscription_account_view')) {
            $block->setRefererUrl($this->_redirect->getRefererUrl());
        }
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Recurring Profile ') . $profile_id);
        $this->_view->renderLayout();
    }
}
