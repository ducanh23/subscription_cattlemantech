<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 31/03/2016
 * Time: 15:54
 */

namespace Magenest\Subscription\Controller\Customer;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\Subscription\Logger\Logger;
use Magenest\Subscription\Helper\Data;
use Magenest\Subscription\Model\ProfileFactory;
use Magenest\Subscription\Api\SubscriptionManagementInterface;

class Manage extends Action
{
    /**
     * @var string
     */
    protected $_api;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var Data
     */
    protected $_helper;

    /**
     * @var SubscriptionManagementInterface
     */
    protected $_subscriptionManagement;

    /**
     * @var ProfileFactory
     */
    protected $_profileFactory;

    /**
     * Manage constructor.
     * @param Context $context
     * @param SubscriptionManagementInterface $subscriptionManagement
     * @param LoggerInterface $loggerInterface
     * @param ProfileFactory $profileFactory
     * @param PageFactory $pageFactory
     * @param Data $data
     */
    public function __construct(
        Context $context,
        SubscriptionManagementInterface $subscriptionManagement,
        Logger $logger,
        ProfileFactory $profileFactory,
        PageFactory $pageFactory,
        Data $data
    ) {
    
        $this->_subscriptionManagement = $subscriptionManagement;
        $this->_logger = $logger;
        $this->_resultPageFactory = $pageFactory;
        $this->_helper = $data;
        $this->_profileFactory = $profileFactory;
        parent::__construct($context);
    }

    /**
     * process the request
     * @return $this
     */
    public function execute()
    {
        $profile_id = $this->getRequest()->getParam('profile_id');
        $action = $this->getRequest()->getParam('action');
        $method = $this->getRequest()->getParam('method');

        try {
            $paymentMethodInstance = $this->_subscriptionManagement->getPaymentMethodInstance($method);
            $response = $paymentMethodInstance->invokeUserAction($profile_id, $action);

            if ($response) {
                $profile = $this->_profileFactory->create()
                    ->getCollection()
                    ->addFieldToFilter('profile_id', $profile_id)
                    ->getFirstItem();
                //update the profile data on Magento side
                if ($action == 'cancel') {
                    $profile->setData('state', \Magenest\Subscription\Model\Profile::STATE_CANCELLED)->save();
                    $this->_eventManager->dispatch('magenest_subscription_cancel_subscription', ['profile_id' => $profile_id, 'profile' => $profile]);
                } elseif ($action == 'suspend') {
                    $profile->setData('state', \Magenest\Subscription\Model\Profile::STATE_SUSPENDED)->save();
                    $this->_eventManager->dispatch('magenest_subscription_suspend_subscription', ['profile_id' => $profile_id, 'profile' => $profile]);
                } elseif ($action == 'reactivate') {
                    $profile->setData('state', \Magenest\Subscription\Model\Profile::STATE_ACTIVE)->save();
                    $this->_eventManager->dispatch('magenest_subscription_reactivate_subscription', ['profile_id' => $profile_id,'profile' => $profile]);
                }
                $this->messageManager->addSuccessMessage(
                    __('The Profile\'s status has been changed.')
                );
            } else {
                $this->messageManager->addErrorMessage(__('We can\'t change the status of the profile yet'));
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Unable to cancel profile'));
        }


        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('subscription/customer/profiles');
    }
}
