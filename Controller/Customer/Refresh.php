<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 31/03/2016
 * Time: 15:33
 */
namespace Magenest\Subscription\Controller\Customer;

use Magento\Framework\App\Action\Action;
use Magenest\Subscription\Model\Cron;
use Magento\Framework\App\Action\Context;

class Refresh extends Action
{
    /**
     * @var Cron
     */
    protected $_cron;

    /**
     * Refresh constructor.
     * @param Cron $cron
     * @param Context $context
     */
    public function __construct(
        Cron $cron,
        Context $context
    ) {
        $this->_cron = $cron;
        parent::__construct($context);
    }

    /**
     * process the request
     */
    public function execute()
    {
        $this->_cron->syncEveryTwoMins();
        $this->_forward('profiles');
    }
}
