<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Controller\Webhooks;

use Magento\Framework\App\Action\Context;

class Authorizenet extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $_httpRequest;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $cart;

    /**
     * @var \Magenest\Subscription\Helper\Authorize\TransactionReporting
     */
    protected $_transReport;

    /**
     * @var string
     */
    protected $_merchantId;

    /**
     * @var string
     */
    protected $_merchantTransactionKey;

    /**
     * @var bool
     */
    protected $_isLive;

    /**
     * @var \Magenest\Subscription\Model\ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var \Magenest\Subscription\Logger\Logger
     */
    protected $_logger;

    /**
     * Authorizenet constructor.
     * @param Context $context
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Magento\Checkout\Model\Cart $cart
     */
    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Framework\App\Request\Http $request,
        \Magenest\Subscription\Model\ProfileFactory $profileFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magenest\Subscription\Helper\Authorize\TransactionReporting $transReport,
        \Magenest\Subscription\Logger\Logger $logger
    ) {
    
        parent::__construct($context);
        $this->cart = $cart;
        $this->_httpRequest = $request;
        $this->_transReport = $transReport;
        $this->_profileFactory = $profileFactory;
        $this->_logger = $logger;
        $this->_isLive = !$scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_IS_TEST);
        $this->_merchantId = $scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_MERCHANT_ID);
        $this->_merchantTransactionKey = $scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_TRANSACTION_KEY);
    }

    /**
     * Execute function
     */
    public function execute()
    {
        $content = $this->_httpRequest->getContent();
        $content = json_decode($content);
        $eventType = is_null($content) ? null : $content->eventType;
        $this->_objectManager->create('Magenest\Subscription\Logger\Logger')
            ->addDebug("webhook content\n" . print_r($content, true));

        if ($eventType == "net.authorize.payment.authcapture.created" || $eventType == "net.authorize.payment.authorization.created") {
            $transactionId = $content->payload->id;
            $merchantAuth = $this->_transReport->getMerchantAuthentication($this->_merchantId, $this->_merchantTransactionKey);
            $transaction = $this->_transReport->getTransactionDetailsRequest($transactionId);
            $response = $this->_transReport->executeRequest($transaction, $this->_isLive);
            $data = $this->_transReport->responseParser($response);
            if (!empty($data['error_code']) || $data['status'] != 'settledSuccessfully') {
                $this->_logger->addDebug('Authorizenet response is not valid at \Magenest\Subscription\Controller\Webhooks\Authorizenet');
                return;
            }
            $this->_objectManager->create('Magenest\Subscription\Logger\Logger')->addDebug("webhook data\n" . print_r($data, true));
            if (!empty($data['subs_id'])) {
                $payNum = $data['paynum'];
                /** @var \Magenest\Subscription\Model\Profile $profile */
                $profile = $this->_profileFactory->create();
                $profile = $profile->loadByProfileId($data['subs_id']);

                // generate order
                $order = $profile->generateOrderBaseOnBox();
                // calculate next billing date of the profile
                $nextBillingDate = $profile->calculateNextBillingDate();
                $nextBillingDateObj = \DateTime::createFromFormat('Y-m-d H:i:s', $nextBillingDate);
                $savedNextBilling = $nextBillingDateObj->format('Y-m-d');

                $cyclesCompleted = intval($profile->getData('cycles_completed'));
                $cyclesCompleted++;

                $cyclesRemaining = intval($profile->getData('cycles_remaining'));
                $cyclesRemaining--;

                $profile->setData([
                    'next_billing_date' => date('Y-m-d', $savedNextBilling),
                    'cycles_completed' => $cyclesCompleted,
                    'cycles_remaining' => $cyclesRemaining
                ])->save();

                // save order and profile
                $order->save();
                $profile->addSequenceOrder($order->getId());
            }
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @param $transactionId
     */
    public function sendInvoice($order, $transactionId)
    {
        $payment = $order->getPayment();
        $payment->setTransactionId($transactionId)->setIsTransactionClosed(false);
        $payment->setCcTransId($transactionId);
        $payment->setLastTransId($transactionId);
        $payment->registerCaptureNotification($order->getGrandTotal());
        $payment->getCreatedInvoice();

        $order->save();
    }
}
