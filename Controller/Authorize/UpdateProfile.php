<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 12/04/2017
 * Time: 14:47
 */

namespace Magenest\Subscription\Controller\Authorize;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Session as CheckoutSession;

class UpdateProfile extends Action
{
    /**
     * @var \Magenest\Subscription\Model\ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var \Magenest\Subscription\Logger\Logger
     */
    protected $_logger;

    /**
     * UpdateProfile constructor.
     * @param Context $context
     * @param \Magenest\Subscription\Logger\Logger $logger
     * @param \Magenest\Subscription\Model\ProfileFactory $profileFactory
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Context $context,
        \Magenest\Subscription\Logger\Logger $logger,
        \Magenest\Subscription\Model\ProfileFactory $profileFactory,
        CheckoutSession $checkoutSession
    ) {
    
        $this->_logger = $logger;
        $this->_profileFactory = $profileFactory;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * Execute
     */
    public function execute()
    {
        $this->_logger->debug("Controller\...\UpdateProfile ran");
        $subscriptionId = $this->getRequest()->getParam('subscription_id');
        $profile = $this->_profileFactory->create()->getCollection()->addFieldToFilter('profile_id', $subscriptionId)->getFirstItem();
        $order = $this->_checkoutSession->getLastRealOrder();
        $orderId = $order->getId();

        if ($profile) {
            $profile->setData('order_id', $orderId)->save();
        }
    }
}
