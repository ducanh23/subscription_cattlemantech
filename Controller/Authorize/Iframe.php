<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 31/07/2016
 * Time: 21:19
 */
namespace Magenest\Subscription\Controller\Authorize;

use Magento\Framework\Controller\ResultFactory;
use Magenest\Subscription\Logger\Logger;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Context;

class Iframe extends \Magento\Framework\App\Action\Action
{
    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * Iframe constructor.
     * @param Context $context
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Context $context,
        CheckoutSession $checkoutSession
    ) {
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $quote = $this->_checkoutSession->getQuote();
        $item = $quote->getItemsCollection()->getFirstItem();

        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        $buyInfo = $item->getBuyRequest();
        $options = $buyInfo->getAdditionalOptions();

        $result = 0;

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                if ($value && $key == 'Main Period') {
                    $result = 1;
                }
            }
        }

        $resultJson->setData($result);
        return $resultJson;
    }
}
