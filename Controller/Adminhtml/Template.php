<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 15/04/2016
 * Time: 09:25
 */
namespace Magenest\Subscription\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Subscription\Model\TemplateFactory;
use Magento\Framework\Registry;
use Magenest\Subscription\Logger\Logger;

abstract class Template extends Action
{
    /**
     * @var TemplateFactory
     */
    protected $_templateFactory;

    /**
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Template constructor.
     * @param Action\Context $context
     * @param PageFactory $pageFactory
     * @param TemplateFactory $templateFactory
     * @param Registry $coreRegistry
     * @param Logger $logger
     */
    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        TemplateFactory $templateFactory,
        Registry $coreRegistry,
        Logger $logger
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_templateFactory = $templateFactory;
        $this->_logger = $logger;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_pageFactory->create();
        $resultPage->setActiveMenu('Magenest_Subscription::template');

        $resultPage->getConfig()->getTitle()->set(__('Subscription Templates'));
        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Subscription::template');
    }
}
