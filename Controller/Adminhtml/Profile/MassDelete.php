<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 30/03/2016
 * Time: 02:13
 */
namespace Magenest\Subscription\Controller\Adminhtml\Profile;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magenest\Subscription\Model\ProfileFactory;
use Magenest\Subscription\Logger\Logger;

class MassDelete extends Action
{
    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * MassDelete constructor.
     * @param Filter $filter
     * @param ProfileFactory $profileFactory
     * @param Action\Context $context
     * @param Logger $logger
     */
    public function __construct(
        Filter $filter,
        ProfileFactory $profileFactory,
        Action\Context $context,
        Logger $logger
    ) {
        $this->_filter = $filter;
        $this->_profileFactory = $profileFactory;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_profileFactory->create()->getCollection());
        $profileDeleted = 0;
        foreach ($collection->getItems() as $profile) {
            $profile->delete();
            $profileDeleted++;
        }
        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been deleted.', $profileDeleted)
        );

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('subscription/*/index');
    }
}
