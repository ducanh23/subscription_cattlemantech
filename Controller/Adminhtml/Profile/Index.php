<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 25/03/2016
 * Time: 19:07
 */
namespace Magenest\Subscription\Controller\Adminhtml\Profile;

use Magento\Backend\App\Action;
use Magenest\Subscription\Controller\Adminhtml\Profile;

class Index extends Profile
{
    /**
     * index method
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()->prepend(__('Recurring Profiles Manager'));

        return $resultPage;
    }

    /**
     * @return bool
     */
    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Subscription::profile');
    }
}
