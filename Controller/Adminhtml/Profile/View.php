<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 25/03/2016
 * Time: 23:18
 */
namespace Magenest\Subscription\Controller\Adminhtml\Profile;

use Magenest\Subscription\Controller\Adminhtml\Profile;

class View extends Profile
{
    /**
     * @return $this|\Magento\Backend\Model\View\Result\Page
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        /** @var \Magenest\Subscription\Model\Profile $model */
        $model = $this->_profileFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This profile no longer exists.'));

                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            } else {
                $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
                if (!empty($data)) {
                    $model->setData($data);
                }

                $this->_coreRegistry->register('subscription_profile_model', $model);
                /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
                $resultPage = $this->_initAction();
                $title = __('View Profile ') . $model->getProfileId();
                $resultPage->getConfig()->getTitle()
                    ->prepend($title);

                return $resultPage;
            }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('This profile no longer exists.')
            );
        }
    }
}
