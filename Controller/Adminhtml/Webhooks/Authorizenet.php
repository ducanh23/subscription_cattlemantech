<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/12/17
 * Time: 11:17 AM
 */

namespace Magenest\Subscription\Controller\Adminhtml\Webhooks;

use Magento\Backend\App\Action;
use Magento\Framework\HTTP\Client\Curl;

class Authorizenet extends \Magento\Backend\App\Action
{
    /**
     * @var Curl
     */
    protected $curl;

    /**
     * Authorizenet constructor.
     * @param Action\Context $context
     * @param \Magenest\Subscription\Helper\Authorize\Webhook $helper
     */
    public function __construct(
        Action\Context $context,
        Curl $curl,
        \Magenest\Subscription\Helper\Authorize\Webhook $helper
    ) {
    
        parent::__construct($context);
        $this->_helper = $helper;
        $this->curl = $curl;
    }

    /**
     * Execute
     * @return \Magento\Framework\Controller\Result\Raw
     */
    public function execute()
    {
        $endpoint = $this->_helper->getPreEndpoint() . 'rest/v1/webhooks';
        $token = $this->_helper->getAuthorizationHeader();
        $webhookUrl = $this->_helper->getWebhookUrl();
        $data = '{
        "name": "Magento Webhook",
        "url": "' . $webhookUrl . '",
        "eventTypes":
        [
        "net.authorize.payment.authcapture.created",
        "net.authorize.payment.authorization.created",
        "net.authorize.payment.capture.created"
        ],
        "status": "active"
        }';

        $httpHeaders = new \Zend\Http\Headers();
        $httpHeaders->addHeaders(array(
            'cache-control' => 'no-cache',
            'authorization' => $token,
            'Content-type' => 'application/json'
        ));

        $request = new \Zend\Http\Request();
        $request->setUri($endpoint);
        $request->setMethod(\Zend\Http\Request::METHOD_POST);
        $request->setHeaders($httpHeaders);

        $client = new \Zend\Http\Client();
        $client->setRequest($request);
        $client->setRawBody($data);

        $response = $client->send();
        $phrase = $response->getReasonPhrase();

        /** @var \Magento\Framework\Controller\Result\Raw $result */
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
        $result->setContents($phrase);
        return $result;
    }
}
