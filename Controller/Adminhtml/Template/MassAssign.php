<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 14/10/2017
 * Time: 21:05
 */
namespace Magenest\Subscription\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;

class MassAssign extends \Magento\Backend\App\Action
{
    /**
     * @var \Magenest\Subscription\Helper\Util
     */
    public $util;

    /**
     * MassAssign constructor.
     * @param Action\Context $context
     * @param \Magenest\Subscription\Helper\Util $util
     */
    public function __construct(
        Action\Context $context,
        \Magenest\Subscription\Helper\Util $util
    ) {
        $this->util = $util;
        parent::__construct($context);
    }

    /**
     * Controller execute
     */
    public function execute()
    {
        $this->_session->setAssignArray(serialize([]));
        $params = $this->getRequest()->getParams();

        $selected = $this->getRequest()->getParam('selected');
        $excluded = $this->getRequest()->getParam('excluded');
        $excludeMode = $this->getRequest()->getParam('excludeMode');

        $resultArray = $this->util->calculateAssignProducts($selected, $excluded, $excludeMode);

        $this->_session->setAssignArray(serialize($resultArray));
    }
}
