<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 17/04/2016
 * Time: 21:17
 */
namespace Magenest\Subscription\Controller\Adminhtml\Template;

use Magento\Framework\Exception\LocalizedException;
use Magento\Backend\App\Action;
use Magenest\Subscription\Logger\Logger;

class Delete extends Action
{
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param Logger $logger
     */
    public function __construct(
        Action\Context $context,
        Logger $logger
    ) {
        $this->_logger = $logger;
        parent::__construct($context);
    }

    /**
     * @throws LocalizedException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            $model = $this->_objectManager->create('Magenest\Subscription\Model\Template');

            $model->load($id);
            if ($id != $model->getId()) {
                throw new LocalizedException(__('Something is wrong while deleting the template. Please try again.'));
            }

            $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($model->getData());

            try {
                $model->delete();
                $this->messageManager->addSuccess(__('The template has been successfully deleted.'));
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/index');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->_objectManager->get('Magenest\Subscription\Logger\Logger')->critical($e);
                $this->_objectManager->get('Magento\Backend\Model\Session')->setPageData($model->getData());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        return $resultRedirect->setPath('*/*/index');
    }
}
