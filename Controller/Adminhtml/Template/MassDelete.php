<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 30/03/2016
 * Time: 02:13
 */
namespace Magenest\Subscription\Controller\Adminhtml\Template;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magenest\Subscription\Model\TemplateFactory;
use Magenest\Subscription\Logger\Logger;

class MassDelete extends Action
{
    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var TemplateFactory
     */
    protected $_templateFactory;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * MassDelete constructor.
     * @param Filter $filter
     * @param TemplateFactory $templateFactory
     * @param Action\Context $context
     * @param Logger $logger
     */
    public function __construct(
        Filter $filter,
        TemplateFactory $templateFactory,
        Action\Context $context,
        Logger $logger
    ) {
        $this->_filter = $filter;
        $this->_templateFactory = $templateFactory;
        $this->_logger = $logger;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $templates = $this->getRequest()->getParam('Template');
        $collection = $this->_templateFactory->create()->getCollection();
        if ($templates) {
            if (is_array($templates)) {
                $collection->addFieldToFilter('id', ['in' => $templates]);
            } else {
                $collection->addFieldToFilter('id', $templates);
            }
        }

        $templateDeleted = 0;
        foreach ($collection->getItems() as $template) {
            $template->delete();
            $templateDeleted++;
        }
        $this->messageManager->addSuccess(
            __('A total of %1 record(s) have been deleted.', $templateDeleted)
        );

        return $this->resultFactory->create(ResultFactory::TYPE_REDIRECT)->setPath('subscription/*/index');
    }
}
