<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 15/04/2016
 * Time: 09:47
 */
namespace Magenest\Subscription\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magenest\Subscription\Model\TemplateFactory;
use Magenest\Subscription\Controller\Adminhtml\Template;

class Edit extends Template
{
    /**
     * @return $this|\Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        /** @var \Magenest\Membership\Model\Rule $model */
        $model = $this->_templateFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This template no longer exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);

        if (!empty($data)) {
            $model->setData($data);
        }

        $this->_coreRegistry->register('subscription_template_edit', $model);
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? __('Edit Option \'%1\'', $model->getData('option_name')) : __('New Option'));

        return $resultPage;
    }
}
