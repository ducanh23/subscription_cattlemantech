<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $cacheTypeList;

    /**
     * @var \Magento\Backend\Helper\Js
     */
    protected $jsHelper;

    /**
     * @param Action\Context $context
     * @param \Magento\Backend\Helper\Js $jsHelper
     */

    protected $templateFactory;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Backend\Helper\Js $jsHelper,
        \Magenest\Subscription\Model\TemplateFactory $templateFactory
    ) {
    
        $this->cacheTypeList = $cacheTypeList;
        parent::__construct($context);
        $this->jsHelper = $jsHelper;
        $this->templateFactory = $templateFactory;
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Subscription::template');
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /* @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */

        if ($data) {
            $optionInfo = [
                'optionName' => $data['template_name'],
                'selectedUnit' => $data['period_unit'],
                'frequency' => $data['billing_frequency'],
                'maxCycles' => $data['billing_max_cycles'],
                'trialAmount' => $data['trial_amount'],
                'trialUnit' => $data['trial_unit'],
                'trialFrequency' => $data['trial_billing_frequency'],
                'trialMaxCycles' => $data['trial_billing_max_cycle'],
                'initFee' => $data['initial_fee']
            ];

            //Check if there is trial option
            if ($data['trial_amount'] > 0) {
                $optionInfo['trialEnabled'] = 'yes';
            } else {
                $optionInfo['trialEnabled'] = 'no';
                $optionInfo['trialUnit'] = '';
            }

            //Check if there is initial fee
            if ($data['initial_fee'] > 0) {
                $optionInfo['initEnabled'] = 'yes';
            } else {
                $optionInfo['initEnabled'] = 'no';
            }
        }

        // Check the session
        $assignArray = $this->_session->getAssignArray();
        $allProductIds = [];
        if ($assignArray) {
            $assignArray = unserialize($assignArray);
            $allProductIds = array_keys($assignArray);
        }
        $this->_session->setAssignArray(serialize([]));

        // Start saving template
        $resultRedirect = $this->resultRedirectFactory->create();
        $model = $this->templateFactory->create();

        $id = $this->getRequest()->getParam('id');
        if ($id) {
            $model->load($id);
        }

        $value = serialize($optionInfo);

        $saveData = [
            'option_name' => $data['template_name'],
            'value' => $value,
            'product_id' => serialize($allProductIds)
        ];
        $model->addData($saveData);

        try {
            $model->getResource()->save($model);
            $this->cacheTypeList->invalidate('full_page');
            $this->messageManager->addSuccess(__('Template Saved.'));
            $this->_objectManager->get('Magento\Backend\Model\Session')->setFormData(false);
            if ($this->getRequest()->getParam('back')) {
                return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId(), '_current' => true]);
            }
            return $resultRedirect->setPath('*/*/');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\RuntimeException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving the template.'));
        }

        $this->_getSession()->setFormData($data);
        return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
    }
}
