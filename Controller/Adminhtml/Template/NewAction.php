<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 15/04/2016
 * Time: 09:44
 */
namespace Magenest\Subscription\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\ForwardFactory;

class NewAction extends Action
{
    /**
     * @var ForwardFactory
     */
    protected $_resultForwardFactory;

    /**
     * NewAction constructor.
     * @param ForwardFactory $forwardFactory
     * @param Action\Context $context
     */
    public function __construct(
        ForwardFactory $forwardFactory,
        Action\Context $context
    ) {
        $this->_resultForwardFactory = $forwardFactory;
        parent::__construct($context);
    }

    /**
     * @return $this
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForward */
        $resultForward = $this->_resultForwardFactory->create();
        return $resultForward->forward('edit');
    }
}
