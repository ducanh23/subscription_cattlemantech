<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 25/03/2016
 * Time: 19:09
 */
namespace Magenest\Subscription\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;
use Magenest\Subscription\Model\ProfileFactory;
use Magenest\Subscription\Logger\Logger;
use Magento\Framework\Registry;

abstract class Profile extends Action
{
    /**
     * @var ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Profile constructor.
     * @param Action\Context $context
     * @param PageFactory $pageFactory
     * @param ProfileFactory $profileFactory
     * @param Logger $logger
     * @param Registry $registry
     */
    public function __construct(
        Action\Context $context,
        PageFactory $pageFactory,
        ProfileFactory $profileFactory,
        Logger $logger,
        Registry $registry
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_logger = $logger;
        $this->_profileFactory = $profileFactory;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    protected function _initAction()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_pageFactory->create();
        $resultPage->setActiveMenu('Magenest_Subscription::profile')
            ->addBreadcrumb(__('Recurring Profiles'), __('Recurring Profiles'));

        $resultPage->getConfig()->getTitle()->set(__('Recurring Profiles'));
        return $resultPage;
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Magenest_Subscription::profile');
    }
}
