<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 14/03/2016
 * Time: 14:49
 */
namespace Magenest\Subscription\Controller\Express;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magenest\Subscription\Logger\Logger;
use Magento\Framework\App\Action\Context;
use Magenest\Subscription\Model\Paypal\Api\Nvp\Request\GetExpressCheckoutDetails;

class ReturnAction extends Action
{
    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var GetExpressCheckoutDetails
     */
    protected $_api;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * ReturnAction constructor.
     * @param Context $context
     * @param Logger $logger
     * @param GetExpressCheckoutDetails $api
     * @param \Magento\Checkout\Model\Session $checkoutSession
     */
    public function __construct(
        Context $context,
        Logger $logger,
        GetExpressCheckoutDetails $api,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_logger = $logger;
        $this->_api = $api;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context);
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $token = $this->getRequest()->getParam('token');
        $payerid = $this->getRequest()->getParam('PayerID');
        $data = $this->getRequest()->getParams();

        $quoteId = $this->_checkoutSession->getQuoteId();
        $this->_api->setToken($token);
        $this->_api->setPayerId($payerid);


        try {
            $response = $this->_api->process();
            if ($response) {
                $this->_forward('review', null, null, $response);
            }
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                $e->getMessage()
            );
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                __('We can\'t process Express Checkout approval.')
            );
        }

        return $resultRedirect->setPath('checkout/cart');
    }
}
