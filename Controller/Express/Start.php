<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 14/03/2016
 * Time: 15:19
 */

namespace Magenest\Subscription\Controller\Express;

use Magento\Framework\App\Action\Context;
use Magento\Checkout\Model\Type\Onepage;
use Magenest\Subscription\Model\Paypal\Api\Nvp\Request\SetExpressCheckout;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Directory\Helper\Data;

class Start extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var SetExpressCheckout
     */
    protected $_api;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magenest\Subscription\Api\SubscriptionManagementInterface
     */
    protected $_subscriptionManagement;

    /**
     * @var \Magenest\Subscription\Helper\Util
     */
    protected $util;

    /**
     * Start constructor.
     * @param Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param ScopeConfigInterface $scopeConfigInterface
     * @param \Magenest\Subscription\Api\SubscriptionManagementInterface $subscriptionManagement
     * @param SetExpressCheckout $api
     * @param \Magenest\Subscription\Helper\Util $util
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        ScopeConfigInterface $scopeConfigInterface,
        \Magenest\Subscription\Api\SubscriptionManagementInterface  $subscriptionManagement,
        SetExpressCheckout $api,
        \Magenest\Subscription\Helper\Util $util
    ) {
    
        parent::__construct($context);
        $this->_scopeConfig = $scopeConfigInterface;
        $this->_customerSession = $customerSession;
        $this->_checkoutSession = $checkoutSession;
        $this->_api = $api;
        $this->util = $util;
        $this->_subscriptionManagement = $subscriptionManagement;
    }

    /**
     * Validate subscription quote
     */
    public function validate()
    {
        $quote = $this->_checkoutSession->getQuote();
        $checkoutWithSubscription = $this->util->isQuoteContainsSubscriptionItem();
        $items = $quote->getAllVisibleItems();
        if (count($items) > 1 && $checkoutWithSubscription) {
            $result['mixedCart'] = true;
            $this->messageManager->addError(__('You can sign up for subscription with only one item in cart'));
            $this->_redirect('checkout/cart');
        }

        if (!empty($items)) {
            /** @var \Magento\Quote\Model\Quote\Item  $item */
            foreach ($items as $item) {
                $itemOptions = $item->getOptions();
                $subscriptionOption = null;

                foreach ($itemOptions as $option) {
                    if ($option->getData('code') == 'subscription_options') {
                        $subscriptionOption = $option;
                    }
                    if (is_object($subscriptionOption)) {
                        $storedValue = $subscriptionOption->getValue();
                        $subscriptionValue =  json_decode($storedValue, true);
                        $boxData['start_date'] = (isset($subscriptionValue['start_date']))? $subscriptionValue['start_date']: '';

                        if ($boxData['start_date']) {
                            $output = $this->util->validateStartDate($boxData['start_date']);
                            if (!$output['valid']) {
                                $this->messageManager->addError(__('The start date of your subscription is not valid'));
                                $this->_redirect('checkout/cart');
                            }
                        }
                    }
                }
            }
        }

    }

    /**
     * Controller execute
     */
    public function execute()
    {
        $this->validate();
        try {
            $quote = $this->_checkoutSession->getQuote();

            $localeCode = $this->_scopeConfig->getValue(
                Data::XML_PATH_DEFAULT_LOCALE,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
            $tax = $quote->getShippingAddress()->getTaxAmount();
            $shipping = $quote->getShippingAddress()->getShippingAmount();

            if ($quote->getIsMultiShipping()) {
                $quote->setIsMultiShipping(false);
                $quote->removeAllAddresses();
            }

            $customerData = $this->_customerSession->getCustomerDataObject();
            $quoteCheckoutMethod = $quote->getCheckoutMethod();
            if ($customerData->getId()) {
                $quote->assignCustomerWithAddressChange(
                    $customerData,
                    $quote->getBillingAddress(),
                    $quote->getShippingAddress()
                );
            } elseif ((!$quoteCheckoutMethod || $quoteCheckoutMethod != Onepage::METHOD_REGISTER)
                && !$this->_objectManager->get('Magento\Checkout\Helper\Data')->isAllowedGuestCheckout(
                    $quote,
                    $quote->getStoreId()
                )
            ) {
                $this->messageManager->addNoticeMessage(
                    __('To check out, please sign in with your email address.')
                );

                $this->_objectManager->get('Magento\Checkout\Helper\ExpressRedirect')->redirectLogin($this);
                $this->_customerSession->setBeforeAuthUrl($this->_url->getUrl('*/*/*', ['_current' => true]));

                return;
            }

            $returnUrl = $this->_url->getUrl('*/*/return');
            $cancelUrl = $this->_url->getUrl('*/*/cancel');
            $currency = $quote->getBaseCurrencyCode();


            /** set properties for the paypal api model */
            $boxes = $this->_subscriptionManagement ->getSubscriptionBoxFromQuote($quote);

            $this->_api->setQuote($quote);
            $this->_api->setSubscriptionBoxes($boxes);


             $this->_api->process();


            $url = $this->_api->getRedirectUrl();

            $this->_redirect($url);
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
            $this->_redirect('checkout/cart');
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                __('We can\'t start Express Checkout.')
            );
        }
    }
}
