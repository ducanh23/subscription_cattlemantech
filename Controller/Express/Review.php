<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 14/03/2016
 * Time: 14:57
 */
namespace Magenest\Subscription\Controller\Express;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\Subscription\Logger\Logger;
use Magento\Framework\Registry;

class Review extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var Logger
     */
    protected $_logger;

    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * Review constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param Logger $logger
     * @param Registry $registry
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        Logger $logger,
        Registry $registry
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_logger = $logger;
        $this->_coreRegistry = $registry;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $response = $this->getRequest()->getParams();
        if ($response) {
            $this->_coreRegistry->register('subscription_express_response', $response);

            $result = $this->_resultPageFactory->create();
            $result->getConfig()->getTitle()->set(__('Recurring Order Preview'));

            return $result;
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('An error occured with Paypal response.')
            );
        }
    }
}
