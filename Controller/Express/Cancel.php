<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 14/03/2016
 * Time: 15:03
 */
namespace Magenest\Subscription\Controller\Express;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Cancel extends Action
{
    /**
     * @return mixed
     */
    public function execute()
    {
        try {
            $this->messageManager->addSuccessMessage(
                __('Express Checkout has been canceled.')
            );
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage($e, $e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Unable to cancel Express Checkout'));
        }

        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('checkout/cart');
    }
}
