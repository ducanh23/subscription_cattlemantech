<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 22/03/2016
 * Time: 08:11
 */

namespace Magenest\Subscription\Controller\Express;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magenest\Subscription\Api\SubscriptionManagementInterface;
use Magento\Framework\DataObject;

class PlaceOrder extends Action
{
    /**
     * @var \Magenest\Subscription\Logger\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    /**
     * @var SubscriptionManagementInterface
     */
    protected $_subscriptionManagement;

    /**
     * PlaceOrder constructor.
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Magento\Checkout\Model\Cart $cart
     * @param \Magenest\Subscription\Logger\Logger $logger
     * @param PageFactory $pageFactory
     * @param Context $context
     * @param SubscriptionManagementInterface $subscriptionManagement
     */
    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Model\Cart $cart,
        \Magenest\Subscription\Logger\Logger $logger,
        PageFactory $pageFactory,
        Context $context,
        SubscriptionManagementInterface $subscriptionManagement
    ) {
    
        $this->_cart = $cart;
        $this->_logger = $logger;
        $this->_resultPageFactory = $pageFactory;
        $this->_checkoutSession = $checkoutSession;
        $this->_subscriptionManagement = $subscriptionManagement;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        try {
            $post_value = $this->getRequest()->getPostValue();
            $token = $post_value['subscription_token'];
            $quoteId = $this->_checkoutSession->getQuoteId();
            $payment = new DataObject();

            $payment->addData([
                'payment_method' => 'paypal_express'
            ]);

            //sign up the payment

            $quote = $this->_checkoutSession->getQuote();
            $this->_subscriptionManagement->process($quote, $payment);

            $result = $this->_resultPageFactory->create();
            $result->getConfig()->getTitle()->prepend(__('Recurring Order Successfully Submitted'));

            return $this->_redirect('checkout/onepage/success/');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                $e->getMessage()
            );
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('checkout/cart');
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage(
                $e,
                __('We can\'t place the order.')
            );
            $this->_logger->addDebug($e->getMessage());
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('checkout/cart');
        }
    }
}
