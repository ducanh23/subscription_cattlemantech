<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 22/10/2017
 * Time: 14:59
 */
namespace Magenest\Subscription\Plugin\CustomerData;

class Cart
{
    /**
     * @var \Magenest\Subscription\Helper\Util
     */
    protected $util;

    /**
     * Cart constructor.
     * @param \Magenest\Subscription\Helper\Util $util
     */
    public function __construct(\Magenest\Subscription\Helper\Util $util)
    {
        $this->util = $util;
    }

    /**
     * Append the information about subscription into the cart section
     */
    public function afterGetSectionData(
        \Magento\Checkout\CustomerData\Cart $subject,
        $result
    ) {
    
        $isSubscription = $this->util->isQuoteContainsSubscriptionItem();
        $result['isSubscription'] = $isSubscription;
        return $result;
    }
}
