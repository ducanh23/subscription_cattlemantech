<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Plugin;

class ListProduct
{
    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * ListProduct constructor.
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ProductFactory $productFactory
    ) {
    
        $this->_productFactory = $productFactory;
    }

    /**
     * @param \Magento\Catalog\Block\Product\AbstractProduct $subject
     * @param callable $proceed
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function aroundGetProductPrice(
        \Magento\Catalog\Block\Product\AbstractProduct $subject,
        callable $proceed,
        \Magento\Catalog\Model\Product $product
    ) {
    
        $productModel = $this->_productFactory->create()->load($product->getData('entity_id'));
        $value = $productModel->getData('enable_subscription');
        $result = $proceed($product);
        if ($value) {
            return $result . '<strong>Subscription</strong>';
        } else {
            return $result;
        }
    }
}
