<?php
/**
 * Created by PhpStorm.
 * User: ducanh
 * Date: 31/05/2018
 * Time: 17:22
 */

namespace Magenest\Subscription\Plugin;

class Quote
{
    protected $_productCollectionFactory;

    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
    }

    public function aroundGetStoreId(\Magento\Quote\Model\ResourceModel\Quote\Item\Collection $subject, callable $proceed)
    {
        $a = 0;
        return (int)$this->_productCollectionFactory->create()->getStoreId();

    }
}