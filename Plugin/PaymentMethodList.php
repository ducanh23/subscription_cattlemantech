<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 25/08/2017
 * Time: 21:14
 */

namespace Magenest\Subscription\Plugin;

class PaymentMethodList
{
    /**
     * @param \Magento\Payment\Model\PaymentMethodList $subject
     * @param $result
     * @return array
     */
    public function afterGetActiveList(
        \Magento\Payment\Model\PaymentMethodList $subject,
        $result
    ) {
    
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $output = [];
        /** @var  \Magenest\Subscription\Helper\Data $helper */
        $helper = $objectManager->create('Magenest\Subscription\Helper\Data');
        $supportedSubsGateways = $helper->getOnlineGatewayCodes();
        if (!empty($result)) {
            /** @var \Magento\Payment\Model\PaymentMethod $paymentMethod */
            foreach ($result as $paymentMethod) {
                $code = $paymentMethod->getCode();
                $position = strpos($code, 'paypal');
                if (in_array($code, $supportedSubsGateways) || $position !== false) {
                    $output[] = $paymentMethod;
                }
            }
        }
        return $output;
    }
}
