<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 28/08/2017
 * Time: 23:32
 */

namespace Magenest\Subscription\Plugin;

class CartAdd
{
    /** @var \Magenest\Subscription\Helper\Util
     * */
    protected $util;

    /**
     * CartAdd constructor.
     * @param \Magenest\Subscription\Helper\Util $util
     */
    public function __construct(
        \Magenest\Subscription\Helper\Util $util
    ) {
    
        $this->util = $util;
    }

    public function beforeExecute(
        \Magento\Checkout\Controller\Cart\Add $subject
    ) {
        $productId = $subject->getRequest()->getParam('product');
        if ($productId == 45) {
            $this->util->createNewQuote($productId);
        }
    }
}
