<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 11/01/2017
 * Time: 09:58
 */
namespace Magenest\Subscription\Plugin;

use Magenest\Subscription\Logger\Logger;

class SuccessPlugin
{
    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    protected $_eventManager;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * SuccessPlugin constructor.
     * @param \Magento\Framework\Event\ManagerInterface $eventManager
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param Logger $logger
     */
    public function __construct(
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        Logger $logger
    ) {
        $this->_orderFactory = $orderFactory;
        $this->_eventManager = $eventManager;
        $this->resultPageFactory = $pageFactory;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Checkout\Controller\Onepage\Success $subject
     * @param callable $proceed
     * @return \Magento\Framework\View\Result\Page
     */
    public function aroundExecute(\Magento\Checkout\Controller\Onepage\Success $subject, callable $proceed)
    {
        $session = $subject->getOnepage()->getCheckout();
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->_orderFactory->create();
        $orderId = $order->getCollection()->getLastItem()->getData($order->getIdFieldName());
        $realId = $order->load($orderId)->getRealOrderId();
        $session->clearHelperData();
        $session->clearQuote()->clearStorage();
        $session->clearStorage();
        $session->setCartWasUpdated(true);
        $session->setLastRealOrderId($realId);
        $resultPage = $this->resultPageFactory->create();

        return $resultPage;
    }
}
