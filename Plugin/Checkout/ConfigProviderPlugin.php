<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 09/10/2017
 * Time: 21:41
 */
namespace Magenest\Subscription\Plugin\Checkout;

use Magento\Checkout\Model\Session as CheckoutSession;

class ConfigProviderPlugin
{
    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var \Magenest\Subscription\Api\SubscriptionManagementInterface
     */
    protected $subscriptionManager;

    /**
     * @var \Magenest\Subscription\Helper\Util
     */
    protected $util;

    /**
     * Paypal supported currency
     * @var array
     */
    protected $paypalSupportedCurrency = ['AUD', 'BRL', 'CAD', 'CZK', 'DKK', 'EUR', 'HKD', 'HUF', 'ILS', 'JPY', 'MYR', 'MXN',
        'NOK', 'NZD', 'PHP', 'PLN', 'GBP', 'SGD', 'SEK', 'CHF', 'TWD', 'THB', 'USD'
    ];

    /**
     * ConfigProviderPlugin constructor.
     * @param \Magenest\Subscription\Api\SubscriptionManagementInterface $subscriptionManagement
     * @param \Magenest\Subscription\Helper\Util $util
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        \Magenest\Subscription\Api\SubscriptionManagementInterface $subscriptionManagement,
        \Magenest\Subscription\Helper\Util $util,
        CheckoutSession $checkoutSession
    ) {
    
        $this->subscriptionManager = $subscriptionManagement;
        $this->checkoutSession = $checkoutSession;
        $this->util = $util;
    }

    /**
     * @param \Magento\Checkout\Model\DefaultConfigProvider $subject
     * @param array $result
     * @return array
     */
    public function afterGetConfig(\Magento\Checkout\Model\DefaultConfigProvider $subject, array $result)
    {
        $quote = $this->checkoutSession->getQuote();
        if ($quote) {
            $quoteCurrencyCode = $quote->getQuoteCurrencyCode();
            $result['quoteCurrencyCode'] = $quoteCurrencyCode;
            if (!in_array($quoteCurrencyCode, $this->paypalSupportedCurrency)) {
                $result['currencyIsSupportedByPaypal'] = 0;
            } else {
                $result['currencyIsSupportedByPaypal'] = 1;

            }

            $checkoutWithSubscription = $this->util->isQuoteContainsSubscriptionItem();
            $items = $quote->getAllVisibleItems();
            if (count($items) > 1 && $checkoutWithSubscription) {
                $result['mixedCart'] = true;
            } else {
                $result['mixedCart'] = false;
            }

            if ($checkoutWithSubscription) {
                $result['checkoutWithSubscription'] = 1;
            } else {
                $result['checkoutWithSubscription'] = 0;
            }

            //validate the start date of subscription item. validate is a jason which has following format {valid:false, message:ok , detail : [{} {}]}
            $validates = $this->util->validateQuote();
            $result['validateSubscription'] = $validates;
        }
        return $result;
    }
}
