<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 20/12/2016
 * Time: 15:38
 */

namespace Magenest\Subscription\Helper;

use Magenest\Subscription\Logger\Logger;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

class Util extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_SHIPPING_TYPE_PATH = 'magenest_subscription/settings_general/shipping_type';

    /**
     * @var \Magento\Framework\Data\Form\FormKey
     */
    protected $_formkey;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quote;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Quote\Model\QuoteManagement
     */
    protected $quoteManagement;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var \Magento\Sales\Model\Service\OrderService
     */
    protected $orderService;

    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Quote\Model\QuoteFactory
     */
    protected $quoteFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Quote\Api\CartRepositoryInterface
     */
    protected $cartRepositoryInterface;

    /**
     * @var \Magento\Quote\Api\CartManagementInterface
     */
    protected $cartManagementInterface;

    /**
     * @var CheckoutSession
     */
    public $checkoutSession;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $productCollection;

    /**
     * Util constructor.
     * @param Context $context
     * @param \Magento\Catalog\Model\Product $product
     * @param \Magento\Quote\Model\QuoteFactory $quote
     * @param Logger $logger
     * @param \Magento\Framework\Data\Form\FormKey $formkey
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Quote\Model\QuoteManagement $quoteManagement
     * @param \Magento\Sales\Model\Service\OrderService $orderService
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface
     * @param \Magento\Quote\Api\CartManagementInterface $cartManagementInterface
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param CheckoutSession $checkoutSession
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
     */
    public function __construct(
        Context $context,
        \Magento\Catalog\Model\Product $product,
        \Magento\Quote\Model\QuoteFactory $quote,
        \Magenest\Subscription\Logger\Logger $logger,
        \Magento\Framework\Data\Form\FormKey $formkey,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Quote\Model\QuoteManagement $quoteManagement,
        \Magento\Sales\Model\Service\OrderService $orderService,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        CheckoutSession $checkoutSession,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
    ) {
    
        parent::__construct($context);
        $this->cartManagementInterface = $cartManagementInterface;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->customerRepository = $customerRepository;
        $this->customerFactory = $customerFactory;
        $this->quoteManagement = $quoteManagement;
        $this->productFactory = $productFactory;
        $this->_storeManager = $storeManager;
        $this->orderService = $orderService;
        $this->_formkey = $formkey;
        $this->_product = $product;
        $this->_logger = $logger;
        $this->quote = $quote;
        $this->checkoutSession = $checkoutSession;
        $this->productCollection = $productCollection;
    }

    /**
     * get active quote for current user
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->checkoutSession->getQuote();

    }

    /**
     * validate item in the quote
     * @return string
     */
    public function validateQuote()
    {
        $result  = [
           'valid' => true,
           'message' => 'ok',
           'detail' =>[]
        ];
        ;
        $quote = $this->getQuote();
        if ($quote) {
            $allItems  = $quote->getAllVisibleItems();
            if (!empty($allItems)) {
                /** @var \Magento\Quote\Model\Quote\Item  $item */
                foreach ($allItems as $item) {
                    $quoteItemId = $item->getId();
                    $itemOptions = $item->getOptions();
                    $subscriptionOption = null;

                    foreach ($itemOptions as $option) {
                        if ($option->getData('code') == 'subscription_options') {
                            $subscriptionOption = $option;
                        }
                        if (is_object($subscriptionOption)) {
                            $storedValue = $subscriptionOption->getValue();
                            $subscriptionValue =  json_decode($storedValue, true);
                            $boxData['start_date'] = (isset($subscriptionValue['start_date']))? $subscriptionValue['start_date']: '';

                            if ($boxData['start_date']) {
                                $output = $this->validateStartDate($boxData['start_date']);
                                $result['detail'][] = $output;
                                if (!$output['valid']) {
                                    $result['valid'] = false;
                                    $result['message'] = "invalid start date";
                                }
                            }
                        }
                    }
                }
            }
        }
        return json_encode($result);
    }

    /**
     * @param $billingStartDate
     * @return array
     */
    public function validateStartDate($billingStartDate)
    {
        $out = ['valid' => true, 'message' => "ok"];
        $scheduledDate = date_create_from_format('Y-m-d', $billingStartDate);
        $today = new \DateTime();
        $interval = $today->diff($scheduledDate);
        $compare = $interval->format('%R');

        if ($compare == '-') {
            $out['valid'] = false;
            $out['message'] = 'invalid start date';
        }
        return $out;
    }

    /**
     * get information about quote to check whether it contains subscription item
     * @return bool
     */
    public function isQuoteContainsSubscriptionItem()
    {
        $quote = $this->getQuote();
        if ($quote) {
            $allItems = $quote->getAllItems();
            if ($allItems) {
                /** @var \Magento\Quote\Model\ResourceModel\Quote\Item $item */
                foreach ($allItems as $item) {
                    $itemOptions = $item->getOptions();
                    $subscriptionOption = null;

                    foreach ($itemOptions as $option) {
                        if ($option->getData('code') == 'subscription_options') {
                            $subscriptionOption = $option;
                        }
                    }

                    if ($subscriptionOption) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * get information about quote to check whether it contains subscription item
     * @return bool
     */
    public function isQuoteContainsOneTimePurchasedItem()
    {
        $quote = $this->getQuote();
        if ($quote) {
            $allItems = $quote->getAllItems();
            if ($allItems) {
                /** @var \Magento\Quote\Model\ResourceModel\Quote\Item $item */
                foreach ($allItems as $item) {
                    $oneTimePurchased = true;
                    $itemOptions = $item->getOptions();
                    $subscriptionOption = null;

                    foreach ($itemOptions as $option) {
                        if ($option->getData('code') == 'subscription_options') {
                            $oneTimePurchased = false;
                        }
                    }

                    if ($oneTimePurchased) {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    public function getShippingInfo($quote)
    {
        /** @var \Magento\Sales\Model\Order\Address $address */
        $address = $quote->getShippingAddress();
        $streetArr = $address->getStreet();
        $streetFull = $streetArr[0];

        return [
            'firstname' => $address->getFirstname(),
            'lastname' => $address->getLastname(),
            'city' => $address->getCity(),
            'postcode' => $address->getPostcode(),
            'telephone' => $address->getTelephone(),
            'street' => $streetFull,
            'customer_id' => $quote->getCustomerId(),
            'email' => $quote->getCustomerEmail(),
            'region' => $address->getRegion(),
            'regionCode' => $address->getRegionCode(),
            'region_id' => $address->getRegionId(),
            'country_id' => $address->getCountryId()
        ];
    }

    /**
     * @param \Magento\Sales\Model\Order $newOrder
     * @param \Magenest\Subscription\Model\Profile $profile
     * @return \Magenest\Subscription\Model\Profile
     */
    public function updateProfile($newOrder, $profile)
    {
        return $profile->addSequenceOrder($newOrder->getIncrementId());
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    public function getPaymentInfo($quote)
    {
        /** @var \Magento\Sales\Model\Order\Address $address */
        $address = $quote->getBillingAddress();
        $streetArr = $address->getStreet();
        $streetFull = $streetArr[0];

        return [
            'firstname' => $address->getFirstname(),
            'lastname' => $address->getLastname(),
            'city' => $address->getCity(),
            'postcode' => $address->getPostcode(),
            'telephone' => $address->getTelephone(),
            'street' => $streetFull,
            'customer_id' => $quote->getCustomerId(),
            'email' => $quote->getCustomerEmail(),
            'region' => $address->getRegion(),
            'regionCode' => $address->getRegionCode(),
            'region_id' => $address->getRegionId(),
            'country_id' => $address->getCountryId()
        ];
    }

    public function calculateAssignProducts($selected, $excluded, $excludeMode)
    {
        $this->transformArray($selected);
        $this->transformArray($excluded);

        $products = $this->productCollection->addAttributeToSelect('entity_id')->load()->toArray();
        if ($excludeMode == 'true') {
            if (is_array($excluded)) {
                $resultProducts = array_diff_key($products, $excluded);
            } else {
                $resultProducts = $products;
            }
        } else {
            if (is_array($excluded)) {
                $selectedProducts = array_diff($selected, $excluded);
            } else {
                $selectedProducts = $selected;
            }

            $resultProducts = array_intersect_key($products, $selectedProducts);
        }

        return $resultProducts;
    }

    /**
     * @param $array[]
     */
    public function transformArray(&$array)
    {
        if (is_array($array)) {
            foreach ($array as $index => $item) {
                $array[$item] = $item;
            }

            foreach ($array as $index => $item) {
                if ($index != $item) {
                    unset($array[$index]);
                }
            }
        }
    }

    public function checkMagentoVersion()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productMetadata = $objectManager->get('Magento\Framework\App\ProductMetadataInterface');
        $version = $productMetadata->getVersion();
        $subVersion = substr($version, 0, 3);
        $subVersion = floatval($subVersion);

        return $subVersion;
    }

    public function stringifyArray($array)
    {
        $version = $this->checkMagentoVersion();

        if ($version == 2.1) {
            return serialize($array);
        } else {
            return json_encode($array, true);
        }
    }

    public function destringifyArray($array)
    {
        $version = $this->checkMagentoVersion();

        if ($version == 2.1) {
            return unserialize($array);
        } else {
            return json_decode($array, true);
        }
    }
}
