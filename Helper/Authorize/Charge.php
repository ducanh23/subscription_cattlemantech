<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 7/19/17
 * Time: 10:38 AM
 */

namespace Magenest\Subscription\Helper\Authorize;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class Charge extends AbstractRequest
{
    const DEFAULT_DESCRIPTION = "This's a description";

    /**
     * @param string $ccNumber
     * @param string $expDate
     * @param string $ccCode
     * @return AnetAPI\CreditCardType
     */
    public function getCreditCard($ccNumber, $expDate, $ccCode = null)
    {
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($ccNumber);
        $creditCard->setExpirationDate($expDate);
        if ($ccCode) {
            $creditCard->setCardCode($ccCode);
        }
        return $creditCard;
    }

    /**
     * @param AnetAPI\CreditCardType $cc
     * @return AnetAPI\PaymentType
     */
    public function getPayment($cc)
    {
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($cc);
        return $paymentOne;
    }

    /**
     * @param null|string $description
     * @param null|string $invoiceNumber
     * @return AnetAPI\OrderType
     */
    public function getOrder($description = null, $invoiceNumber = null)
    {
        $order = new AnetAPI\OrderType();
        if (!$invoiceNumber) {
            $order->setInvoiceNumber($invoiceNumber);
        }
        $order->setDescription($description ? $description : self::DEFAULT_DESCRIPTION);
        return $order;
    }

    /**
     * @param array $data
     * @return AnetAPI\CustomerAddressType
     */
    public function getCustomerAddress($data)
    {
        $customerAddress = new AnetAPI\CustomerAddressType();
        if (isset($data['last_name'])) {
            $customerAddress->setFirstName($data['first_name']);
        }
        if (isset($data['last_name'])) {
            $customerAddress->setLastName($data['last_name']);
        }
        if (isset($data['company'])) {
            $customerAddress->setCompany($data['company']);
        }
        if (isset($data['address'])) {
            $customerAddress->setAddress($data['address']);
        }
        if (isset($data['city'])) {
            $customerAddress->setCity($data['city']);
        }
        if (isset($data['state'])) {
            $customerAddress->setState($data['state']);
        }
        if (isset($data['zip_code'])) {
            $customerAddress->setZip($data['zip_code']);
        }
        if (isset($data['country'])) {
            $customerAddress->setCountry($data['country']);
        }
        return $customerAddress;
    }

    /**
     * @param string $email
     * @param null|string|int $id
     * @param null|string $type = {individual, business}
     * @return AnetAPI\CustomerDataType
     */
    public function getCustomerData($email, $id = null, $type = null)
    {
        $customerData = new AnetAPI\CustomerDataType();
        $customerData->setEmail($email);
        $customerData->setType($type ? $type : "individual");
        if ($id) {
            $customerData->setId($id);
        }
        return $customerData;
    }

    /**
     * @param null|string $name = {allowPartialAuth, duplicateWindow, emailCustomer, recurringBilling}
     * @param null|bool|int $value
     * @return AnetAPI\SettingType
     */
    public function getSetting($name = null, $value = null)
    {
        // Add values for transaction settings
        $setting = new AnetAPI\SettingType();
        $setting->setSettingName($name ? $name : "duplicateWindow");
        $setting->setSettingValue($value ? $value : "60");
        return $setting;
    }

    /**
     * Add some merchant defined fields. These fields won't be stored with the transaction,
     * but will be echoed back in the response.
     * @param string $name
     * @param string $value
     * @return AnetAPI\UserFieldType
     */
    public function getUserField($name, $value)
    {
        $merchantDefinedField = new AnetAPI\UserFieldType();
        $merchantDefinedField->setName($name);
        $merchantDefinedField->setValue($value);
        return $merchantDefinedField;
    }

    /**
     * Create a TransactionRequestType object and add the previous objects to it
     * @param bool|string $capture
     * @param int|float $amt
     * @param AnetAPI\OrderType $order
     * @param AnetAPI\PaymentType $payment
     * @param AnetAPI\CustomerAddressType $addr
     * @param AnetAPI\CustomerDataType $customer
     * @param AnetAPI\SettingType $setting
     * @param null|AnetAPI\UserFieldType $userField
     * @return AnetAPI\TransactionRequestType
     */
    public function getTransactionRequest($capture, $amt, $order, $payment, $addr, $customer, $setting, $userField = null)
    {
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType($capture ? "authCaptureTransaction" : "authOnlyTransaction");
        $transactionRequestType->setAmount($amt);
        $transactionRequestType->setOrder($order);
        $transactionRequestType->setPayment($payment);
        $transactionRequestType->setBillTo($addr);
        $transactionRequestType->setCustomer($customer);
        $transactionRequestType->addToTransactionSettings($setting);
        if (!$userField) {
            $userField = [];
        }
        foreach ($userField as $field) {
            $transactionRequestType->addToUserFields($field);
        };
        return $transactionRequestType;
    }

    /**
     * Assemble the complete transaction request
     * @param AnetAPI\MerchantAuthenticationType $merchantAuthentication
     * @param AnetAPI\TransactionRequestType $transactionRequestType
     * @param null|string $refId
     * @return AnetAPI\CreateTransactionRequest
     */
    public function getCreateTransactionRequest($merchantAuthentication, $transactionRequestType, $refId = null)
    {
        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setTransactionRequest($transactionRequestType);
        $request->setRefId($refId ? $refId : $this->getRefId());
        return $request;
    }

    /**
     * @param AnetAPI\CreateTransactionRequest $request
     * @param bool $isLive
     * @return AnetAPI\AnetApiResponseType
     */
    public function executeRequest($request, $isLive = null)
    {
        $apiEndpoint = $isLive ?
            \net\authorize\api\constants\ANetEnvironment::PRODUCTION :
            \net\authorize\api\constants\ANetEnvironment::SANDBOX;
        $controller = new AnetController\CreateTransactionController($request);
        return $controller->executeWithApiResponse($apiEndpoint);
    }

    /**
     * @param AnetAPI\AnetApiResponseType $response
     * @return array
     */
    public function responseParser($response)
    {
        $return['result'] = "Fail";
        if ($response != null) {
            if ($response->getMessages()->getResultCode() == self::RESPONSE_OK) {
                // Since the API request was successful, look for a transaction response
                // and parse it to display the results of authorizing the card
                $tresponse = $response->getTransactionResponse();

                if ($tresponse != null && $tresponse->getMessages() != null) {
                    $return['result'] = "Success";
                    $return['transaction_id'] = $tresponse->getTransId();
                    $return['response_code'] = $tresponse->getResponseCode();
                    $return['message_code'] = $tresponse->getMessages()[0]->getCode();
                    $return['auth_code'] = $tresponse->getAuthCode();
                    $return['description'] = $tresponse->getMessages()[0]->getDescription();
                } else {
                    if ($tresponse->getErrors() != null) {
                        $return['error_code'] = $tresponse->getErrors()[0]->getErrorCode();
                        $return['error_text'] = $tresponse->getErrors()[0]->getErrorText();
                    }
                }
                // Or, print errors if the API request wasn't successful
            } else {
                $return['result'] = "Fail";
                $tresponse = $response->getTransactionResponse();
                if ($tresponse != null && $tresponse->getErrors() != null) {
                    $return['error_code'] = $tresponse->getErrors()[0]->getErrorCode();
                    $return['error_text'] = $tresponse->getErrors()[0]->getErrorText();
                } else {
                    $return['error_code'] = $response->getMessages()->getMessage()[0]->getCode();
                    $return['error_text'] = $response->getMessages()->getMessage()[0]->getText();
                }
            }
        } else {
            $return['error_text'] = "No response returned";
        }
        return $return;
    }
}
