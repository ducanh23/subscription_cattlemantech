<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 8/7/17
 * Time: 2:00 PM
 */

namespace Magenest\Subscription\Helper\Authorize;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class TransactionReporting extends AbstractRequest
{
    /**
     * @param AnetAPI\GetTransactionDetailsRequest $request
     * @param null $isLive
     * @return AnetAPI\AnetApiResponseType
     */
    public function executeRequest($request, $isLive = null)
    {
        $apiEndpoint = $isLive ?
            \net\authorize\api\constants\ANetEnvironment::PRODUCTION :
            \net\authorize\api\constants\ANetEnvironment::SANDBOX;
        $controller = new AnetController\GetTransactionDetailsController($request);
        return $controller->executeWithApiResponse($apiEndpoint);
    }

    /**
     * @param string $transactionId
     * @param null|AnetAPI\MerchantAuthenticationType $merchantAuthentication
     * @return AnetAPI\GetTransactionDetailsRequest
     */
    public function getTransactionDetailsRequest($transactionId, $merchantAuthentication = null)
    {
        if (!is_null($merchantAuthentication)) {
            $merchantAuthentication = $this->getMerchantAuthentication();
        }
        $request = new AnetAPI\GetTransactionDetailsRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setTransId($transactionId);
        return $request;
    }

    /**
     * @param AnetAPI\AnetApiResponseType $response
     * @return array
     */
    public function responseParser($response)
    {
        if (($response != null) && ($response->getMessages()->getResultCode() == self::RESPONSE_OK)) {
            $res['status'] = $response->getTransaction()->getTransactionStatus();
            $res['amount'] = $response->getTransaction()->getAuthAmount();
            $res['trans_id'] = $response->getTransaction()->getTransId();
            $subs = $response->getTransaction()->getSubscription();
            if (!empty($subs)) {
                $res['subs_id'] = $subs->getId();
                $res['paynum'] = $subs->getPayNum();
            }
            $order = $response->getTransaction()->getOrder();
            $res['data'] = json_decode($order->getDescription(), true);
//            $res['data'] = json_decode("{\"first_order_id\":\"000000200\",\"sku\":\"24-UG06\",\"qty\":1}", true);

//            var_dump($res);
        } else {
            $errorMessages = $response->getMessages()->getMessage();
            $res['error_code'] = $errorMessages[0]->getCode();
            $res['error_text'] = $errorMessages[0]->getText();
//            var_dump($res);
        }
        return $res;

    }
}
