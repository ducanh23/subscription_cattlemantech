<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 8/7/17
 * Time: 2:00 PM
 */

namespace Magenest\Subscription\Helper\Authorize;

use net\authorize\api\contract\v1 as AnetAPI;

/**
 * Class AbstractRequest
 * @package Magenest\Subscription\Helper\Authorize
 */
abstract class AbstractRequest
{
    /**
     * Response code
     */
    const RESPONSE_OK = "Ok";

    /**
     * Merchant ID
     * @var string
     */
    protected $_merchantId;

    /**
     * Merchant Transaction Key
     * @var string
     */
    protected $_merchantTransactionKey;

    /**
     * Authorizenet Request constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
    
        $this->_merchantId = $scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_MERCHANT_ID);
        $this->_merchantTransactionKey = $scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_TRANSACTION_KEY);
    }

    /**
     * @param AnetAPI\ANetApiRequestType $request
     * @param bool $isLive
     * @return AnetAPI\AnetApiResponseType
     */
    abstract public function executeRequest($request, $isLive = null);

    /**
     * @param string $loginId
     * @param string $transKey
     * @return AnetAPI\MerchantAuthenticationType
     */
    public function getMerchantAuthentication($loginId = null, $transKey = null)
    {
        if (is_null($loginId)) {
            $loginId = $this->_merchantId;
        }
        if (is_null($transKey)) {
            $transKey = $this->_merchantTransactionKey;
        }
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName($loginId);
        $merchantAuthentication->setTransactionKey($transKey);
        return $merchantAuthentication;
    }

    /**
     * @return string
     */
    public function getRefId()
    {
        return 'ref' . time();
    }
}
