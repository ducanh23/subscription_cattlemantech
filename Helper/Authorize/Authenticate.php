<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 8/7/17
 * Time: 2:00 PM
 */

namespace Magenest\Subscription\Helper\Authorize;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class Authenticate extends AbstractRequest
{
    /**
     * @param AnetAPI\ANetApiRequestType $request
     * @param null $isLive
     * @return AnetAPI\AnetApiResponseType
     */
    public function executeRequest($request, $isLive = null)
    {
        $apiEndpoint = $isLive ?
            \net\authorize\api\constants\ANetEnvironment::PRODUCTION :
            \net\authorize\api\constants\ANetEnvironment::SANDBOX;
        $controller = new AnetController\AuthenticateTestController($request);
        return $controller->executeWithApiResponse($apiEndpoint);
    }

    /**
     * @param null|AnetAPI\MerchantAuthenticationType $merchantAuthentication
     * @param null|string $refId
     * @return AnetAPI\ANetApiRequestType
     */
    public function getAuthenticateTestRequest($merchantAuthentication = null, $refId = null)
    {
        if (!$merchantAuthentication) {
            $merchantAuthentication = $this->getMerchantAuthentication();
        }
        $request = new AnetAPI\AuthenticateTestRequest();
        $request->setRefId($refId ? $refId : $this->getRefId());
        $request->setMerchantAuthentication($merchantAuthentication);
        return $request;
    }
}
