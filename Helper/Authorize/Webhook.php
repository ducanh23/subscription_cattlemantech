<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/11/17
 * Time: 3:36 PM
 */

namespace Magenest\Subscription\Helper\Authorize;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\HTTP\Client\Curl;

/**
 * Class Webhook
 * @package Magenest\Subscription\Helper\Authorize
 */
class Webhook extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Authorizenet webhook controller path
     */
    const CONTROLLER_PATH = 'subscription/webhooks/authorizenet';

    /**
     * @var string
     */
    protected $_preEndpoint;

    /**
     * @var string
     */
    protected $_webhookUrl;

    /**
     * @var string
     */
    protected $_authorizationHeader;

    /**
     * @var array
     */
    protected $_webhookArray;

    /**
     * @var bool|string
     */
    protected $_status;

    /**
     * @var Curl
     */
    protected $curl;

    /**
     * Webhook constructor.
     * @param Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\UrlInterface $url
     */
    public function __construct(
        Context $context,
        Curl $curl
    ) {
    
        parent::__construct($context);
        $merchantId = $this->scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_MERCHANT_ID);
        $merchantTransactionKey = $this->scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_TRANSACTION_KEY);
        $isTest = $this->scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_IS_TEST);
        $this->curl = $curl;
        $this->_authorizationHeader = "Basic " . base64_encode($merchantId . ':' . $merchantTransactionKey);
        $this->_preEndpoint = $isTest ? 'https://apitest.authorize.net/' : 'https://api.authorize.net/';
    }

    public function getMerchantId()
    {
        return $this->scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_MERCHANT_ID);
    }

    public function getMerchantKey()
    {
        return $this->scopeConfig->getValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_TRANSACTION_KEY);
    }

    /**
     * @return string
     */
    public function getPreEndpoint()
    {
        return $this->_preEndpoint;
    }

    /**
     * @return string
     */
    public function getAuthorizationHeader()
    {
        return $this->_authorizationHeader;
    }

    /**
     * @param array $webhooks
     * @return bool|string
     */
    public function checkValidWebhooks()
    {
        if (!isset($this->_status)) {
            $this->_status = __("Webhook is not available")->getText();
            $webhooks = $this->getWebhooks();

            if ($webhooks) {
                foreach ($webhooks as $webhook) {
                    if (is_object($webhook)) {
                        if ($webhook->url != $this->getWebhookUrl()) {
                            continue;
                        }
                        if ($webhook->status != 'active') {
                            continue;
                        }
                        if (!in_array('net.authorize.payment.authcapture.created', $webhook->eventTypes)) {
                            continue;
                        }
                        if (!in_array('net.authorize.payment.authorization.created', $webhook->eventTypes)) {
                            continue;
                        }
                        $this->_status = true;
                    }
                }
            }
        }
        return $this->_status;
    }

    /**
     * @return array
     */
    public function getWebhooks()
    {
        if (!is_array($this->_webhookArray)) {
            $this->_webhookArray = json_decode($this->callGetWebhooks());
        }
        return $this->_webhookArray;
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    protected function callGetWebhooks()
    {
        try {
            $apiUrl =  $this->_preEndpoint . "rest/v1/webhooks";
            $login  = $this->getMerchantId();
            $password = $this->getMerchantKey();
            $this->curl->setCredentials($login, $password);
            $this->curl->get($apiUrl);
            $response =  $this->curl->getBody();

            return $response;
        } catch (\Exception $e) {
            return $this->getCache()->loadPartnersFromCache();
        }
    }

    /**
     * Return URL for submitting to Authorizenet.net
     * @return string
     */
    public function getWebhookUrl()
    {
        if (!is_string($this->_webhookUrl)) {
            $this->_webhookUrl = $this->_urlBuilder->getBaseUrl() . self::CONTROLLER_PATH;
        }
        return $this->_webhookUrl;
    }
}
