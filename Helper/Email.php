<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 7/8/17
 * Time: 2:48 PM
 */

namespace Magenest\Subscription\Helper;

use Magento\Framework\App\Helper\Context;

class Email extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Types of template
     */
    const TYPE_TEXT = 1;
    const TYPE_HTML = 2;

    const PRE_EMAIL_TEMPLATE_PATH = 'magenest_subscription/mail_settings/';
    const START_SUBSCRIPTION_TEMPLATE_ID = 'start_subscription';
    const SUBSCRIPTION_PAY_TEMPLATE_ID = 'subscription_pay';
    const CANCEL_SUBSCRIPTION_TEMPLATE_ID = 'cancel_subscription';
    const SUSPEND_SUBSCRIPTION_TEMPLATE_ID = 'suspend_subscription';

    const SENDER_PATH = "magenest_subscription/mail_settings/sender";
    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * Email constructor.
     * @param Context $context
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $state
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $state,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
    
        $this->_storeManager = $storeManager;
        $this->_inlineTranslation = $state;
        $this->_transportBuilder = $transportBuilder;
        parent::__construct($context);
    }

    /**
     * @param $toAddress
     * @param $toName
     * @param $emailTempalte
     * @param null $data
     * @throws \Magento\Framework\Exception\MailException
     */
    public function send($toAddress, $toName, $emailTempalte, $data = null)
    {
        if (!is_array($data)) {
            $data = [];
        }
        $templateId = $this->scopeConfig->getValue(self::PRE_EMAIL_TEMPLATE_PATH . $emailTempalte);
        if ($templateId === '-1') {
            return ;
        } else {
            try {
                $this->_inlineTranslation->suspend();
                $transport = $this->_transportBuilder
                    ->setTemplateIdentifier($templateId)
                    ->setTemplateOptions([
                        'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                        'store' => $this->_storeManager->getStore()->getId()
                    ])->setTemplateVars(
                        $data
                    )->setFrom(
                        $this->getSender()
                    )->addTo(
                        $toAddress,
                        $toName
                    )->getTransport();
                $transport->sendMessage();
                $this->_inlineTranslation->resume();
            } catch (\Magento\Framework\Exception\MailException $e) {
                $this->_logger->critical($e->getMessage());
                throw $e;
            } catch (\UnexpectedValueException $e) {
                $this->_logger->critical($e->getMessage());
                throw $e;
            }
        }
    }

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->scopeConfig->getValue(self::SENDER_PATH);
    }
}
