<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 23/07/2016
 * Time: 13:18
 */

namespace Magenest\Subscription\Helper;

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;
//use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Catalog\Model\ProductFactory;
//use Magento\Sales\Model\OrderFactory;
use Magenest\Subscription\Model\ProfileFactory;

/**
 * Helper Class for Authorizenet Subscription
 * @package Magenest\Subscription\Helper
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /** Paypal Express Checkout core configs */
    const XML_PAYPAL_EXPRESS_PATH = "payment/paypal_express/active";

    /** Authorizenet core configs */
    const XML_AUTHORIZENET_IS_TEST = "payment/authorizenet_directpost/test";
    const XML_AUTHORIZENET_MERCHANT_ID = "payment/authorizenet_directpost/login";
    const XML_AUTHORIZENET_TRANSACTION_KEY = "payment/authorizenet_directpost/trans_key";
    const XML_AUTHORIZENET_PAYMENT_ACTION = "payment/authorizenet_directpost/payment_action";

    /** General configs */
    const XML_CREATE_ORDER_FOR_SIGNUP_PATH = 'magenest_subscription/settings_general/create_order_for_signup';
    const XML_GATEWAY_SUPPORT_SUBSCRIPTION_PATH = "magenest_subscription/gateway_support_subscription";
    const XML_IS_LOGIN_CHECKOUT_MANDATORY_PATH = "magenest_subscription/settings_general/login_only";
    const XML_GENERAL_DISPLAY_STYLE_PATH = 'magenest_subscription/settings_general/display_style';
    const XML_IS_LOGGER_ENABLED = 'magenest_subscription/settings_general/debug';

    /** Paypal subscription configs */
    const XML_PAYPAL_ALLOW_CANCEL = "magenest_subscription/settings/allow_cancel";
    const XML_PAYPAL_ALLOW_SUSPEND = "magenest_subscription/settings/allow_suspend";
    const XML_PAYPAL_ALLOW_REACTIVATE = "magenest_subscription/settings/allow_reactivate";
    const XML_PAYPAL_INIT_FAIL_ACTION = "magenest_subscription/settings/init_action";

    /** Authorizenet subscription configs */
    const XML_AUTHORIZENET_ALLOW_CANCEL = "magenest_subscription/settings_authorize/allow_cancel";
    const XML_AUTHORIZENET_USE_REF = "magenest_subscription/settings_authorize/use_ref";

    /**
     * @var String
     */
    protected $_merchantId;

    /**
     * @var String
     */
    protected $_merchantTransactionKey;

    /**
     * @var String
     */
    protected $_paymentAction;

    /**
     * @var String
     */
    protected $_testMode;

    /**
     * @var String
     */
    protected $_useRef;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var \Magenest\Subscription\Logger\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var Authorize\Charge
     */
    protected $_AChargeHelper;

    /**
     * @var Util
     */
    protected $_util;

    /**
     * Data constructor.
     * @param Context $context
     * @param ProductFactory $productFactory
     * @param ProfileFactory $profileFactory
     * @param \Magenest\Subscription\Logger\Logger $logger
     * @param Util $util
     * @param \Magento\Framework\Message\ManagerInterface $message
     * @param Authorize\Charge $charge
     */
    public function __construct(
        Context $context,
        ProductFactory $productFactory,
        ProfileFactory $profileFactory,
        \Magenest\Subscription\Logger\Logger $logger,
        \Magenest\Subscription\Helper\Util $util,
        \Magento\Framework\Message\ManagerInterface $message,
        \Magenest\Subscription\Helper\Authorize\Charge $charge
    ) {
    
        $this->_util = $util;
        $this->_AChargeHelper;
        $this->_messageManager = $message;
        $this->_logger = $logger;
        $this->_productFactory = $productFactory;
        $this->_profileFactory = $profileFactory;
        $this->_AChargeHelper = $charge;
        parent::__construct($context);
        $this->_merchantId = $this->scopeConfig->getValue(self::XML_AUTHORIZENET_MERCHANT_ID);
        $this->_merchantTransactionKey = $this->scopeConfig->getValue(self::XML_AUTHORIZENET_TRANSACTION_KEY);
        $this->_testMode = $this->scopeConfig->getValue(self::XML_AUTHORIZENET_IS_TEST);
        $this->_paymentAction = $this->scopeConfig->getValue(self::XML_AUTHORIZENET_PAYMENT_ACTION);
        $this->_useRef = $this->scopeConfig->getValue(self::XML_AUTHORIZENET_USE_REF);
    }

    /**
     * Return true if running in test mode/sandbox
     * @return bool
     */
    public function getIsTestMode() {
        return $this->_testMode;
    }

    /**
     * Get payment model class
     * @param $code String
     * @return mixed
     */
    public function getPaymentClass($code)
    {
        $class = $this->scopeConfig->getValue($code, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $class;
    }

    /**
     * Allow creating nominal order or not
     * @return mixed
     */
    public function isAllowNominalOrderCreation()
    {
        $isAllowed =  $this->scopeConfig->getValue(self::XML_CREATE_ORDER_FOR_SIGNUP_PATH);
        return $isAllowed;
    }


    public function isCustomerLoginMandatory()
    {
        $isMandatory = $this->scopeConfig->getValue(self::XML_IS_LOGIN_CHECKOUT_MANDATORY_PATH);
        return $isMandatory;
    }


    /**
     * Get online shipping carrier codes
     *
     * @param int|\Magento\Store\Model\Store|null $store
     * @return array
     */
    public function getOnlineGatewayCodes()
    {
        $paymentCodes = [];
        foreach ($this->scopeConfig->getValue(self::XML_GATEWAY_SUPPORT_SUBSCRIPTION_PATH, \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES) as $key => $value) {
            if (isset($value['support_subscription'])) {
                if ($value['support_subscription'] == 1) {
                    $paymentCodes[] = $key;
                }
            }
        }
        return $paymentCodes;
    }

    public function getConfigValue($path)
    {
        return $this->scopeConfig->getValue($path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
