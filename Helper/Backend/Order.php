<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 8/9/17
 * Time: 10:24 AM
 */

namespace Magenest\Subscription\Helper\Backend;

class Order extends \Magento\Backend\Helper\Data
{
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magenest\Subscription\Model\ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var \Magenest\Subscription\Model\RelationshipFactory
     */
    protected $_relationshipFactory;

    /**
     * Order constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Route\Config $routeConfig
     * @param \Magento\Framework\Locale\ResolverInterface $locale
     * @param \Magento\Backend\Model\UrlInterface $backendUrl
     * @param \Magento\Backend\Model\Auth $auth
     * @param \Magento\Backend\App\Area\FrontNameResolver $frontNameResolver
     * @param \Magento\Framework\Math\Random $mathRandom
     * @param \Magenest\Subscription\Model\RelationshipFactory $relationshipFactory
     * @param \Magenest\Subscription\Model\ProfileFactory $profileFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Route\Config $routeConfig,
        \Magento\Framework\Locale\ResolverInterface $locale,
        \Magento\Backend\Model\UrlInterface $backendUrl,
        \Magento\Backend\Model\Auth $auth,
        \Magento\Backend\App\Area\FrontNameResolver $frontNameResolver,
        \Magento\Framework\Math\Random $mathRandom,
        \Magenest\Subscription\Model\RelationshipFactory $relationshipFactory,
        \Magenest\Subscription\Model\ProfileFactory $profileFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory
    ) {
    
        parent::__construct($context, $routeConfig, $locale, $backendUrl, $auth, $frontNameResolver, $mathRandom);
        $this->_orderFactory = $orderFactory;
        $this->_profileFactory = $profileFactory;
        $this->_relationshipFactory = $relationshipFactory;
    }

    /**
     * Get parent order of an order
     *
     * @param $order
     * @return mixed
     */
    public function getParentOrder($order)
    {
        /** @var \Magenest\Subscription\Model\Relationship $relationship */
        $relationship = $this->_relationshipFactory->create();
        $collection = $relationship->getCollection();

        if ($order instanceof \Magento\Sales\Model\Order) {
            $incrementId = $order->getIncrementId();
        } else {
            /** @var \Magento\Sales\Model\Order $orderModel */
            $orderModel = $this->_orderFactory->create();
            $incrementId = $orderModel->load($order)->getIncrementId();
        }

        return $collection->addFieldToFilter('child_order', $incrementId)
            ->getFirstItem()
            ->getData('parent_order');
    }

    /**
     * @param $order
     * @return bool
     */
    public function isChildOrder($order)
    {
        /** @var \Magenest\Subscription\Model\Relationship $relationship */
        $relationship = $this->_relationshipFactory->create();
        $collection = $relationship->getCollection();

        if ($order instanceof \Magento\Sales\Model\Order) {
            $incrementId = $order->getIncrementId();
        } else {
            /** @var \Magento\Sales\Model\Order $orderModel */
            $orderModel = $this->_orderFactory->create();
            $incrementId = $orderModel->load($order)->getIncrementId();
        }

        return !!$collection
            ->addFieldToFilter('child_order', $incrementId)
            ->getFirstItem()
            ->getData();
    }

    /**
     * @param $orderId
     * @return bool
     */
    public function isParentOrder($order)
    {
        /** @var \Magenest\Subscription\Model\Relationship $relationship */
        $relationship = $this->_relationshipFactory->create();
        $collection = $relationship->getCollection();

        if ($order instanceof \Magento\Sales\Model\Order) {
            $incrementId = $order->getIncrementId();
        } else {
            /** @var \Magento\Sales\Model\Order $orderModel */
            $orderModel = $this->_orderFactory->create();
            $incrementId = $orderModel->load($order)->getIncrementId();
        }
        return !!$collection
            ->addFieldToFilter('parent_order', $incrementId)
            ->getFirstItem()
            ->getData();
    }

    /**
     * Get order view url
     * @param $order
     * @return string
     */
    public function getOrderUrl($order)
    {
        if ($order instanceof \Magento\Sales\Model\Order) {
            $order = $order->getId();
        } else {
            /** @var \Magento\Sales\Model\Order $orderModel */
            $orderModel = $this->_orderFactory->create();
            $order = $orderModel->loadByIncrementId($order)->getId();
        }
        return $this->getUrl('sales/order/view', ['order_id' => $order]);
    }
}
