<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 03/10/2017
 * Time: 15:40
 */

namespace Magenest\Subscription\Block\Checkout;

use Magenest\Subscription\Model\ProfileFactory;
use Magento\Sales\Model\Order;

class Success extends \Magento\Checkout\Block\Onepage\Success
{
    /**
     * @var \Magento\Framework\Session\Generic
     */
    protected $subscriptionSession;

    /**
     * @var ProfileFactory \Magenest\Subscription\Model\ProfileFactory
     */
    protected $profileFactory;

    /**
     * Success constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param Order\Config $orderConfig
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Framework\Session\Generic $subscriptionSession
     * @param ProfileFactory $profileFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        Order\Config $orderConfig,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Framework\Session\Generic $subscriptionSession,
        ProfileFactory $profileFactory,
        array $data = []
    ) {
        parent::__construct($context, $checkoutSession, $orderConfig, $httpContext, $data);
        $this->subscriptionSession = $subscriptionSession;
        $this->profileFactory = $profileFactory;
    }

    /**
     * Get profile view url
     *
     * @param $profileId
     * @return string
     */
    public function getViewUrl($profileId)
    {
        return $this->getUrl('subscription/customer/view', ['id' => $profileId]);
    }

    /**
     * Prepare data
     */
    public function getSubscriptionProfiles()
    {
        $profileIds = $this->subscriptionSession->getLastAddedProfileIds();
        if ($profileIds) {
            $profileCollection = $this->profileFactory->create()->getCollection()->addFieldToFilter('id', ['in' => $profileIds]);

            if ($profileCollection->getSize() > 0) {
                return $profileCollection;
            }
        }

        return [];
    }
}
