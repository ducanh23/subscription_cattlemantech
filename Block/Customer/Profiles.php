<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 24/03/2016
 * Time: 01:39
 */

namespace Magenest\Subscription\Block\Customer;

use Magenest\Subscription\Model\ProfileFactory;
use Magento\Catalog\Block\Product\Context;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\App\ObjectManager;

class Profiles extends \Magento\Framework\View\Element\Template
{
    /**
     * @var CurrentCustomer
     */
    protected $_currentCustomer;

    /**
     * @var ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var \Magenest\Subscription\Model\ResourceModel\Profile\Collection
     */
    protected $profiles;

    /**
     * Profiles constructor.
     * @param Context $context
     * @param CurrentCustomer $currentCustomer
     * @param ProfileFactory $profileFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        CurrentCustomer $currentCustomer,
        ProfileFactory $profileFactory,
        array $data
    ) {
    
        $this->_currentCustomer = $currentCustomer;
        $this->_profileFactory = $profileFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param $id
     * @return string
     */
    public function getProfileViewUrl($id)
    {
        return $this->getUrl('subscription/customer/view', [
            'id' => $id
        ]);
    }

    /**
     * @return string
     */
    public function getRefreshUrl()
    {
        return $this->getUrl('subscription/customer/refresh');
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'subscription.profiles.history.pager'
        )
            ->setShowPerPage(true)
            ->setCollection(
                $this->getProfiles()
            );
        $this->setChild('pager', $pager);
        $this->getProfiles()->load();
        return $this;
    }

    /**
     * Get profile list for pagination
     *
     * @return $this
     */
    public function getProfiles()
    {
        $customerId = $this->_currentCustomer->getCustomerId();
        if (!$this->profiles) {
            $this->profiles = $this->_profileFactory->create()->getCollection()->addFieldToSelect(
                '*'
            )->addFieldToFilter('customer_id', $customerId)->setOrder('id', 'desc')->setPageSize(10);
        }
        return $this->profiles;
    }

    /**
     * get the json object to feed javascript component
     * which has following format
     * {2: {123: {order_id, 123}}, [] }
     */
    public function getOrdersForEachProfile($jsonFormat = true)
    {
        $output = [];
        $profilesCollection = $this->getProfiles();

        if ($profilesCollection->getSize() > 0) {
            foreach ($profilesCollection as $profile) {
                $output[$profile->getId()] = [];
                $output[$profile->getId()][] = $profile->getProfileId();
                $sequenceOrders = unserialize($profile->getSequenceOrderIds());

                if (!empty($sequenceOrders)) {
                    foreach ($sequenceOrders as $orderId) {
                        /** @var \Magento\Sales\Model\Order $order */
                        $order = ObjectManager::getInstance()->get('Magento\Sales\Model\Order')->load($orderId);
                        $incrementId = $order->getIncrementId();
                        $createdDate = $order->getCreatedAt();
                        $orderLink = $this->getUrl('sales/order/view', ['order_id' => $orderId]);
                        $status = $order->getStatus();

                        $output[$profile->getId()][] = [
                            'order_link' => $orderLink,
                            'increment_id' => $incrementId,
                            'created_date' => $createdDate,
                            'status' => strtoupper($status),
                            'grand_total' => $order->getOrderCurrencyCode() . ' ' . number_format($order->getGrandTotal(), 2)
                        ];
                    }
                }
            }
        }

        if ($jsonFormat) {
            return json_encode($output);
        } else {
            return $output;
        }
    }
}
