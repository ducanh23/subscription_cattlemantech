<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Block\Customer;

use Magenest\Subscription\Api\SubscriptionManagementInterface;
use Magenest\Subscription\Model\ProfileFactory;
use Magento\Catalog\Block\Product\Context;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class AbstractDetail provides a base implementation for recurring profile
 * @package Magenest\Subscription\Block\Customer
 */
class Detail extends \Magento\Framework\View\Element\Template
{
    /** @var \Magenest\Subscription\Model\ProfileFactory */
    protected $_profileFactory;

    /** @var \Magento\Framework\Registry */
    protected $_coreRegistry;

    /** @var ObjectManagerInterface */
    protected $_objectManager;

    /** @var SubscriptionManagementInterface  */
    protected $_subscriptionManagement;

    /** @var \Magenest\Subscription\Model\Profile */
    protected $profile;

    /** @var String */
    protected $method;

    /**
     * View constructor.
     * @param Context $context
     * @param ProfileFactory $profileFactory
     * @param ObjectManagerInterface $objectManagerInterface
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProfileFactory $profileFactory,
        ObjectManagerInterface $objectManagerInterface,
        SubscriptionManagementInterface $subscriptionManagement,
        $data = []
    ) {
    
        $this->_coreRegistry = $context->getRegistry();
        $this->_profileFactory = $profileFactory;
        $this->_objectManager = $objectManagerInterface;
        $this->_subscriptionManagement = $subscriptionManagement;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getProfileId()
    {
        return $this->_coreRegistry->registry('customer_view_profile_id');
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getProductModel()
    {
        $model = $this->_objectManager->get('\Magento\Catalog\Model\Product');

        return $model;
    }

    /**
     * @param $action
     * @return string
     */
    public function getManageProfileUrl($action)
    {
        $profile = $this->getProfile();
        $profileId = $profile->getProfileId();

        return $this->getUrl(
            'subscription/customer/manage',
            [
                'profile_id' => $profileId,
                'action' => $action,
                'method' => $this->method
            ]
        );
    }

    /**
     * @return \Magenest\Subscription\Model\Profile
     */
    public function getProfile()
    {
        $id = $this->_coreRegistry->registry('customer_view_profile_id');

        $this->profile = $this->_profileFactory->create()->load($id);

        return $this->profile;
    }
}
