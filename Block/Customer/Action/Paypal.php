<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 26/09/2017
 * Time: 14:41
 */

namespace Magenest\Subscription\Block\Customer\Action;

use Magenest\Subscription\Api\SubscriptionManagementInterface;
use Magenest\Subscription\Model\ProfileFactory;
use Magento\Catalog\Block\Product\Context;
use Magento\Framework\ObjectManagerInterface;

class Paypal extends \Magenest\Subscription\Block\Customer\Detail
{
    protected $method = 'paypal_express';

    protected $_template = 'customer/profiles/action/paypal.phtml';

    /**
     * @var \Magenest\Subscription\Helper\Data
     */
    public $helper;

    /**
     * Paypal constructor.
     * @param Context $context
     * @param ProfileFactory $profileFactory
     * @param ObjectManagerInterface $objectManagerInterface
     * @param SubscriptionManagementInterface $subscriptionManagement
     * @param \Magenest\Subscription\Helper\Data $dataHelper
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProfileFactory $profileFactory,
        ObjectManagerInterface $objectManagerInterface,
        SubscriptionManagementInterface $subscriptionManagement,
        \Magenest\Subscription\Helper\Data $dataHelper,
        array $data = []
    ) {
        $this->helper = $dataHelper;
        parent::__construct($context, $profileFactory, $objectManagerInterface, $subscriptionManagement, $data);
    }

    /**
     * @return mixed
     */
    public function getIsCancelAllowed()
    {
        return $this->helper->getConfigValue(\Magenest\Subscription\Helper\Data::XML_PAYPAL_ALLOW_CANCEL);
    }

    /**
     * @return mixed
     */
    public function getIsSuspendAllowed()
    {
        return $this->helper->getConfigValue(\Magenest\Subscription\Helper\Data::XML_PAYPAL_ALLOW_SUSPEND);
    }

    /**
     * @return mixed
     */
    public function getIsReactivateAllowed()
    {
        return $this->helper->getConfigValue(\Magenest\Subscription\Helper\Data::XML_PAYPAL_ALLOW_REACTIVATE);
    }
}
