<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 26/09/2017
 * Time: 15:12
 */

namespace Magenest\Subscription\Block\Customer\Action;

use Magenest\Subscription\Api\SubscriptionManagementInterface;
use Magenest\Subscription\Model\ProfileFactory;
use Magento\Catalog\Block\Product\Context;
use Magento\Framework\ObjectManagerInterface;

class Authorize extends \Magenest\Subscription\Block\Customer\Detail
{
    protected $method = 'authorizenet_directpost';

    protected $_template = 'customer/profiles/action/authorize.phtml';

    /**
     * @var \Magenest\Subscription\Helper\Data
     */
    protected $helper;

    /**
     * Authorize constructor.
     * @param Context $context
     * @param ProfileFactory $profileFactory
     * @param ObjectManagerInterface $objectManagerInterface
     * @param SubscriptionManagementInterface $subscriptionManagement
     * @param \Magenest\Subscription\Helper\Data $helperData
     * @param array $data
     */
    public function __construct(
        Context $context,
        ProfileFactory $profileFactory,
        ObjectManagerInterface $objectManagerInterface,
        SubscriptionManagementInterface $subscriptionManagement,
        \Magenest\Subscription\Helper\Data $helperData,
        array $data = []
    ) {
    
        $this->helper = $helperData;
        parent::__construct($context, $profileFactory, $objectManagerInterface, $subscriptionManagement, $data);
    }

    /**
     * @return mixed
     */
    public function getIsCancelAllowed()
    {
        return $this->helper->getConfigValue(\Magenest\Subscription\Helper\Data::XML_AUTHORIZENET_ALLOW_CANCEL);
    }
}
