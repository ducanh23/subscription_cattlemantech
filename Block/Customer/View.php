<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 29/03/2016
 * Time: 02:50
 */

namespace Magenest\Subscription\Block\Customer;

/**
 * Class View is used to display information about recurring profiles in customer dashboard
 * @package Magenest\Subscription\Block\Customer
 */
class View extends Detail
{
    /**
     * Render field based on value
     *
     * @param $value
     * @return string
     */
    public function renderField($value)
    {
        if ($value == '' || $value == -1 || $value == null) {
            return 'N/A';
        } else {
            return $value;
        }
    }

    /**
     * Preparing global layout
     *
     * You can redefine this method in child classes for changing layout
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->getProfile();
        $paymentCode = $this->profile->getMethodCode();
        $methodInstance = $this->_subscriptionManagement->getPaymentMethodInstance($paymentCode);
        $actionBlock = $methodInstance->getActionBlockType();
        $nameInLayout = $this->getNameInLayout();

        $this->setChild(
            'action_info',
            $this->getLayout()->addBlock(
                $actionBlock,
                $this->getNameInLayout() . '.action_info',
                $nameInLayout
            )->setData(
                [
                    'id' => 'product_option_<%- data.option_id %>_price_type',
                    'class' => 'select product-option-price-type',
                ]
            )
        );
        // $this->setChild('action_info', $actionBlock);

        return parent::_prepareLayout();
    }
}
