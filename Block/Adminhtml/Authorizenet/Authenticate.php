<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */

namespace Magenest\Subscription\Block\Adminhtml\Authorizenet;

use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Authenticate
 * Backend Authorizenet API check renderer
 * @package Magenest\Subscription\Block\Adminhtml\Authorizenet
 */
class Authenticate extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var \Magenest\Subscription\Helper\Authorize\Authenticate
     */
    protected $_helper;

    /**
     * @var \Magenest\Subscription\Helper\Data
     */
    protected $_dataHelper;

    /**
     * Authenticate constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magenest\Subscription\Helper\Authorize\Authenticate $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magenest\Subscription\Helper\Authorize\Authenticate $helper,
        \Magenest\Subscription\Helper\Data $dataHelper,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_dataHelper = $dataHelper;
        $this->_helper = $helper;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $isLive = $this->_scopeConfig->getValue($this->_dataHelper->getIsTestMode());
        $request = $this->_helper->getAuthenticateTestRequest();
        $response = $this->_helper->executeRequest($request, $isLive);
        if (isset($response)) {
            if ($response->getMessages()->getResultCode() == \Magenest\Subscription\Helper\Authorize\Authenticate::RESPONSE_OK) {
                $return = "<font color=\"green\">&#10004 Ok</font>";
            } else {
                $text = $response->getMessages()->getMessage()[0]->getText();
                $return = "<font color=\"red\">&#10008 $text</font>";
            }
        } else {
            $return = "<font color=\"red\">&#10008 No response</font>";
        }
        return $return;
    }
}
