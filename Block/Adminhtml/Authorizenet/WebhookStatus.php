<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Block\Adminhtml\Authorizenet;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class WebhookStatus
 * Backend Authorizenet webhook status check renderer
 * @package Magenest\Subscription\Block\Adminhtml\Authorizenet
 */
class WebhookStatus extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var \Magenest\Subscription\Helper\Authorize\Webhook
     */
    protected $_helper;

    /**
     * Authenticate constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magenest\Subscription\Helper\Authorize\Webhook $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magenest\Subscription\Helper\Authorize\Webhook $helper,
        array $data = []
    ) {
    
        parent::__construct($context, $data);
        $this->_helper = $helper;
    }

    /**
     * @return \Magenest\Subscription\Helper\Authorize\Webhook
     */
    public function getHelper()
    {
        return $this->_helper;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        try {
            $webhookStatus = $this->_helper->checkValidWebhooks();
        } catch (LocalizedException $e) {
            $webhookStatus = $e->getMessage();
        }
        if ($webhookStatus !== true) {
            $this->setData('message', $webhookStatus);
            $this->setTemplate('webhooks/authorize/add.phtml');
            return $this->_toHtml();
        } else {
            $return = "<font color=\"green\">&#10004 Ok</font>";
        }
        return $return;
    }
}
