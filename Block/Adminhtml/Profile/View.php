<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 25/03/2016
 * Time: 23:41
 */

namespace Magenest\Subscription\Block\Adminhtml\Profile;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Form\Container as FormContainer;
use Magento\Framework\Registry;

/**
 * Class View
 * @package Magenest\Subscription\Block\Adminhtml\Profile
 */
class View extends FormContainer
{
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * View constructor.
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        $data = []
    ) {
    
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        $model = $this->_coreRegistry->registry('subscription_profile_model');

        return __("View Profile '%1'", $this->escapeHtml($model->getProfileId()));
    }

    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Magenest_Subscription';
        $this->_controller = 'adminhtml_profile';
        parent::_construct();

        $this->buttonList->remove('delete');
        $this->buttonList->remove('reset');
        $this->buttonList->remove('save');

        $this->_mode = 'view';
    }
}
