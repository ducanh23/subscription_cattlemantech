<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 25/03/2016
 * Time: 23:37
 */
namespace Magenest\Subscription\Block\Adminhtml\Profile\View;

/**
 * Class Tabs
 * @package Magenest\Subscription\Block\Adminhtml\Profile\View
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('profile_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Recurring Profile Information'));
    }
}
