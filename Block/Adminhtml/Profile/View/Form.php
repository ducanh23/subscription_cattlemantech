<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 26/03/2016
 * Time: 00:11
 */
namespace Magenest\Subscription\Block\Adminhtml\Profile\View;

/**
 * Class Form
 * @package Magenest\Subscription\Block\Adminhtml\Profile\View
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * Prepare backend form
     * @return $this
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            ['data' =>
                [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post',
                    'enctype' => 'multipart/form-data'
                ]
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
