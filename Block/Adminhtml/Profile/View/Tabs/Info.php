<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 26/03/2016
 * Time: 00:12
 */
namespace Magenest\Subscription\Block\Adminhtml\Profile\View\Tabs;

use Magento\Framework\ObjectManagerInterface;

/**
 * Class Info
 * @package Magenest\Subscription\Block\Adminhtml\Profile\View\Tabs
 */
class Info extends \Magento\Sales\Block\Adminhtml\Order\AbstractOrder implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * Info constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Helper\Admin $adminHelper
     * @param ObjectManagerInterface $interface
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Sales\Helper\Admin $adminHelper,
        ObjectManagerInterface $interface,
        array $data
    ) {
    
        $this->_objectManager = $interface;
        parent::__construct($context, $registry, $adminHelper, $data);
    }

    /**
     * @return \Magenest\Subscription\Model\Profile
     */
    public function getProfile()
    {
        /** @var \Magenest\Subscription\Model\Profile $profile */
        $profile = $this->_coreRegistry->registry('subscription_profile_model');

        return $profile;
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getProductModel()
    {
        $model = $this->_objectManager->get('\Magento\Catalog\Model\Product');

        return $model;
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('General Profile Information');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('General Profile Information');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @param $value
     * @return string
     */
    public function renderField($value)
    {
        if ($value == '' || $value == -1 || $value == null) {
            return 'N/A';
        } else {
            return $value;
        }
    }
}
