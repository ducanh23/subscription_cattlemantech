<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 26/03/2016
 * Time: 10:08
 */
namespace Magenest\Subscription\Block\Adminhtml\Profile\View\Tabs;

use Magento\Backend\Block\Widget\Context;
use Magento\Backend\Block\Widget\Grid\Container;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Registry;

/**
 * Class RelatedOrder
 * @package Magenest\Subscription\Block\Adminhtml\Profile\View\Tabs
 */
class RelatedOrder extends Container implements TabInterface
{
    /**
     * @var Registry|null
     */
    protected $_coreRegistry = null;

    /**
     * RelatedOrder constructor.
     * @param Context $context
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
    
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __("Related Order");
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __("Related Order");
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * Constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->buttonList->remove('add');
        $this->_blockGroup = 'Magenest_Subscription';
        $this->_controller = 'adminhtml_profile_view_tabs_relatedOrder';
    }
}
