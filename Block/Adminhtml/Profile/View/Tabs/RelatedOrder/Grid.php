<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 26/03/2016
 * Time: 10:13
 */
namespace Magenest\Subscription\Block\Adminhtml\Profile\View\Tabs\RelatedOrder;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Framework\Registry;
use Magento\Sales\Model\OrderFactory;

/**
 * Class Grid
 * @package Magenest\Subscription\Block\Adminhtml\Profile\View\Tabs\RelatedOrder
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var Registry
     */
    protected $_coreRegistry;

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * Grid constructor.
     * @param Context $context
     * @param Data $backendHelper
     * @param Registry $registry
     * @param OrderFactory $orderFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data $backendHelper,
        Registry $registry,
        OrderFactory $orderFactory,
        $data = []
    ) {
    
        $this->_coreRegistry = $registry;
        $this->_orderFactory = $orderFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl(
            'sales/order/view',
            ['order_id' => $row->getEntityId()]
        );
    }

    /**
     * Constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('postGrid');
        $this->setDefaultSort('order_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('post_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $profile = $this->_coreRegistry->registry('subscription_profile_model');
        $orderId = $profile->getOrderId();
        $sequenceOrderIds = $profile->getData('sequence_order_ids');

        $ids = [];
        if ($sequenceOrderIds) {
            $sequenceOrderIds = unserialize($sequenceOrderIds);
            $ids = $sequenceOrderIds;
        }

        array_unshift($ids, $orderId);

        $collection = $this->_orderFactory->create()->getCollection()->addFieldToFilter('entity_id', $ids);
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'increment_id',
            [
                'header' => __('ID'),
                'type' => 'text',
                'index' => 'increment_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );
        $this->addColumn(
            'created_at',
            [
                'header' => __('Purchased Date'),
                'index' => 'created_at',
                'type' => 'datetime'
            ]
        );
        $this->addColumn(
            'billing_name',
            [
                'header' => __('Bill-to Name'),
                'index' => 'created_at',
                'type' => 'text'
            ]
        );
        $this->addColumn(
            'shipping_name',
            [
                'header' => __('Ship-to Name'),
                'index' => 'created_at',
                'type' => 'text'
            ]
        );
        $this->addColumn(
            'base_grand_total',
            [
                'header' => __('Grand Total (Base)'),
                'index' => 'base_grand_total',
                'type' => 'text'
            ]
        );
        $this->addColumn(
            'grand_total',
            [
                'header' => __('Grand Total (Purchased)'),
                'index' => 'base_grand_total',
                'type' => 'text'
            ]
        );
        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'base_grand_total',
                'type' => 'text'
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }
}
