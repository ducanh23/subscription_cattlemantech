<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 15/04/2016
 * Time: 10:47
 */

namespace Magenest\Subscription\Block\Adminhtml\Template\Edit;

/**
 * Class Tabs
 * @package Magenest\Subscription\Block\Adminhtml\Template\Edit
 */
class Tabs extends \Magento\Backend\Block\Widget\Tabs
{
    /**
     * Constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('template_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Subscription Template Configuration'));
    }
}
