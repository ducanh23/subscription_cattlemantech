<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 15/04/2016
 * Time: 10:47
 */

namespace Magenest\Subscription\Block\Adminhtml\Template\Edit;

/**
 * Class Form
 * @package Magenest\Subscription\Block\Adminhtml\Template\Edit
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $form = $this->_formFactory->create(
            ['data' =>
                [
                    'id' => 'edit_form',
                    'action' => $this->getData('action'),
                    'method' => 'post'
                ]
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
