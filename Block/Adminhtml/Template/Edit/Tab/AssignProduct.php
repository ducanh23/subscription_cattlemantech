<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 23/04/2016
 * Time: 20:59
 */

namespace Magenest\Subscription\Block\Adminhtml\Template\Edit\Tab;

use Magenest\Subscription\Model\TemplateFactory;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class AssignProduct extends \Magento\Backend\Block\Template implements TabInterface
{
    /**
     * @var string
     */
    protected $_template = 'option/edit/assign_products.phtml';

    /**
     * @var TemplateFactory
     */
    protected $_templateFactory;

    /**
     * AssignProduct constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param TemplateFactory $templateFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        TemplateFactory $templateFactory,
        array $data = []
    ) {
    
        $this->_templateFactory = $templateFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getAssignedProducts()
    {
        $model = $this->_templateFactory->create();
        $id = $this->getRequest()->getParam('id');
        $model = $model->load($id);

        $productIds = $model->getProductId();
        if ($productIds) {
            return unserialize($productIds);
        } else {
            return [];
        }
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Assign Product');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Assign Product');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }
}
