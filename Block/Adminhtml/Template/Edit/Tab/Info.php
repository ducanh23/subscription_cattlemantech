<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 15/04/2016
 * Time: 10:53
 */

namespace Magenest\Subscription\Block\Adminhtml\Template\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Form\Renderer\Fieldset;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Info extends Generic implements TabInterface
{
    /**
     * @var Fieldset
     */
    protected $_rendererFieldset;

    /**
     * Info constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param Fieldset $fieldset
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        Fieldset $fieldset,
        array $data
    ) {
    
        $this->_rendererFieldset = $fieldset;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Subscription Option');
    }

    /**
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Subscription Option');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('subscription_template_edit');
        if ($model->getValue()) {
            $value = unserialize($model->getValue());
            $model->setData('template_name', $value['optionName']);
            $model->setData('period_unit', $value['selectedUnit']);
            $model->setData('billing_frequency', $value['frequency']);
            $model->setData('max_billing_cycle', $value['maxCycles']);
            $model->setData('trial_amount', $value['trialAmount']);
            $model->setData('trial_unit', $value['trialUnit']);
            $model->setData('trial_billing_frequency', $value['trialFrequency']);
            $model->setData('trial_billing_max_cycle', $value['trialMaxCycles']);
            $model->setData('initial_fee', $value['initFee']);
        }
        $form = $this->_formFactory->create();
        $form->setHtmlIdPrefix('template_');

        $fieldset = $form->addFieldset(
            'general_fieldset',
            [
                'legend' => __('Main Period'),
                'class' => 'fieldset-wide'
            ]
        );

        if ($model->getId()) {
            $fieldset->addField(
                'id',
                'hidden',
                ['name' => 'id']
            );
        }

        $fieldset->addField(
            'template_name',
            'text',
            [
                'name' => 'template_name',
                'label' => __('Option Name'),
                'title' => __('Option Name'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'period_unit',
            'select',
            [
                'name' => 'period_unit',
                'label' => __('Period Unit'),
                'title' => __('Period Unit'),
                'required' => true,
                'options' => [
                    'day' => 'Day',
                    'week' => 'Week',
                    'semimonth' => 'Semi-Month',
                    'month' => 'Month',
                    'year' => 'Year'
                ]
            ]
        );

        $fieldset->addField(
            'billing_frequency',
            'text',
            [
                'name' => 'billing_frequency',
                'label' => __('Billing Frequency'),
                'title' => __('Billing Frequency'),
                'required' => true
            ]
        );

        $fieldset->addField(
            'max_billing_cycle',
            'text',
            [
                'name' => 'billing_max_cycles',
                'label' => __('Maximum Billing Cycles'),
                'title' => __('Maximum Billing Cycles'),
                'required' => true
            ]
        );

        $trialFieldset = $form->addFieldset(
            'trial_fieldset',
            [
                'legend' => __('Trial Period'),
                'class' => 'fieldset-wide'
            ]
        );

        //Trial Field
        $trialFieldset->addField(
            'trial_amount',
            'text',
            [
                'name' => 'trial_amount',
                'label' => __('Amount'),
                'title' => __('Amount'),
            ]
        );

        $trialFieldset->addField(
            'trial_unit',
            'select',
            [
                'name' => 'trial_unit',
                'label' => __('Period Unit'),
                'title' => __('Period Unit'),
                'options' => [
                    'day' => 'Day',
                    'week' => 'Week',
                    'semimonth' => 'Semi-Month',
                    'month' => 'Month',
                    'year' => 'Year'
                ]
            ]
        );

        $trialFieldset->addField(
            'trial_billing_frequency',
            'text',
            [
                'name' => 'trial_billing_frequency',
                'label' => __('Billing Frequency'),
                'title' => __('Billing Frequency'),
            ]
        );

        $trialFieldset->addField(
            'trial_billing_max_cycle',
            'text',
            [
                'name' => 'trial_billing_max_cycle',
                'label' => __('Maximum Billing Cycles'),
                'title' => __('Maximum Billing Cycles'),
            ]
        );

        //Initial Fee
        $initFieldset = $form->addFieldset(
            'init_fieldset',
            [
                'legend' => __('Initial Fee (available for Paypal Express Checkout)'),
                'class' => 'fieldset-wide'
            ]
        );

        $initFieldset->addField(
            'initial_fee',
            'text',
            [
                'name' => 'initial_fee',
                'label' => __('Amount'),
                'title' => __('Amount'),
            ]
        );

        $form->setValues($model->getData());
        $this->setForm($form);
        return parent::_prepareForm();
    }
}
