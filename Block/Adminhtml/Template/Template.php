<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 15/04/2016
 * Time: 09:32
 */

namespace Magenest\Subscription\Block\Adminhtml\Template;

/**
 * Class Template
 * @package Magenest\Subscription\Block\Adminhtml\Template
 */
class Template extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * Constructor
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Magenest_Subscription';

        parent::_construct();
    }

    /**
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        $this->setChild(
            'grid',
            $this->getLayout()->createBlock('Magenest\Subscription\Block\Adminhtml\Template\Grid', 'subscription.template.grid')
        );
        return parent::_prepareLayout();
    }
}
