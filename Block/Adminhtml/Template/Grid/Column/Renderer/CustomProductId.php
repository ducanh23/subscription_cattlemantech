<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 11/10/2016
 * Time: 01:17
 */

namespace Magenest\Subscription\Block\Adminhtml\Template\Grid\Column\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\DataObject;

/**
 * Class CustomProductId
 * @package Magenest\Subscription\Block\Adminhtml\Template\Grid\Column\Renderer
 */
class CustomProductId extends AbstractRenderer
{
    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * CustomProductId constructor.
     * @param \Magento\Backend\Block\Context $context
     * @param ProductFactory $productFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Context $context,
        ProductFactory $productFactory,
        array $data = []
    ) {
    
        $this->_productFactory = $productFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row)
    {
        $output = '<div class="template-grid-noproduct">No product assigned</div>';
        $productIds = $row->getData('product_id');
        if ($productIds) {
            $productIds = unserialize($productIds);
            $output = "";

            foreach ($productIds as $id) {
                $productName = $this->_productFactory->create()->load($id)->getName();
                $output .= '<div class="template-grid-product">' . $productName . '</div>';
            }
        }

        return $output;
    }
}
