<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 15/04/2016
 * Time: 09:33
 */

namespace Magenest\Subscription\Block\Adminhtml\Template;

use Magenest\Subscription\Model\TemplateFactory;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data as HelperData;

/**
 * Class Grid
 * @package Magenest\Subscription\Block\Adminhtml\Template
 */
class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var TemplateFactory
     */
    protected $_templateFactory;

    /**
     * Grid constructor.
     * @param Context $context
     * @param HelperData $backendHelper
     * @param TemplateFactory $templateFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        HelperData $backendHelper,
        TemplateFactory $templateFactory,
        array $data = []
    ) {
    
        $this->_templateFactory = $templateFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @param \Magento\Catalog\Model\Product|\Magento\Framework\DataObject $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('subscription/*/edit', ['id' => $row->getId()]);
    }

    /**
     * Constructor
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('ruleGrid');
        $this->setDefaultSort('option_name');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->_templateFactory->create()->getCollection();
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('Option ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'option_name',
            [
                'header' => __('Option Name'),
                'type' => 'text',
                'index' => 'option_name'
            ]
        );

        $this->addColumn(
            'product_id',
            [
                'header' => __('Assign for Products'),
                'type' => 'text',
                'index' => 'product_id',
                'filter' => 0,
                'sortable' => 0,
                'renderer' => 'Magenest\Subscription\Block\Adminhtml\Template\Grid\Column\Renderer\CustomProductId',
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('Template');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('subscription/*/massDelete'),
                'confirm' => __('Do you want to delete this option?')
            ]
        );

        return $this;
    }
}
