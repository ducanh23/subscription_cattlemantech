<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Block\Adminhtml\Paypal;

use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Authenticate
 * Backend Paypal API check renderer
 * @package Magenest\Subscription\Block\Adminhtml\Paypal
 */
class Authenticate extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var \Magenest\Subscription\Model\Paypal\Api\Nvp\Request\GetPalDetails
     */
    protected $_nvp;

    /**
     * Authenticate constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magenest\Subscription\Model\Paypal\Api\Nvp\Request\GetPalDetails $api
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magenest\Subscription\Model\Paypal\Api\Nvp\Request\GetPalDetails $api,
        array $data = []
    ) {
    
        parent::__construct($context, $data);
        $this->_nvp = $api;
    }

    /**
     * @param AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        try {
            $this->_nvp->process();
            return "<font color=\"green\">&#10004 Ok</font>";
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return "<font color=\"red\">&#10008 " . $e->getMessage() . "</font>";
        }
    }
}
