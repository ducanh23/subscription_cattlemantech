<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Block\Adminhtml\Sales\Order\View;

use Magento\Backend\Block\Template;

class Info extends Template
{
    /**
     * @var \Magenest\Subscription\Model\ProfileFactory
     */
    protected $_profileFactory;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magenest\Subscription\Model\RelationshipFactory
     */
    protected $_relationshipFactory;

    /**
     * Info constructor.
     * @param \Magenest\Subscription\Model\RelationshipFactory $relationshipFactory
     * @param \Magenest\Subscription\Model\ProfileFactory $profileFactory
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magenest\Subscription\Model\RelationshipFactory $relationshipFactory,
        \Magenest\Subscription\Model\ProfileFactory $profileFactory,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        Template\Context $context,
        array $data = []
    ) {
    
        $this->_relationshipFactory = $relationshipFactory;
        $this->_profileFactory = $profileFactory;
        $this->_orderFactory = $orderFactory;
        parent::__construct($context, $data);
    }

    /**
     * @param string $incrementId
     * @return string
     */
    public function getOrderUrl($incrementId)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $this->_orderFactory->create();
        $order->loadByIncrementId($incrementId);
        return $this->getUrl('sales/order/view', ['order_id' => $order->getId()]);
    }

    /**
     * @return array|bool
     */
    public function isParentOrder()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        if ($orderId) {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $this->_orderFactory->create()->load($orderId);
            $incrementId = $order->getIncrementId();
            /** @var \Magenest\Subscription\Model\Profile $profile */
            $profile = $this->_profileFactory->create();
            $profileCollection = $profile->getCollection();
            $data = $profileCollection
                ->addFieldToFilter('order_id', $incrementId)
                ->addFieldToSelect('profile_id')
                ->addFieldToSelect('order_id')
                ->setOrder($profile->getIdFieldName(), $profileCollection::SORT_ORDER_DESC)
                ->getData();
            if ($data) {
                return $data;
            }
        }
        return false;
    }

    /**
     * Get profile url by profile id
     * @param string $profileId
     * @return string
     */
    public function getProfileUrl($profileId)
    {
        /** @var \Magenest\Subscription\Model\Profile $profile */
        $profile = $this->_profileFactory->create();
        $id = $profile->loadByProfileId($profileId)->getData($profile->getIdFieldName());
        return $this->getUrl('subscription/profile/view', ['id' => $id]);
    }

    /**
     * Get parent order if available, if not return false
     * @return bool|\Magento\Sales\Model\Order
     */
    public function getParentOrder()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        if ($orderId) {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $this->_orderFactory->create()->load($orderId);
            $incrementId = $order->getIncrementId();

            /** @var \Magenest\Subscription\Model\Relationship $relationship */
            $relationship = $this->_relationshipFactory->create();
            $collection = $relationship->getCollection();

            return $collection->addFieldToFilter('child_order', $incrementId)
                ->getFirstItem()
                ->getData();
        }
        return false;
    }

    /**
     * Get associated profiles
     * @return array
     */
    public function getAssociatedProfiles()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        if ($orderId) {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $this->_orderFactory->create()->load($orderId);
            $incrementId = $order->getIncrementId();

            /** @var \Magenest\Subscription\Model\Relationship $relationship */
            $relationship = $this->_relationshipFactory->create();
            $collection = $relationship->getCollection();

            return $collection
                ->addFieldToFilter('parent_order', $incrementId)
                ->getData();
        }
        return [];
    }
}
