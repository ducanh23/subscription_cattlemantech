<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/03/2016
 * Time: 11:41
 */

namespace Magenest\Subscription\Block\Catalog\Product;

use Magenest\Subscription\Model\AttributeFactory;
use Magenest\Subscription\Model\TemplateFactory;
use Magento\Catalog\Block\Product\Context;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magenest\Subscription\Helper\Util;
use Magento\Quote\Model\QuoteRepository;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;

/**
 * Product Subscription part block
 */
class View extends \Magento\Catalog\Block\Product\AbstractProduct
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Magenest\Subscription\Model\AttributeFactory
     */
    protected $_attributeFactory;

    /**
     * @var \Magenest\Subscription\Model\TemplateFactory
     */
    protected $_templateFactory;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $currentCustomer;

    /**
     * @var Util
     */
    protected $util;

    /**
     * View constructor.
     * @param AttributeFactory $attributeFactory
     * @param TemplateFactory $templateFactory
     * @param DateTime $dateTime
     * @param Context $context
     * @param PricingHelper $pricingHelper
     * @param array $data
     */
    public function __construct(
        AttributeFactory $attributeFactory,
        TemplateFactory $templateFactory,
        DateTime $dateTime,
        Context $context,
        PricingHelper $pricingHelper,
        CheckoutSession $checkoutSession,
        CustomerSession $customerSession,
        QuoteRepository $quoteRepository,
        Util $util,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        array $data = []
    ) {
    
        parent::__construct($context, $data);
        $this->currentCustomer = $currentCustomer;

        $this->_attributeFactory = $attributeFactory;
        $this->_templateFactory = $templateFactory;
        $this->_date = $dateTime;
        $this->helper = $pricingHelper;
        $this->util = $util;
        $this->quoteRepository = $quoteRepository;
        $this->checkoutSession = $checkoutSession;
        $this->customerSession = $customerSession;
    }

    /**
     * Whether customer could subscribe to this product
     * @return string
     */
    public function getIsSubscriptionProduct()
    {
        $product = $this->_coreRegistry->registry('current_product');
        $value = $product->getData('enable_subscription');

        return $value;
    }

    /**
     * get the current date time in appropriate format
     * @return string
     */
    public function getCurrentDate()
    {
        return $this->_date->gmtDate('d/m/Y');
    }

    /**
     * Whether customer is allowed to choose the start date of subscription
     * @return string
     */
    public function getCanUserDefineStartDate()
    {
        $product = $this->_coreRegistry->registry('current_product');
        $value = $product->getData('customer_define_startdate');

        return $value;
    }

    /**
     * get the configured data for the javascription component
     * @return string
     */
    public function getJsLayout()
    {
        $productId = $this->getProduct()->getId();
        $this->jsLayout['components']['subscription']['config']['productId'] = $productId;
        $this->jsLayout['components']['subscription']['config']['rawData'] = $this->getBillingOptions();
        $this->jsLayout['components']['subscription']['config']['displayStyle'] = $this->getDisplayStyle();
        $this->jsLayout['components']['subscription']['config']['oneTimePurchasedLabel'] = __('One time purchased');
        $this->jsLayout['components']['subscription']['config']['loadingImgUrl'] = $this->getViewFileUrl('Magenest_Subscription::images/spinner.gif');
        $this->jsLayout['components']['subscription']['config']['loadingTxt'] =  __('Loading');
        return json_encode($this->jsLayout);
    }

    /**
     * get the subscription's option
     * @return array|string
     */
    public function getBillingOptions()
    {
        $currency = \Magento\Framework\App\ObjectManager::getInstance()->create('\Magento\Directory\Model\Currency');
        $currencySymbol = $currency->getCurrencySymbol();
        $product = $this->_coreRegistry->registry('current_product');
        $productId = $product->getId();
        $optionList = [];

        $templateCollection = $this->_templateFactory->create()->getCollection();
        foreach ($templateCollection as $temp) {
            $tempProductIds = unserialize($temp->getData('product_id'));
            foreach ($tempProductIds as $tempId) {
                if ($productId == $tempId) {
                    $tempOptions = unserialize($temp->getData('value'));
                    $tempOptions['trialAmount'] = $currencySymbol . $tempOptions['trialAmount'];
                    $tempOptions['initFee'] = $currencySymbol . $tempOptions['initFee'];
                    array_push($optionList, $tempOptions);
                }
            }
        }

        /** @var \Magenest\Subscription\Model\Attribute $model */
        $model = $this->_attributeFactory->create();
        $attr = $model->getCollection()->addFieldToFilter('entity_id', $productId)->getFirstItem();
        if ($attr->getId()) {
            $options = unserialize($attr->getValue());
            if (is_array($options)) {
                foreach ($options as $key => $option) {
                    foreach ($option as $newKey => $value) {
                        if ($newKey == 'trialAmount' || $newKey == 'initFee') {
                            $options[$key][$newKey] = $currencySymbol . $value;
                        }
                    }
                }
            }
            for ($i = 0; $i < sizeof($options); $i++) {
                array_push($optionList, $options[$i]);
            }
        }

        if (empty($optionList)) {
            return '';
        } else {
            return $optionList;
        }
    }

    /**
     * Administrator can configure to display the subscription's option as radio buttons or a dropdown in backend area.
     * This method to get the configured value
     * @return string
     */
    public function getDisplayStyle()
    {
        $style = $this->_scopeConfig->getValue(
            \Magenest\Subscription\Helper\Data::XML_GENERAL_DISPLAY_STYLE_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        if (!$style) {
            $style = 'dropdown';
        }
        return $style;
    }

    /** get the start date of subscription that user already selected
     * @return string
     */
    public function getEditedStartDate()
    {
        $quote = $this->util->getQuote();

        if ($quote) {
            $quoteId = $quote->getId();
            $productId = $this->getRequest()->getParam('product_id');
            if ($productId) {
                $id = $this->getRequest()->getParam('id');

                if ($id) {
                    $quote = $this->quoteRepository->getActive($quoteId);
                    $quote->getAllItems();
                    $item = $quote->getItemById($id);

                    if (is_object($item)) {
                        $buyRequest = $item->getBuyRequest();
                        $startDate = $buyRequest->getData('subscription_start_date');
                        $startDateObj = \DateTime::createFromFormat('d/m/Y', $startDate);
                        return $startDateObj->format('d/m/Y');
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get the selected option to edit product in the cart page
     * @return string
     */
    public function getCurrentItemOptions()
    {
        $quote = $this->util->getQuote();

        if ($quote) {
            $quoteId = $quote->getId();
            $productId = $this->getRequest()->getParam('product_id');

            if ($productId) {
                $id = $this->getRequest()->getParam('id');

                if ($id) {
                    $quote = $this->quoteRepository->getActive($quoteId);
                    $quote->getAllItems();
                    $item = $quote->getItemById($id);

                    if (is_object($item)) {
                        $buyRequest = $item->getBuyRequest();
                        $options = $buyRequest->getData('additional_options');
                        return $options;
                    }
                }
            }
        }
        return null;
    }
}
