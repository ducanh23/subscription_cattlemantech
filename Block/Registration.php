<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 10/01/2017
 * Time: 16:09
 */

namespace Magenest\Subscription\Block;

class Registration extends \Magento\Checkout\Block\Registration
{
    /**
     * @return string
     */
    public function toHtml()
    {
        if ($this->customerSession->isLoggedIn()
            || !$this->registration->isAllowed()
            || !$this->accountManagement->isEmailAvailable($this->getEmailAddress())

        ) {
            return '';
        }
        return parent::toHtml();
    }
}
