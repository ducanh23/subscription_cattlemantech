<?php
/**
 * Created by PhpStorm.
 * User: ducanh
 * Date: 30/05/2018
 * Time: 14:56
 */
namespace Magenest\Subscription\Block\Order;

use \Magento\Checkout\Model\Session as CheckoutSession;

class History extends \Magento\Sales\Block\Order\History
{
    protected $quoteItemCollectionFactory;
    protected $_storeManager;
    protected $checkoutSession;
    protected $quoteFactory;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Quote\Model\ResourceModel\Quote\Item\Collection\Interceptor $quoteItemCollectionFactory,
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        CheckoutSession $checkoutSession,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Sales\Model\Order\Config $orderConfig,
        array $data = [])
    {
        $this->_storeManager = $storeManager;
        $this->quoteItemCollectionFactory = $quoteItemCollectionFactory;
        $this->checkoutSession = $checkoutSession;
        $this->quoteFactory = $quoteFactory;

        parent::__construct($context, $orderCollectionFactory, $customerSession, $orderConfig, $data);
    }

    public function getQuoteItemx()
    {
//        $collection = $this->quoteItemCollectionFactory->create();
//        return $collection;
        return $this->quoteItemCollectionFactory;
    }

    public function getCheckoutSession()
    {
        return $this->checkoutSession;

    }

    public function getQuoteByCustomerId($customerId)
    {
        $quote = $this->quoteFactory->create()->getCollection()->addFieldToFilter('customer_id', $customerId);
        return $quote;

    }
}
