<?php
/**
 * Created by PhpStorm.
 * User: ducanh
 * Date: 01/06/2018
 * Time: 14:15
 */
namespace Magenest\Subscription\Block\Sales\Order;

class Fee extends \Magento\Framework\View\Element\Template
{
    /**
     *
     * @var \Magento\Tax\Model\Config
     */
    protected $_config;

    /**
     * @var Order
     */
    protected $_order;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_source;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Tax\Model\Config $taxConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Tax\Model\Config $taxConfig,
        array $data = []
    )
    {
        $this->_config = $taxConfig;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return bool
     */
    public function displayFullSummary()
    {
        return true;
    }

    /**
     *
     * @return \Magento\Framework\DataObject
     */
    public function getSource()
    {
        return $this->_source;
    }

    public function getStore()
    {
        return $this->_order->getStore();
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * @return array
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * @return array
     */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

    /**
     *
     * @return \Magento\Tax\Block\Sales\Order\Tax
     */
    public function initTotals()
    {

        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();

        $store = $this->getStore();
        foreach ($this->_order->getItems() as $item) {
            if (isset($item->getData('product_options')["additional_options"])) {
                $initFee = $item->getData('product_options')["additional_options"][0]["initial_fee"];
            }else{
                $initFee = 0;
            }
            break;
        }
        $fee = new \Magento\Framework\DataObject(
            [
                'code' => 'fee',
                'strong' => false,
                'value' => $initFee,
                //'value' => $this->_source->getFee(),
                'label' => __('Initial Fee and VAT'),
            ]
        );

        $parent->addTotal($fee, 'fee');


        return $this;
    }
}
