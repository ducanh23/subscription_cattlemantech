<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 20/03/2016
 * Time: 23:01
 */

namespace Magenest\Subscription\Block\Express\Review;

use Magento\Checkout\Block\Cart\Totals;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\View\Element\Template\Context;

class Details extends Totals
{
    /**
     * @var \Magento\Quote\Model\Quote\Address
     */
    protected $_address;

    /**
     * Details constructor.
     * @param Context $context
     * @param CustomerSession $customerSession
     * @param CheckoutSession $checkoutSession
     * @param \Magento\Sales\Model\Config $salesConfig
     * @param array $layoutProcessors
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        CheckoutSession $checkoutSession,
        \Magento\Sales\Model\Config $salesConfig,
        array $layoutProcessors,
        array $data
    ) {
    
        parent::__construct($context, $customerSession, $checkoutSession, $salesConfig, $layoutProcessors, $data);
    }

    /**
     * @return \Magento\Quote\Model\Quote\Address
     */
    public function getAddress()
    {
        if (empty($this->_address)) {
            $this->_address = $this->getQuote()->getShippingAddress();
        }
        return $this->_address;
    }

    /**
     * @return \Magento\Quote\Model\Quote\Address\Total[]
     */
    public function getTotals()
    {
        return $this->getQuote()->getTotals();
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct()
    {
        $quote = $this->getQuote();
        /** @var \Magento\Quote\Model\Quote\Item $item */
        $item = $quote->getItemsCollection()->getFirstItem();

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $item->getProduct();

        return $product;
    }
}
