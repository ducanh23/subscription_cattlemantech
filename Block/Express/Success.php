<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 23/03/2016
 * Time: 23:00
 */

namespace Magenest\Subscription\Block\Express;

use Magento\Catalog\Block\Product\Context;

class Success extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    public function __construct(
        Context $context,
        array $data
    ) {
    
        $this->_coreRegistry = $context->getRegistry();
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getProfileInfo()
    {
        $profile = $this->_coreRegistry->registry('new_profile');

        return $profile;
    }
}
