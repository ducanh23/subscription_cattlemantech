<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 19/03/2016
 * Time: 23:24
 */

namespace Magenest\Subscription\Block\Express;

use Magento\Catalog\Block\Product\Context;
use Magento\Customer\Model\Address\Config as AddressConfig;

class Review extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var AddressConfig
     */
    protected $_addressConfig;

    /**
     * Review constructor.
     * @param Context $context
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param AddressConfig $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Checkout\Model\Session $checkoutSession,
        AddressConfig $config,
        array $data
    ) {
    
        $this->_coreRegistry = $context->getRegistry();
        $this->_checkoutSession = $checkoutSession;
        $this->_addressConfig = $config;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getResponseInfo()
    {
        $response = $this->_coreRegistry->registry('subscription_express_response');

        return $response;
    }

    /**
     * @return string
     */
    public function getCustomerName()
    {
        $quote = $this->getQuote();

        $customer = $quote->getCustomer();
        if ($customer) {
            $name = $customer->getPrefix() . ' ' .
                $customer->getFirstname() . ' ' .
                $customer->getMiddlename() . ' ' .
                $customer->getLastname() . ' ' .
                $customer->getSuffix();
        } else {
            $name = $quote->getShippingAddress()->getName();
        }

        return $name;
    }

    /**
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        $quote = $this->_checkoutSession->getQuote();

        return $quote;
    }

    /**
     * @return \Magento\Framework\DataObject
     */
    public function getCartItem()
    {
        $quote = $this->_checkoutSession->getQuote();

        $item = $quote->getItemsCollection()->getFirstItem();

        return $item;
    }

    /**
     * @param $address
     * @return string
     */
    public function renderAddress($address)
    {
        /** @var \Magento\Customer\Block\Address\Renderer\RendererInterface $renderer */
        $renderer = $this->_addressConfig->getFormatByCode('html')->getRenderer();
        $addressData = \Magento\Framework\Convert\ConvertArray::toFlatArray($address->getData());
        return $renderer->renderArray($addressData);
    }

    /**
     * @return string
     */
    public function getPlaceOrderUrl()
    {
        return $this->getUrl('subscription/express/placeOrder');
    }
}
