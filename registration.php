<?php
/**
 * Created by Magenest.
 * Author: Pham Quang Hau
 * Date: 04/03/2016
 * Time: 10:16
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Magenest_Subscription',
    __DIR__
);
