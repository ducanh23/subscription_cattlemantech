<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 14/10/2017
 * Time: 15:43
 */
namespace Magenest\Subscription\Ui\DataProvider\Template;

class ProductDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Collection
     */
    protected $collection;

    /**
     * ProductDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->toArray();

        foreach ($items as &$stuff) {
            $stuff['ids'] = ["'".$stuff['entity_id']."'"];
        }

        $input = [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => array_values($items)
        ];
        return $input;
    }
}
