<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 25/03/2016
 * Time: 21:10
 */

namespace Magenest\Subscription\Ui\DataProvider\Profile;

use Magenest\Subscription\Model\ResourceModel\Profile\CollectionFactory;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider;

/**
 * Class ProfileDataProvider
 * @package Magenest\Subscription\Ui\DataProvider\Profile
 */
class ProfileDataProvider extends DataProvider
{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * ProfileDataProvider constructor.
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param ReportingInterface $reporting
     * @param CollectionFactory $collectionFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param RequestInterface $request
     * @param FilterBuilder $filterBuilder
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ReportingInterface $reporting,
        CollectionFactory $collectionFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        array $meta = [],
        array $data = []
    ) {
    
        $this->collectionFactory = $collectionFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->toArray();

        return [
            'totalRecords' => $this->getCollection()->getSize(),
            'items' => array_values($items['items']),
        ];
    }

    /**
     * @return \Magenest\Subscription\Model\ResourceModel\Profile\Collection
     */
    public function getCollection()
    {
        if (!isset($this->collection)) {
            /** @var \Magenest\Subscription\Model\ResourceModel\Profile\Collection $collection */
            $collection = $this->collectionFactory->create();
            $this->collection = $collection;
        }
        return $this->collection;
    }
}
