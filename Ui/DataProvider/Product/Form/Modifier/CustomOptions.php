<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 * Date: 06/07/2016
 * Time: 23:12
 */

namespace Magenest\Subscription\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ProductOptions\ConfigInterface;
use Magento\Catalog\Model\Config\Source\Product\Options\Price as ProductOptionsPrice;
use Magento\Framework\UrlInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Ui\Component\Modal;
use Magento\Ui\Component\Container;
use Magento\Ui\Component\DynamicRows;
use Magento\Ui\Component\Form\Fieldset;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\Component\Form\Element\Input;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\DataType\Number;
use Magenest\Subscription\Model\AttributeFactory;
use Magenest\Subscription\Model\TemplateFactory;
use Magenest\Subscription\Logger\Logger;

class CustomOptions extends AbstractModifier
{
    /**#@+
     * Group values
     */
    const GROUP_BILLING_OPTIONS_NAME = 'billing_options';
    const GROUP_BILLING_OPTIONS_SCOPE = 'data.product';
    // name of tab that precedes this billing options tab
    const GROUP_BILLING_OPTIONS_PREVIOUS_NAME = 'subscription-options';
    const GROUP_BILLING_OPTIONS_DEFAULT_SORT_ORDER = 300;
    /**#@-*/

    /**#@+
     * Button values
     */
    const BUTTON_ADD = 'button_add';
    /**#@-*/

    /**#@+
     * Container values
     */
    const CONTAINER_HEADER_NAME = 'billing_container_header';
    const CONTAINER_OPTION = 'billing_container_option';
    const CONTAINER_COMMON_NAME = 'billing_container_common';
    const CONTAINER_TYPE_TRIAL_NAME = 'billing_container_type_trial';
    const CONTAINER_TYPE_INIT_NAME = 'billing_container_type_init';
    /**#@-*/

    /**#@+
     * Grid values
     */
    const GRID_OPTIONS_NAME = 'billing_options';
    const GRID_TYPE_SELECT_NAME = 'billing_values';
    /**#@-*/

    /**#@+
     * Field values
     */
    const FIELD_ENABLE = 'billing_affect_product_custom_options';
    const FIELD_OPTION_ID = 'billing_option_id';
    const FIELD_OPTION_NAME = 'billing_option_name';
    const FIELD_UNIT_NAME = 'billing_unit';
    const FIELD_FREQUENCY_NAME = 'billing_frequency';
    const FIELD_MAX_CYCLES_NAME = 'billing_max_cycles';
    const FIELD_TITLE_NAME = 'billing_title';
    const FIELD_TYPE_NAME = 'billing_type';
    const FIELD_IS_REQUIRE_NAME = 'billing_is_require';
    const FIELD_SORT_ORDER_NAME = 'billing_sort_order';
    const FIELD_IS_DELETE = 'billing_is_delete';
    /**#@-*/

    /**#@+
     * Trial Field values
     */
    const FIELD_IS_TRIAL_ENABLED = 'billing_is_trial_enabled';
    const FIELD_TRIAL_AMOUNT_NAME = 'billing_trial_amount';
    const FIELD_TRIAL_UNIT_NAME = 'billing_trial_unit';
    const FIELD_TRIAL_FREQUENCY_NAME = 'billing_trial_frequency';
    const FIELD_TRIAL_MAX_CYCLES_NAME = 'billing_trial_max_cycles';
    /**#@-*/

    /**#@+
     * Init Field values
     */
    const FIELD_IS_INIT_ENABLED = 'billing_is_init_enabled';
    const FIELD_INIT_FEE = 'billing_init_fee';
    /**#@-*/

    /**#@+
     * Import options values
     */
    const CUSTOM_OPTIONS_LISTING = 'product_custom_options_listing';
    /**#@-*/

    protected $meta = [];
    protected $logger;
    protected $locator;
    protected $storeManager;
    protected $productOptionsConfig;
    protected $productOptionsPrice;
    protected $urlBuilder;
    protected $arrayManager;
    protected $attrFactory;
    protected $templateFactory;

    public function __construct(
        LocatorInterface $locator,
        StoreManagerInterface $storeManager,
        ConfigInterface $productOptionsConfig,
        ProductOptionsPrice $productOptionsPrice,
        UrlInterface $urlBuilder,
        ArrayManager $arrayManager,
        Logger $logger,
        AttributeFactory $attributeFactory,
        TemplateFactory $templateFactory
    ) {
    
        $this->locator = $locator;
        $this->storeManager = $storeManager;
        $this->productOptionsConfig = $productOptionsConfig;
        $this->productOptionsPrice = $productOptionsPrice;
        $this->urlBuilder = $urlBuilder;
        $this->arrayManager = $arrayManager;
        $this->logger = $logger;
        $this->attrFactory = $attributeFactory;
        $this->templateFactory = $templateFactory;
    }

    public function modifyData(array $data)
    {
        $productId = $this->locator->getProduct()->getId();

        $attrModel = $this->attrFactory->create();
        $productOptions = [];
        $attr = $attrModel->getCollection()->addFieldToFilter('entity_id', $productId)->getFirstItem();
        $billingOptions = unserialize($attr->getValue());

        $sortOrder = 1;
        // Process options

        if ($billingOptions) {
            foreach ($billingOptions as $option) {
                $productOption = [
                    'billing_sort_order' => $sortOrder,
                    'billing_option_name' => $option['optionName'],
                    'billing_unit' => $option['selectedUnit'],
                    'billing_frequency' => $option['frequency'],
                    'billing_max_cycles' => $option['maxCycles'],
                    'billing_is_trial_enabled' => $option['trialEnabled'],
                    'billing_trial_amount' => $option['trialAmount'],
                    'billing_trial_unit' => $option['trialUnit'],
                    'billing_trial_frequency' => $option['trialFrequency'],
                    'billing_trial_max_cycles' => $option['trialMaxCycles'],
                    'billing_is_init_enabled' => $option['initEnabled'],
                    'billing_init_fee' => $option['initFee'],
                    'is_option_template' => 'no'
                ];

                array_push($productOptions, $productOption);
                $sortOrder++;
            }

            $data[$productId]['product']['billing_options'] = $productOptions;
        }

        return $data;
    }

    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;

        $this->createSubscriptionOptionsPanel();

        return $this->meta;
    }

    protected function createSubscriptionOptionsPanel()
    {
        $this->meta = array_replace_recursive(
            $this->meta,
            [
                static::GROUP_BILLING_OPTIONS_NAME => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'label' => __('Subscription Interval Settings'),
                                'componentType' => Fieldset::NAME,
                                'dataScope' => static::GROUP_BILLING_OPTIONS_SCOPE,
                                'collapsible' => true,
                                'sortOrder' => $this->getNextGroupSortOrder(
                                    $this->meta,
                                    static::GROUP_BILLING_OPTIONS_PREVIOUS_NAME,
                                    static::GROUP_BILLING_OPTIONS_DEFAULT_SORT_ORDER
                                ),
                            ],
                        ],
                    ],
                    'children' => [
                        static::CONTAINER_HEADER_NAME => $this->getHeaderContainerConfig(10),
                        static::FIELD_ENABLE => $this->getEnableFieldConfig(20),
                        static::GRID_OPTIONS_NAME => $this->getOptionsGridConfig(30)
                    ]
                ]
            ]
        );

        return $this;
    }

    protected function getHeaderContainerConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => null,
                        'formElement' => Container::NAME,
                        'componentType' => Container::NAME,
                        'template' => 'ui/form/components/complex',
                        'sortOrder' => $sortOrder,
                        'content' =>
                            "NOTE: <strong>Authorize.net</strong> does not allow less than <strong>\"7 Days\"</strong> recurring frequency; <strong>Initial Fee</strong> is also not available",
                    ],
                ],
            ],
            'children' => [
                static::BUTTON_ADD => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'title' => __('Add Option'),
                                'formElement' => Container::NAME,
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/form/components/button',
                                'sortOrder' => 20,
                                'actions' => [
                                    [
                                        'targetName' => "product_form.product_form.billing_options.billing_options",
                                        'actionName' => 'processingAddChild',
                                    ]
                                ]
                            ]
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function getEnableFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => Field::NAME,
                        'componentType' => Input::NAME,
                        'dataScope' => static::FIELD_ENABLE,
                        'dataType' => Number::NAME,
                        'visible' => false,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];
    }

    protected function getOptionsGridConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'addButtonLabel' => __('Add Option'),
                        'componentType' => DynamicRows::NAME,
                        'component' => 'Magento_Catalog/js/components/dynamic-rows-import-custom-options',
                        'template' => 'ui/dynamic-rows/templates/collapsible',
                        'additionalClasses' => 'admin__field-wide',
                        'deleteProperty' => static::FIELD_IS_DELETE,
                        'deleteValue' => '1',
                        'addButton' => false,
                        'renderDefaultRecord' => false,
                        'columnsHeader' => false,
                        'collapsibleHeader' => true,
                        'sortOrder' => $sortOrder,
                        'dataProvider' => static::CUSTOM_OPTIONS_LISTING,
                        'links' => ['insertData' => '${ $.provider }:${ $.dataProvider }'],
                    ],
                ],
            ],
            'children' => [
                'record' => [
                    'arguments' => [
                        'data' => [
                            'config' => [
                                'headerLabel' => __('New Billing Profile'),
                                'componentType' => Container::NAME,
                                'component' => 'Magento_Ui/js/dynamic-rows/record',
                                'positionProvider' => static::CONTAINER_OPTION . '.' . static::FIELD_SORT_ORDER_NAME,
                                'isTemplate' => true,
                                'is_collection' => true,
                            ],
                        ],
                    ],
                    'children' => [
                        static::CONTAINER_OPTION => [
                            'arguments' => [
                                'data' => [
                                    'config' => [
                                        'componentType' => Fieldset::NAME,
                                        'label' => null,
                                        'sortOrder' => 10,
                                        'opened' => true,
                                    ],
                                ],
                            ],
                            'children' => [
                                static::FIELD_SORT_ORDER_NAME => $this->getPositionFieldConfig(10),
                                static::CONTAINER_COMMON_NAME => $this->getCommonContainerConfig(20),
                                static::CONTAINER_TYPE_TRIAL_NAME => $this->getTrialTypeContainerConfig(30),
                                static::CONTAINER_TYPE_INIT_NAME => $this->getInitTypeContainerConfig(40)
                            ]
                        ],
                    ]
                ]
            ]
        ];
    }

    protected function getPositionFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_SORT_ORDER_NAME,
                        'dataType' => Number::NAME,
                        'visible' => false,
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
        ];
    }

    protected function getCommonContainerConfig($sortOrder)
    {
        $commonContainer = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'formElement' => Container::NAME,
                        'component' => 'Magento_Ui/js/form/components/group',
                        'breakLine' => false,
                        'showLabel' => false,
                        'additionalClasses' => 'admin__field-group-columns admin__control-group-equal',
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                static::FIELD_OPTION_ID => $this->getOptionIdFieldConfig(10),
                static::FIELD_OPTION_NAME => $this->getInputFieldConfig(
                    15,
                    __('Option Name'),
                    static::FIELD_OPTION_NAME,
                    Text::NAME,
                    true
                ),
                static::FIELD_UNIT_NAME => $this->getUnitFieldConfig(
                    20,
                    __('Period Unit'),
                    true
                ),
                static::FIELD_FREQUENCY_NAME => $this->getInputFieldConfig(
                    30,
                    __('Billing Frequency'),
                    static::FIELD_FREQUENCY_NAME,
                    Number::NAME,
                    true
                ),
                static::FIELD_MAX_CYCLES_NAME => $this->getInputFieldConfig(
                    40,
                    __('Maximum Billing Cycles'),
                    static::FIELD_MAX_CYCLES_NAME,
                    Number::NAME,
                    true
                ),
                static::FIELD_IS_TRIAL_ENABLED => $this->getIsTrialEnabledFieldConfig(60),
                static::FIELD_IS_INIT_ENABLED => $this->getIsInitEnabledFieldConfig(70),
            ]
        ];

        if ($this->locator->getProduct()->getStoreId()) {
            $useDefaultConfig = [
                'service' => [
                    'template' => 'Magento_Catalog/form/element/helper/custom-option-service',
                ]
            ];
            $titlePath = $this->arrayManager->findPath(static::FIELD_TITLE_NAME, $commonContainer, null)
                . static::META_CONFIG_PATH;
            $commonContainer = $this->arrayManager->merge($titlePath, $commonContainer, $useDefaultConfig);
        }

        return $commonContainer;
    }

    protected function getOptionIdFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'formElement' => Input::NAME,
                        'componentType' => Field::NAME,
                        'dataScope' => static::FIELD_OPTION_ID,
                        'sortOrder' => $sortOrder,
                        'visible' => false,
                    ],
                ],
            ],
        ];
    }

    protected function getInputFieldConfig($sortOrder, $label, $scope, $type, $isRequired)
    {
        $validateNumber = true;

        $options = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => $label,
                        'component' => 'Magento_Catalog/component/static-type-input',
                        'valueUpdate' => 'input'
                    ],
                ],
            ],
        ];

        if ($type == Text::NAME) {
            $validateNumber = false;
        }

        return array_replace_recursive(
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => $label,
                            'componentType' => Field::NAME,
                            'formElement' => Input::NAME,
                            'dataScope' => $scope,
                            'dataType' => $type,
                            'sortOrder' => $sortOrder,
                            'validation' => [
                                'required-entry' => $isRequired,
                                'validate-zero-or-greater' => $validateNumber
                            ],
                        ],
                    ],
                ],
            ],
            $options
        );
    }

    protected function getUnitFieldConfig($sortOrder, $label, $isRequired)
    {
        $options = [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => $label,
                        'valueUpdate' => 'input'
                    ],
                ],
            ],
        ];

        return array_replace_recursive(
            [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'label' => $label,
                            'componentType' => Field::NAME,
                            'dataType' => Text::NAME,
                            'formElement' => Select::NAME,
                            'dataScope' => static::FIELD_UNIT_NAME,
                            'options' => $this->getUnitOptions(),
                            'sortOrder' => $sortOrder,
                            'visible' => true,
                            'validation' => [
                                'required-entry' => $isRequired
                            ],
                        ],
                    ],
                ],
            ],
            $options
        );
    }

    protected function getUnitOptions()
    {
        return [
            [
                'label' => 'Day',
                'value' => 'day'
            ],
            [
                'label' => 'Week',
                'value' => 'week'
            ],
            [
                'label' => 'Semi-Month',
                'value' => 'semimonth'
            ],
            [
                'label' => 'Month',
                'value' => 'month'
            ],
            [
                'label' => 'Year',
                'value' => 'year'
            ]
        ];
    }

    protected function getIsTrialEnabledFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Trial Enabled'),
                        'componentType' => Field::NAME,
                        'formElement' => Select::NAME,
                        'component' => 'Magento_Catalog/js/custom-options-type',
                        'elementTmpl' => 'ui/grid/filters/elements/ui-select',
                        'selectType' => 'optgroup',
                        'dataScope' => static::FIELD_IS_TRIAL_ENABLED,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'options' => $this->getTrialEnabledConfig(),
                        'disableLabel' => true,
                        'multiple' => false,
                        'selectedPlaceholders' => [
                            'defaultPlaceholder' => __('-- Please select --'),
                        ],
                        'validation' => [
                            'required-entry' => true
                        ],
                        'groupsConfig' => [
                            'options' => [
                                'values' => ['yes'],
                                'indexes' => [
                                    static::CONTAINER_TYPE_TRIAL_NAME,
                                    static::FIELD_TRIAL_AMOUNT_NAME,
                                    static::FIELD_TRIAL_UNIT_NAME,
                                    static::FIELD_TRIAL_FREQUENCY_NAME,
                                    static::FIELD_TRIAL_MAX_CYCLES_NAME
                                ]
                            ]
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function getTrialEnabledConfig()
    {
        return [
            [
                'value' => 0,
                'label' => 'Options',
                'optgroup' => [
                    [
                        'label' => 'Yes',
                        'value' => 'yes'
                    ],
                    [
                        'label' => 'No',
                        'value' => 'no'
                    ]
                ]
            ]
        ];
    }

    protected function getIsInitEnabledFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Initial Fee Enabled'),
                        'componentType' => Field::NAME,
                        'formElement' => Select::NAME,
                        'component' => 'Magento_Catalog/js/custom-options-type',
                        'elementTmpl' => 'ui/grid/filters/elements/ui-select',
                        'selectType' => 'optgroup',
                        'dataScope' => static::FIELD_IS_INIT_ENABLED,
                        'dataType' => Text::NAME,
                        'sortOrder' => $sortOrder,
                        'options' => $this->getTrialEnabledConfig(),
                        'disableLabel' => true,
                        'multiple' => false,
                        'selectedPlaceholders' => [
                            'defaultPlaceholder' => __('-- Please select --'),
                        ],
                        'validation' => [
                            'required-entry' => true
                        ],
                        'groupsConfig' => [
                            'options' => [
                                'values' => ['yes'],
                                'indexes' => [
                                    static::CONTAINER_TYPE_INIT_NAME,
                                    static::FIELD_INIT_FEE
                                ]
                            ]
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function getTrialTypeContainerConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'formElement' => Container::NAME,
                        'component' => 'Magento_Ui/js/form/components/group',
                        'breakLine' => false,
                        'showLabel' => false,
                        'additionalClasses' => 'admin__field-group-columns admin__control-group-equal',
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                static::FIELD_TRIAL_AMOUNT_NAME => $this->getTrialAmountFieldConfig(10),
                static::FIELD_TRIAL_UNIT_NAME => $this->getTrialUnitFieldConfig(20),
                static::FIELD_TRIAL_FREQUENCY_NAME => $this->getTrialFrequencyConfig(30),
                static::FIELD_TRIAL_MAX_CYCLES_NAME => $this->getTrialMaxCyclesConfig(40)
            ]
        ];
    }

    protected function getTrialAmountFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Amount'),
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_TRIAL_AMOUNT_NAME,
                        'dataType' => Number::NAME,
                        'addbefore' => $this->getCurrencySymbol(),
                        'sortOrder' => $sortOrder,
                        'validation' => [
                            'validate-zero-or-greater' => true
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function getCurrencySymbol()
    {
        return $this->storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();
    }

    protected function getTrialUnitFieldConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Period Unit'),
                        'componentType' => Field::NAME,
                        'dataType' => Text::NAME,
                        'formElement' => Select::NAME,
                        'dataScope' => static::FIELD_TRIAL_UNIT_NAME,
                        'options' => $this->getUnitOptions(),
                        'sortOrder' => $sortOrder,
                        'visible' => true,
                    ],
                ],
            ],
        ];
    }

    protected function getTrialFrequencyConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Frequency'),
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_TRIAL_FREQUENCY_NAME,
                        'dataType' => Number::NAME,
                        'sortOrder' => $sortOrder,
                        'validation' => [
                            'validate-zero-or-greater' => true
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function getTrialMaxCyclesConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Billing Cycles'),
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_TRIAL_MAX_CYCLES_NAME,
                        'dataType' => Number::NAME,
                        'sortOrder' => $sortOrder,
                        'validation' => [
                            'validate-zero-or-greater' => true
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function getInitTypeContainerConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Container::NAME,
                        'formElement' => Container::NAME,
                        'component' => 'Magento_Ui/js/form/components/group',
                        'breakLine' => false,
                        'showLabel' => false,
                        'additionalClasses' => 'admin__field-group-columns admin__control-group-equal',
                        'sortOrder' => $sortOrder,
                    ],
                ],
            ],
            'children' => [
                static::FIELD_INIT_FEE => $this->getInitFeeConfig(50)
            ]
        ];
    }

    protected function getInitFeeConfig($sortOrder)
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'label' => __('Initial Amount'),
                        'componentType' => Field::NAME,
                        'formElement' => Input::NAME,
                        'dataScope' => static::FIELD_INIT_FEE,
                        'dataType' => Number::NAME,
                        'addbefore' => $this->getCurrencySymbol(),
                        'sortOrder' => $sortOrder,
                        'validation' => [
                            'validate-zero-or-greater' => true
                        ],
                    ],
                ],
            ],
        ];
    }
}
