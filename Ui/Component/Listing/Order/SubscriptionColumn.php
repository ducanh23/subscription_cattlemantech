<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Ui\Component\Listing\Order;

use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Ui\Component\Listing\Columns\Column;
use \Magento\Framework\Api\SearchCriteriaBuilder;

class SubscriptionColumn extends Column
{
    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $_searchCriteria;

    /**
     * @var \Magenest\Subscription\Helper\Backend\Order
     */
    protected $_helper;

    /**
     * SubscriptionColumn constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Magenest\Subscription\Helper\Backend\Order $orderHelper
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $criteria
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Magenest\Subscription\Helper\Backend\Order $orderHelper,
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $criteria,
        array $components = [],
        array $data = []
    ) {
    
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteria = $criteria;
        $this->_helper = $orderHelper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $result = '';
                $order = $this->_orderRepository->get($item["entity_id"]);

                if ($this->_helper->isChildOrder($order)) {
                    $parentOrder = $this->_helper->getParentOrder($order);
                    $url = $this->_helper->getOrderUrl($parentOrder);
                    $result = [
                        'view' => [
                            'href' => $url,
                            'label' => __($parentOrder)
                        ]
                    ];
                }
                $item[$this->getData('name')] = $result;
            }
        }
        return $dataSource;

    }
}
