<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Ui\Component\Listing\Columns\Status;

use Magenest\Subscription\Model\ResourceModel\Profile\CollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Options
 */
class Options implements OptionSourceInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Constructor
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = $this->collectionFactory->create()->toOptionArray();
        }
        $optionArray = [];
        $options = $this->options;
        foreach ($options as $temp) {
            if ($optionArray == []) {
                array_push($optionArray, $temp['value']);
            }
            if (!in_array($temp['value'], $optionArray)) {
                array_push($optionArray, $temp['value']);
            }
        }
        $result = [];
        foreach ($optionArray as $key => $item) {
            $result[] = [
                'value' => $item,
                'label' => ucfirst($item)
            ];
        }
        return $result;
    }
}
