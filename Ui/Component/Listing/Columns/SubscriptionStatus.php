<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Catalog\Model\ProductFactory;

class SubscriptionStatus extends Column
{
    /**
     * @var mixed
     */
    protected $_coreRegistry;

    /**
     * @var mixed
     */
    protected $_product;

    /**
     * @var ProductFactory
     */
    protected $_productFactory;

    /**
     * SubscriptionStatus constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     * @param ProductFactory $productFactory
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ProductFactory $productFactory,
        array $components = [],
        array $data = []
    ) {
        $this->_productFactory = $productFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $product = $this->_productFactory->create()->load($item['entity_id']);
                $value = $product->getData('enable_subscription');
                if ($value) {
                    $item[$this->getData('name')] = "Yes";
                } else {
                    $item[$this->getData('name')] = 'No';
                }
            }
        }
        return $dataSource;
    }
}
