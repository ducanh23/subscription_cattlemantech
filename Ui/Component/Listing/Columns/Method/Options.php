<?php
/**
 * Created by Magenest. All rights reserved.
 * Author: joel
 */
namespace Magenest\Subscription\Ui\Component\Listing\Columns\Method;

use Magenest\Subscription\Model\ResourceModel\Profile\CollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Options
 */
class Options implements OptionSourceInterface
{
    protected $methodList = [
        'authorizenet_directpost' => 'Authorize.net Directpost',
        'paypal_express' => 'Paypal Express Checkout'
    ];

    /**
     * @var array
     */
    protected $options;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Options constructor.
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $this->options = $this->collectionFactory->create()->getMethodListArray();
        }
        $items = array_unique($this->options, SORT_REGULAR);
        $result = [];
        foreach ($items as $item) {
            $code = $item['method_code'];
            $result[] = [
                'value' => $code,
                'label' => isset($this->methodList[$code]) ? $this->methodList[$code] : $code
            ];
        }
        return $result;
    }
}
